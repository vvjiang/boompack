import { parse, print } from "recast";
import { readFile, writeFile } from "fs";
import path from "path";
import modifyAst from "./utils/modifyAst.js";

const fromPath = path.join("./test/from/index.js");
const toPath = path.join("./test/to/index.js");

readFile(fromPath, { encoding: "utf8" }, (err, sourceCode) => {
  const ast = parse(sourceCode);
  modifyAst(ast);
  writeFile(toPath, print(ast).code, () => {
    console.info("搞完");
  });
});
