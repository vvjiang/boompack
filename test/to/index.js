/*! jQuery v1.7 jquery.com | jquery.org/license */
(function (var_a, var_b) {
  function cA(var_a) {
    let result;

    if (var_f.isWindow(var_a)) {
      result = var_a;
    } else {
      if (var_a.nodeType === 9) {
        if (var_a.defaultView) {
          result = var_a.defaultView;
        } else {
          result = var_a.parentWindow;
        }
      } else {
        result = false;
      }
    }

    return result;
  }
  function cx(var_a) {
    if (!cm[var_a]) {
      let var_b, var_d, var_e;
      var_b = var_c.body;
      var_d = var_f("<" + var_a + ">").appendTo(var_b);
      var_e = var_d.css("display");
      var_d.remove();
      if (var_e === "none" || var_e === "") {
        if (!cn) {
          cn = var_c.createElement("iframe");
          cn.frameBorder = cn.width = cn.height = 0;
        }

        var_b.appendChild(cn);
        if (!co || !cn.createElement) {
          co = (cn.contentWindow || cn.contentDocument).document;

          co.write(
            (var_c.compatMode === "CSS1Compat" ? "<!doctype html>" : "") +
            "<html><body>"
          );

          co.close();
        }
        var_d = co.createElement(var_a);
        co.body.appendChild(var_d);
        var_e = var_f.css(var_d, "display");
        var_b.removeChild(cn);
      }
      cm[var_a] = var_e;
    }
    let result;
    result = cm[var_a];
    return result;
  }
  function cw(var_a, var_b) {
    let var_c;
    var_c = {};
    var_f.each(cs.concat.apply([], cs.slice(0, var_b)), function () {
      var_c[this] = var_a;
    });
    return var_c;
  }
  function cv() {
    ct = var_b;
  }
  function cu() {
    setTimeout(cv, 0);
    let result;
    result = ct = var_f.now();
    return result;
  }
  function cl() {
    try {
      let result;
      result = new var_a.ActiveXObject("Microsoft.XMLHTTP");
      return result;
    } catch (var_b) { }
  }
  function ck() {
    try {
      let result;
      result = new var_a.XMLHttpRequest();
      return result;
    } catch (var_b) { }
  }
  function ce(var_a, var_c) {
    if (var_a.dataFilter) {
      var_c = var_a.dataFilter(var_c, var_a.dataType);
    }

    let var_d, var_e, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p;
    var_d = var_a.dataTypes;
    var_e = {};
    var_i = var_d.length;
    var_k = var_d[0];
    for (var_g = 1; var_g < var_i; var_g++) {
      if (var_g === 1) {
        for (var_h in var_a.converters) {
          if (typeof var_h == "string") {
            var_e[var_h.toLowerCase()] = var_a.converters[var_h];
          }
        }
      }
      var_l = var_k;
      var_k = var_d[var_g];
      if (var_k === "*") {
        var_k = var_l;
      } else {
        if (var_l !== "*" && var_l !== var_k) {
          var_m = var_l + " " + var_k;

          if (var_e[var_m]) {
            var_n = var_e[var_m];
          } else {
            var_n = var_e["* " + var_k];
          }

          if (!var_n) {
            var_p = var_b;
            for (var_o in var_e) {
              var_j = var_o.split(" ");
              if (var_j[0] === var_l || var_j[0] === "*") {
                var_p = var_e[var_j[1] + " " + var_k];
                if (var_p) {
                  var_o = var_e[var_o];

                  if (var_o === true) {
                    var_n = var_p;
                  } else {
                    if (var_p === true) {
                      var_n = var_o;
                    }
                  }

                  break;
                }
              }
            }
          }

          if (!var_n && !var_p) {
            var_f.error("No conversion from " + var_m.replace(" ", " to "));
          }

          if (var_n !== true) {
            if (var_n) {
              var_c = var_n(var_c);
            } else {
              var_c = var_p(var_o(var_c));
            }
          }
        }
      }
    }
    return var_c;
  }
  function cd(var_a, var_c, var_d) {
    let var_e, var_f, var_g, var_h, var_i, var_j, var_k;
    var_e = var_a.contents;
    var_f = var_a.dataTypes;
    var_g = var_a.responseFields;
    for (var_i in var_g) {
      if (var_i in var_d) {
        var_c[var_g[var_i]] = var_d[var_i];
      }
    }
    while (var_f[0] === "*") {
      var_f.shift();

      if (var_h === var_b) {
        if (var_a.mimeType) {
          var_h = var_a.mimeType;
        } else {
          var_h = var_c.getResponseHeader("content-type");
        }
      }
    }
    if (var_h) {
      for (var_i in var_e) {
        if (var_e[var_i] && var_e[var_i].test(var_h)) {
          var_f.unshift(var_i);
          break;
        }
      }
    }
    if (var_f[0] in var_d) {
      var_j = var_f[0];
    } else {
      for (var_i in var_d) {
        if (!var_f[0] || var_a.converters[var_i + " " + var_f[0]]) {
          var_j = var_i;
          break;
        }

        if (!var_k) {
          var_k = var_i;
        }
      }

      if (var_j) {
        var_j = var_j;
      } else {
        var_j = var_k;
      }
    }
    if (var_j) {
      if (var_j !== var_f[0]) {
        var_f.unshift(var_j);
      }

      let result;
      result = var_d[var_j];
      return result;
    }
  }
  function cc(var_a, var_b, var_c, var_d) {
    if (var_f.isArray(var_b)) {
      var_f.each(var_b, function (var_b, var_e) {
        if (var_c || bG.test(var_a)) {
          var_d(var_a, var_e);
        } else {
          cc(
              var_a + "[" + (typeof var_e == "object" || var_f.isArray(var_e) ? var_b : "") + "]",
              var_e,
              var_c,
              var_d
            );
        }
      });
    } else {
      if (!var_c && var_b != null && typeof var_b == "object") {
        for (var var_e in var_b) {
          cc(var_a + "[" + var_e + "]", var_b[var_e], var_c, var_d);
        }
      } else {
        var_d(var_a, var_b);
      }
    }
  }
  function cb(var_a, var_c) {
    let var_d, var_e, var_g;

    if (var_f.ajaxSettings.flatOptions) {
      var_g = var_f.ajaxSettings.flatOptions;
    } else {
      var_g = {};
    }

    for (var_d in var_c) {
      if (var_c[var_d] !== var_b) {
        (var_g[var_d] ? var_a : var_e || (var_e = {}))[var_d] = var_c[var_d];
      }
    }

    if (var_e) {
      var_f.extend(true, var_a, var_e);
    }
  }
  function ca(var_a, var_c, var_d, var_e, var_f, var_g) {
    if (var_f) {
      var_f = var_f;
    } else {
      var_f = var_c.dataTypes[0];
    }

    if (var_g) {
      var_g = var_g;
    } else {
      var_g = {};
    }

    var_g[var_f] = true;
    let var_h, var_i, var_j, var_k, var_l;
    var_h = var_a[var_f];
    var_i = 0;

    if (var_h) {
      var_j = var_h.length;
    } else {
      var_j = 0;
    }

    var_k = var_a === bV;
    for (; var_i < var_j && (var_k || !var_l); var_i++) {
      var_l = var_h[var_i](var_c, var_d, var_e);

      if (typeof var_l == "string") {
        if (!var_k || var_g[var_l]) {
          var_l = var_b;
        } else {
          var_c.dataTypes.unshift(var_l);
          var_l = ca(var_a, var_c, var_d, var_e, var_l, var_g);
        }
      }
    }

    if ((var_k || !var_l) && !var_g["*"]) {
      var_l = ca(var_a, var_c, var_d, var_e, "*", var_g);
    }

    return var_l;
  }
  function b_(var_a) {
    let result;

    result = function (var_b, var_c) {
      if (typeof var_b != "string") {
        var_c = var_b;
        var_b = "*";
      }

      if (var_f.isFunction(var_c)) {
        let var_d, var_e, var_g, var_h, var_i, var_j;
        var_d = var_b.toLowerCase().split(bR);
        var_e = 0;
        var_g = var_d.length;
        for (; var_e < var_g; var_e++) {
          var_h = var_d[var_e];
          var_j = /^\+/.test(var_h);

          if (var_j) {
            if (var_h.substr(1)) {
              var_h = var_h.substr(1);
            } else {
              var_h = "*";
            }
          }

          var_i = var_a[var_h] = var_a[var_h] || [];
          var_i[var_j ? "unshift" : "push"](var_c);
        }
      }
    };

    return result;
  }
  function bE(var_a, var_b, var_c) {
    let var_d, var_e;

    if (var_b === "width") {
      var_d = var_a.offsetWidth;
    } else {
      var_d = var_a.offsetHeight;
    }

    if (var_b === "width") {
      var_e = bz;
    } else {
      var_e = bA;
    }

    if (var_d > 0) {
      if (var_c !== "border") {
        var_f.each(var_e, function () {
          if (!var_c) {
            var_d -= parseFloat(var_f.css(var_a, "padding" + this)) || 0;
          }

          if (var_c === "margin") {
            var_d += parseFloat(var_f.css(var_a, var_c + this)) || 0;
          } else {
            var_d -= parseFloat(var_f.css(var_a, "border" + this + "Width")) || 0;
          }
        });
      }

      let result;
      result = var_d + "px";
      return result;
    }
    var_d = bB(var_a, var_b, var_b);
    if (var_d < 0 || var_d == null) {
      if (var_a.style[var_b]) {
        var_d = var_a.style[var_b];
      } else {
        var_d = 0;
      }
    }

    if (parseFloat(var_d)) {
      var_d = parseFloat(var_d);
    } else {
      var_d = 0;
    }

    if (var_c) {
      var_f.each(var_e, function () {
        var_d += parseFloat(var_f.css(var_a, "padding" + this)) || 0;

        if (var_c !== "padding") {
          var_d += parseFloat(var_f.css(var_a, "border" + this + "Width")) || 0;
        }

        if (var_c === "margin") {
          var_d += parseFloat(var_f.css(var_a, var_c + this)) || 0;
        }
      });
    }

    let result;
    result = var_d + "px";
    return result;
  }
  function br(var_a, var_b) {
    if (var_b.src) {
      var_f.ajax({
          url: var_b.src,
          async: false,
          dataType: "script",
        });
    } else {
      var_f.globalEval(
          (var_b.text || var_b.textContent || var_b.innerHTML || "").replace(bi, "/*$0*/")
        );
    }

    if (var_b.parentNode) {
      var_b.parentNode.removeChild(var_b);
    }
  }
  function bq(var_a) {
    let var_b;
    var_b = (var_a.nodeName || "").toLowerCase();

    if (var_b === "input") {
      bp(var_a);
    } else {
      if (var_b !== "script" &&
        typeof var_a.getElementsByTagName != "undefined") {
        var_f.grep(var_a.getElementsByTagName("input"), bp);
      }
    }
  }
  function bp(var_a) {
    if (var_a.type === "checkbox" || var_a.type === "radio") {
      var_a.defaultChecked = var_a.checked;
    }
  }
  function bo(var_a) {
    let result;

    if (typeof var_a.getElementsByTagName != "undefined") {
      result = var_a.getElementsByTagName("*");
    } else {
      if (typeof var_a.querySelectorAll != "undefined") {
        result = var_a.querySelectorAll("*");
      } else {
        result = [];
      }
    }

    return result;
  }
  function bn(var_a, var_b) {
    let var_c;
    if (var_b.nodeType === 1) {
      if (var_b.clearAttributes) {
        var_b.clearAttributes();
      }

      if (var_b.mergeAttributes) {
        var_b.mergeAttributes(var_a);
      }

      var_c = var_b.nodeName.toLowerCase();
      if (var_c === "object") {
        var_b.outerHTML = var_a.outerHTML;
      } else {
        if (var_c !== "input" || (var_a.type !== "checkbox" && var_a.type !== "radio")) {
          if (var_c === "option") {
            var_b.selected = var_a.defaultSelected;
          } else {
            if (var_c === "input" || var_c === "textarea") {
              var_b.defaultValue = var_a.defaultValue;
            }
          }
        } else {
          if (var_a.checked) {
            var_b.defaultChecked = var_b.checked = var_a.checked;
          }

          if (var_b.value !== var_a.value) {
            var_b.value = var_a.value;
          }
        }
      }
      var_b.removeAttribute(var_f.expando);
    }
  }
  function bm(var_a, var_b) {
    if (var_b.nodeType === 1 && !!var_f.hasData(var_a)) {
      let var_c, var_d, var_e, var_g, var_h, var_i;
      var_g = var_f._data(var_a);
      var_h = var_f._data(var_b, var_g);
      var_i = var_g.events;
      if (var_i) {
        delete var_h.handle;
        var_h.events = {};
        for (var_c in var_i) {
          for (var_d = 0, var_e = var_i[var_c].length; var_d < var_e; var_d++) {
            var_f.event.add(
              var_b,
              var_c + (var_i[var_c][var_d].namespace ? "." : "") + var_i[var_c][var_d].namespace,
              var_i[var_c][var_d],
              var_i[var_c][var_d].data
            );
          }
        }
      }

      if (var_h.data) {
        var_h.data = var_f.extend({}, var_h.data);
      }
    }
  }
  function bl(var_a, var_b) {
    let result;

    if (var_f.nodeName(var_a, "table")) {
      if (var_a.getElementsByTagName("tbody")[0]) {
        result = var_a.getElementsByTagName("tbody")[0];
      } else {
        result = var_a.appendChild(var_a.ownerDocument.createElement("tbody"));
      }
    } else {
      result = var_a;
    }

    return result;
  }
  function func_X(var_a) {
    let var_b, var_c;
    var_b = var_Y.split(" ");
    var_c = var_a.createDocumentFragment();
    if (var_c.createElement) {
      while (var_b.length) {
        var_c.createElement(var_b.pop());
      }
    }
    return var_c;
  }
  function func_W(var_a, var_b, var_c) {
    if (var_b) {
      var_b = var_b;
    } else {
      var_b = 0;
    }

    if (var_f.isFunction(var_b)) {
      let result;

      result = var_f.grep(var_a, function (var_a, var_d) {
        let var_e;
        var_e = !!var_b.call(var_a, var_d, var_a);
        return var_e === var_c;
      });

      return result;
    }
    if (var_b.nodeType) {
      let result;

      result = var_f.grep(var_a, function (var_a, var_d) {
        return (var_a === var_b) === var_c;
      });

      return result;
    }
    if (typeof var_b == "string") {
      let var_d;

      var_d = var_f.grep(var_a, function (var_a) {
        let result;
        result = var_a.nodeType === 1;
        return result;
      });

      if (var_R.test(var_b)) {
        let result;
        result = var_f.filter(var_b, var_d, !var_c);
        return result;
      }
      var_b = var_f.filter(var_b, var_d);
    }
    let result;

    result = var_f.grep(var_a, function (var_a, var_d) {
      return var_f.inArray(var_a, var_b) >= 0 === var_c;
    });

    return result;
  }
  function func_V(var_a) {
    let result;

    if (!var_a || !var_a.parentNode) {
      if (!var_a) {
        result = !var_a;
      } else {
        result = !var_a.parentNode;
      }
    } else {
      result = var_a.parentNode.nodeType === 11;
    }

    return result;
  }
  function func_N() {
    return true;
  }
  function func_M() {
    return false;
  }
  function var_n(var_a, var_b, var_c) {
    let var_d, var_e, var_g, var_h;
    var_d = var_b + "defer";
    var_e = var_b + "queue";
    var_g = var_b + "mark";
    var_h = var_f._data(var_a, var_d);

    if (var_h &&
      (var_c === "queue" || !var_f._data(var_a, var_e)) &&
      (var_c === "mark" || !var_f._data(var_a, var_g))) {
      setTimeout(function () {
        if (!var_f._data(var_a, var_e) && !var_f._data(var_a, var_g)) {
          var_f.removeData(var_a, var_d, true);
          var_h.fire();
        }
      }, 0);
    }
  }
  function var_m(var_a) {
    for (var var_b in var_a) {
      if (var_b === "data" && var_f.isEmptyObject(var_a[var_b])) {
        continue;
      }
      if (var_b !== "toJSON") {
        return false;
      }
    }
    return true;
  }
  function var_l(var_a, var_c, var_d) {
    if (var_d === var_b && var_a.nodeType === 1) {
      let var_e;
      var_e = "data-" + var_c.replace(var_k, "-$1").toLowerCase();
      var_d = var_a.getAttribute(var_e);
      if (typeof var_d == "string") {
        try {
          if (var_d === "true") {
            var_d = true;
          } else {
            if (var_d === "false") {
              var_d = false;
            } else {
              if (var_d === "null") {
                var_d = null;
              } else {
                if (var_f.isNumeric(var_d)) {
                  var_d = parseFloat(var_d);
                } else {
                  if (var_j.test(var_d)) {
                    var_d = var_f.parseJSON(var_d);
                  } else {
                    var_d = var_d;
                  }
                }
              }
            }
          }
        } catch (var_g) { }
        var_f.data(var_a, var_c, var_d);
      } else {
        var_d = var_b;
      }
    }
    return var_d;
  }
  function var_h(var_a) {
    let var_b, var_c, var_d;
    var_b = var_g[var_a] = {};
    var_a = var_a.split(/\s+/);
    for (var_c = 0, var_d = var_a.length; var_c < var_d; var_c++) {
      var_b[var_a[var_c]] = true;
    }
    return var_b;
  }
  let var_c, var_d, var_e, var_f, var_g;
  var_c = var_a.document;
  var_d = var_a.navigator;
  var_e = var_a.location;

  var_f = (function () {
    function var_K() {
      if (!var_e.isReady) {
        try {
          var_c.documentElement.doScroll("left");
        } catch (var_a) {
          setTimeout(var_K, 1);
          return;
        }
        var_e.ready();
      }
    }
    let var_e, var_f, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p, var_q, var_r, var_s, var_t, var_u, var_v, var_w, var_x, var_y, var_z, var_A, var_B, var_C, var_D, var_E, var_F, var_G, var_H, var_I, var_J;

    var_e = function (var_a, var_b) {
      let result;
      result = new var_e.fn.init(var_a, var_b, var_h);
      return result;
    };

    var_f = var_a.jQuery;
    var_g = var_a.$;
    var_i = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/;
    var_j = /\S/;
    var_k = /^\s+/;
    var_l = /\s+$/;
    var_m = /\d/;
    var_n = /^<(\w+)\s*\/?>(?:<\/\1>)?$/;
    var_o = /^[\],:{}\s]*$/;
    var_p = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
    var_q = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
    var_r = /(?:^|:|,)(?:\s*\[)+/g;
    var_s = /(webkit)[ \/]([\w.]+)/;
    var_t = /(opera)(?:.*version)?[ \/]([\w.]+)/;
    var_u = /(msie) ([\w.]+)/;
    var_v = /(mozilla)(?:.*? rv:([\w.]+))?/;
    var_w = /-([a-z]|[0-9])/gi;
    var_x = /^-ms-/;

    var_y = function (var_a, var_b) {
      let result;
      result = (var_b + "").toUpperCase();
      return result;
    };

    var_z = var_d.userAgent;
    var_D = Object.prototype.toString;
    var_E = Object.prototype.hasOwnProperty;
    var_F = Array.prototype.push;
    var_G = Array.prototype.slice;
    var_H = String.prototype.trim;
    var_I = Array.prototype.indexOf;
    var_J = {};

    var_e.fn = var_e.prototype =
    {
      constructor: var_e,
      init: function (var_a, var_d, var_f) {
        let var_g, var_h, var_j, var_k;
        if (!var_a) {
          let result;
          result = this;
          return result;
        }
        if (var_a.nodeType) {
          this.context = this[0] = var_a;
          this.length = 1;
          let result;
          result = this;
          return result;
        }
        if (var_a === "body" && !var_d && var_c.body) {
          this.context = var_c;
          this[0] = var_c.body;
          this.selector = var_a;
          this.length = 1;
          let result;
          result = this;
          return result;
        }
        if (typeof var_a == "string") {
          if (var_a.charAt(0) !== "<" ||
            var_a.charAt(var_a.length - 1) !== ">" ||
            var_a.length < 3) {
            var_g = var_i.exec(var_a);
          } else {
            var_g = [null, var_a, null];
          }

          if (var_g && (var_g[1] || !var_d)) {
            if (var_g[1]) {
              if (var_d instanceof var_e) {
                var_d = var_d[0];
              } else {
                var_d = var_d;
              }

              if (var_d) {
                if (var_d.ownerDocument) {
                  var_k = var_d.ownerDocument;
                } else {
                  var_k = var_d;
                }
              } else {
                var_k = var_c;
              }

              var_j = var_n.exec(var_a);

              if (var_j) {
                if (var_e.isPlainObject(var_d)) {
                  var_a = [var_c.createElement(var_j[1])];
                  var_e.fn.attr.call(var_a, var_d, true);
                } else {
                  var_a = [var_k.createElement(var_j[1])];
                }
              } else {
                var_j = var_e.buildFragment([var_g[1]], [var_k]);

                var_a = (var_j.cacheable ? var_e.clone(var_j.fragment) : var_j.fragment)
                      .childNodes;
              }

              let result;
              result = var_e.merge(this, var_a);
              return result;
            }
            var_h = var_c.getElementById(var_g[2]);
            if (var_h && var_h.parentNode) {
              if (var_h.id !== var_g[2]) {
                let result;
                result = var_f.find(var_a);
                return result;
              }
              this.length = 1;
              this[0] = var_h;
            }
            this.context = var_c;
            this.selector = var_a;
            let result;
            result = this;
            return result;
          }
          let result;

          if (!var_d || var_d.jquery) {
            result = (var_d || var_f).find(var_a);
          } else {
            result = this.constructor(var_d).find(var_a);
          }

          return result;
        }
        if (var_e.isFunction(var_a)) {
          let result;
          result = var_f.ready(var_a);
          return result;
        }

        if (var_a.selector !== var_b) {
          this.selector = var_a.selector;
          this.context = var_a.context;
        }

        let result;
        result = var_e.makeArray(var_a, this);
        return result;
      },
      selector: "",
      jquery: "1.7",
      length: 0,
      size: function () {
        let result;
        result = this.length;
        return result;
      },
      toArray: function () {
        let result;
        result = var_G.call(this, 0);
        return result;
      },
      get: function (var_a) {
        let result;

        if (var_a == null) {
          result = this.toArray();
        } else {
          if (var_a < 0) {
            result = this[this.length + var_a];
          } else {
            result = this[var_a];
          }
        }

        return result;
      },
      pushStack: function (var_a, var_b, var_c) {
        let var_d;
        var_d = this.constructor();

        if (var_e.isArray(var_a)) {
          var_F.apply(var_d, var_a);
        } else {
          var_e.merge(var_d, var_a);
        }

        var_d.prevObject = this;
        var_d.context = this.context;

        if (var_b === "find") {
          var_d.selector = this.selector + (this.selector ? " " : "") + var_c;
        } else {
          if (var_b) {
            var_d.selector = this.selector + "." + var_b + "(" + var_c + ")";
          }
        }

        return var_d;
      },
      each: function (var_a, var_b) {
        let result;
        result = var_e.each(this, var_a, var_b);
        return result;
      },
      ready: function (var_a) {
        var_e.bindReady();
        var_B.add(var_a);
        let result;
        result = this;
        return result;
      },
      eq: function (var_a) {
        let result;

        if (var_a === false) {
          result = this.slice(var_a);
        } else {
          result = this.slice(var_a, +var_a + 1);
        }

        return result;
      },
      first: function () {
        let result;
        result = this.eq(0);
        return result;
      },
      last: function () {
        let result;
        result = this.eq(false);
        return result;
      },
      slice: function () {
        let result;

        result = this.pushStack(
          var_G.apply(this, arguments),
          "slice",
          var_G.call(arguments).join(",")
        );

        return result;
      },
      map: function (var_a) {
        let result;

        result = this.pushStack(
          var_e.map(this, function (var_b, var_c) {
            return var_a.call(var_b, var_c, var_b);
          })
        );

        return result;
      },
      end: function () {
        let result;

        if (this.prevObject) {
          result = this.prevObject;
        } else {
          result = this.constructor(null);
        }

        return result;
      },
      push: var_F,
      sort: [].sort,
      splice: [].splice,
    };

    var_e.fn.init.prototype = var_e.fn;

    var_e.extend = var_e.fn.extend =
        function () {
          let var_a, var_c, var_d, var_f, var_g, var_h, var_i, var_j, var_k, var_l;

          if (arguments[0]) {
            var_i = arguments[0];
          } else {
            var_i = {};
          }

          var_j = 1;
          var_k = arguments.length;
          var_l = false;

          if (typeof var_i == "boolean") {
            var_l = var_i;

            if (arguments[1]) {
              var_i = arguments[1];
            } else {
              var_i = {};
            }

            var_j = 2;
          }

          if (typeof var_i != "object" && !var_e.isFunction(var_i)) {
            var_i = {};
          }

          if (var_k === var_j) {
            var_i = this;
            --var_j;
          }

          for (; var_j < var_k; var_j++) {
            if ((var_a = arguments[var_j]) != null) {
              for (var_c in var_a) {
                var_d = var_i[var_c];
                var_f = var_a[var_c];
                if (var_i === var_f) {
                  continue;
                }

                if (var_l && var_f && (var_e.isPlainObject(var_f) || (var_g = var_e.isArray(var_f)))) {
                  if (var_g) {
                    var_g = false;

                    if (var_d && var_e.isArray(var_d)) {
                      var_h = var_d;
                    } else {
                      var_h = [];
                    }
                  } else {
                    if (var_d && var_e.isPlainObject(var_d)) {
                      var_h = var_d;
                    } else {
                      var_h = {};
                    }
                  }

                  var_i[var_c] = var_e.extend(var_l, var_h, var_f);
                } else {
                  if (var_f !== var_b) {
                    var_i[var_c] = var_f;
                  }
                }
              }
            }
          }
          return var_i;
        };

    var_e.extend({
      noConflict: function (var_b) {
        if (var_a.$ === var_e) {
          var_a.$ = var_g;
        }

        if (var_b && var_a.jQuery === var_e) {
          var_a.jQuery = var_f;
        }

        return var_e;
      },
      isReady: false,
      readyWait: 1,
      holdReady: function (var_a) {
        if (var_a) {
          var_e.readyWait++;
        } else {
          var_e.ready(true);
        }
      },
      ready: function (var_a) {
        if ((var_a === true && !--var_e.readyWait) || (var_a !== true && !var_e.isReady)) {
          if (!var_c.body) {
            let result;
            result = setTimeout(var_e.ready, 1);
            return result;
          }
          var_e.isReady = true;
          if (var_a !== true && --var_e.readyWait > 0) {
            return;
          }
          var_B.fireWith(var_c, [var_e]);

          if (var_e.fn.trigger) {
            var_e(var_c).trigger("ready").unbind("ready");
          }
        }
      },
      bindReady: function () {
        if (!var_B) {
          var_B = var_e.Callbacks("once memory");
          if (var_c.readyState === "complete") {
            let result;
            result = setTimeout(var_e.ready, 1);
            return result;
          }
          if (var_c.addEventListener) {
            var_c.addEventListener("DOMContentLoaded", var_C, false);
            var_a.addEventListener("load", var_e.ready, false);
          } else {
            if (var_c.attachEvent) {
              var_c.attachEvent("onreadystatechange", var_C);
              var_a.attachEvent("onload", var_e.ready);
              let var_b;
              var_b = false;
              try {
                var_b = var_a.frameElement == null;
              } catch (var_d) { }

              if (var_c.documentElement.doScroll && var_b) {
                var_K();
              }
            }
          }
        }
      },
      isFunction: function (var_a) {
        let result;
        result = var_e.type(var_a) === "function";
        return result;
      },
      isArray:
        Array.isArray ||
        function (var_a) {
          let result;
          result = var_e.type(var_a) === "array";
          return result;
        },
      isWindow: function (var_a) {
        let result;

        if (var_a && typeof var_a == "object") {
          result = "setInterval" in var_a;
        } else {
          if (var_a) {
            result = typeof var_a == "object";
          } else {
            result = var_a;
          }
        }

        return result;
      },
      isNumeric: function (var_a) {
        let result;

        if (var_a != null && var_m.test(var_a)) {
          result = !isNaN(var_a);
        } else {
          if (var_a != null) {
            result = var_m.test(var_a);
          } else {
            result = var_a != null;
          }
        }

        return result;
      },
      type: function (var_a) {
        let result;

        if (var_a == null) {
          result = String(var_a);
        } else {
          if (var_J[var_D.call(var_a)]) {
            result = var_J[var_D.call(var_a)];
          } else {
            result = "object";
          }
        }

        return result;
      },
      isPlainObject: function (var_a) {
        if (!var_a || var_e.type(var_a) !== "object" || var_a.nodeType || var_e.isWindow(var_a)) {
          return false;
        }
        try {
          if (var_a.constructor &&
          !var_E.call(var_a, "constructor") &&
          !var_E.call(var_a.constructor.prototype, "isPrototypeOf")) {
            return false;
          }
        } catch (var_c) {
          return false;
        }
        let var_d;
        for (var_d in var_a)
          {}
        let result;

        if (var_d === var_b) {
          result = var_d === var_b;
        } else {
          result = var_E.call(var_a, var_d);
        }

        return result;
      },
      isEmptyObject: function (var_a) {
        for (var var_b in var_a) {
          return false;
        }
        return true;
      },
      error: function (var_a) {
        throw var_a;
      },
      parseJSON: function (var_b) {
        if (typeof var_b != "string" || !var_b) {
          return null;
        }
        var_b = var_e.trim(var_b);
        if (var_a.JSON && var_a.JSON.parse) {
          let result;
          result = var_a.JSON.parse(var_b);
          return result;
        }
        if (var_o.test(var_b.replace(var_p, "@").replace(var_q, "]").replace(var_r, ""))) {
          let result;
          result = new Function("return " + var_b)();
          return result;
        }
        var_e.error("Invalid JSON: " + var_b);
      },
      parseXML: function (var_c) {
        let var_d, var_f;
        try {
          if (var_a.DOMParser) {
            var_f = new DOMParser();
            var_d = var_f.parseFromString(var_c, "text/xml");
          } else {
            var_d = new ActiveXObject("Microsoft.XMLDOM");
            var_d.async = "false";
            var_d.loadXML(var_c);
          }
        } catch (var_g) {
          var_d = var_b;
        }

        if (!var_d ||
          !var_d.documentElement ||
          var_d.getElementsByTagName("parsererror").length) {
          var_e.error("Invalid XML: " + var_c);
        }

        return var_d;
      },
      noop: function () { },
      globalEval: function (var_b) {
        if (var_b &&
          var_j.test(var_b)) {
          (
            var_a.execScript ||
            function (var_b) {
              var_a.eval.call(var_a, var_b);
            }
          )(var_b);
        }
      },
      camelCase: function (var_a) {
        let result;
        result = var_a.replace(var_x, "ms-").replace(var_w, var_y);
        return result;
      },
      nodeName: function (var_a, var_b) {
        let result;

        if (var_a.nodeName) {
          result = var_a.nodeName.toUpperCase() === var_b.toUpperCase();
        } else {
          result = var_a.nodeName;
        }

        return result;
      },
      each: function (var_a, var_c, var_d) {
        let var_f, var_g, var_h, var_i;
        var_g = 0;
        var_h = var_a.length;

        if (var_h === var_b) {
          var_i = var_h === var_b;
        } else {
          var_i = var_e.isFunction(var_a);
        }

        if (var_d) {
          if (var_i) {
            for (var_f in var_a) {
              if (var_c.apply(var_a[var_f], var_d) === false) {
                break;
              }
            }
          } else {
            for (; var_g < var_h; ) {
              if (var_c.apply(var_a[var_g++], var_d) === false) {
                break;
              }
            }
          }
        } else {
          if (var_i) {
            for (var_f in var_a) {
              if (var_c.call(var_a[var_f], var_f, var_a[var_f]) === false) {
                break;
              }
            }
          } else {
            for (; var_g < var_h; ) {
              if (var_c.call(var_a[var_g], var_g, var_a[var_g++]) === false) {
                break;
              }
            }
          }
        }
        return var_a;
      },
      trim: var_H
        ? function (var_a) {
        let result;

        if (var_a == null) {
          result = "";
        } else {
          result = var_H.call(var_a);
        }

        return result;
      }
        : function (var_a) {
        let result;

        if (var_a == null) {
          result = "";
        } else {
          result = (var_a + "").replace(var_k, "").replace(var_l, "");
        }

        return result;
      },
      makeArray: function (var_a, var_b) {
        let var_c;

        if (var_b) {
          var_c = var_b;
        } else {
          var_c = [];
        }

        if (var_a != null) {
          let var_d;
          var_d = var_e.type(var_a);

          if (var_a.length == null ||
            var_d === "string" ||
            var_d === "function" ||
            var_d === "regexp" ||
            var_e.isWindow(var_a)) {
            var_F.call(var_c, var_a);
          } else {
            var_e.merge(var_c, var_a);
          }
        }
        return var_c;
      },
      inArray: function (var_a, var_b, var_c) {
        let var_d;
        if (var_b) {
          if (var_I) {
            let result;
            result = var_I.call(var_b, var_a, var_c);
            return result;
          }
          var_d = var_b.length;

          if (var_c) {
            if (var_c < 0) {
              var_c = Math.max(0, var_d + var_c);
            } else {
              var_c = var_c;
            }
          } else {
            var_c = 0;
          }

          for (; var_c < var_d; var_c++) {
            if (var_c in var_b && var_b[var_c] === var_a) {
              return var_c;
            }
          }
        }
        return false;
      },
      merge: function (var_a, var_c) {
        let var_d, var_e;
        var_d = var_a.length;
        var_e = 0;
        if (typeof var_c.length == "number") {
          for (var var_f = var_c.length; var_e < var_f; var_e++) {
            var_a[var_d++] = var_c[var_e];
          }
        } else {
          while (var_c[var_e] !== var_b) {
            var_a[var_d++] = var_c[var_e++];
          }
        }
        var_a.length = var_d;
        return var_a;
      },
      grep: function (var_a, var_b, var_c) {
        let var_d, var_e;
        var_d = [];
        var_c = !!var_c;
        for (var var_f = 0, var_g = var_a.length; var_f < var_g; var_f++) {
          var_e = !!var_b(var_a[var_f], var_f);

          if (var_c !== var_e) {
            var_d.push(var_a[var_f]);
          }
        }
        return var_d;
      },
      map: function (var_a, var_c, var_d) {
        let var_f, var_g, var_h, var_i, var_j, var_k;
        var_h = [];
        var_i = 0;
        var_j = var_a.length;

        if (var_a instanceof var_e) {
          var_k = var_a instanceof var_e;
        } else {
          if (var_j !== var_b &&
            typeof var_j == "number") {
            if ((var_j > 0 && var_a[0] && var_a[var_j - 1]) || var_j === 0) {
              if (var_j > 0 && var_a[0] && var_a[var_j - 1]) {
                if (var_j > 0 && var_a[0]) {
                  var_k = var_a[var_j - 1];
                } else {
                  if (var_j > 0) {
                    var_k = var_a[0];
                  } else {
                    var_k = var_j > 0;
                  }
                }
              } else {
                var_k = var_j === 0;
              }
            } else {
              var_k = var_e.isArray(var_a);
            }
          } else {
            if (var_j !== var_b) {
              var_k = typeof var_j == "number";
            } else {
              var_k = var_j !== var_b;
            }
          }
        }

        if (var_k) {
          for (; var_i < var_j; var_i++) {
            var_f = var_c(var_a[var_i], var_i, var_d);

            if (var_f != null) {
              var_h[var_h.length] = var_f;
            }
          }
        } else {
          for (var_g in var_a) {
            var_f = var_c(var_a[var_g], var_g, var_d);

            if (var_f != null) {
              var_h[var_h.length] = var_f;
            }
          }
        }
        let result;
        result = var_h.concat.apply([], var_h);
        return result;
      },
      guid: 1,
      proxy: function (var_a, var_c) {
        if (typeof var_c == "string") {
          let var_d;
          var_d = var_a[var_c];
          var_c = var_a;
          var_a = var_d;
        }
        if (!var_e.isFunction(var_a)) {
          return var_b;
        }
        let var_f, var_g;
        var_f = var_G.call(arguments, 2);

        var_g = function () {
          let result;
          result = var_a.apply(var_c, var_f.concat(var_G.call(arguments)));
          return result;
        };

        var_g.guid = var_a.guid = var_a.guid || var_g.guid || var_e.guid++;
        return var_g;
      },
      access: function (var_a, var_c, var_d, var_f, var_g, var_h) {
        let var_i;
        var_i = var_a.length;
        if (typeof var_c == "object") {
          for (var var_j in var_c) {
            var_e.access(var_a, var_j, var_c[var_j], var_f, var_g, var_d);
          }
          return var_a;
        }
        if (var_d !== var_b) {
          if (!var_h && var_f) {
            var_f = var_e.isFunction(var_d);
          } else {
            if (!var_h) {
              var_f = var_f;
            } else {
              var_f = !var_h;
            }
          }

          for (var var_k = 0; var_k < var_i; var_k++) {
            var_g(var_a[var_k], var_c, var_f ? var_d.call(var_a[var_k], var_k, var_g(var_a[var_k], var_c)) : var_d, var_h);
          }
          return var_a;
        }
        let result;

        if (var_i) {
          result = var_g(var_a[0], var_c);
        } else {
          result = var_b;
        }

        return result;
      },
      now: function () {
        let result;
        result = new Date().getTime();
        return result;
      },
      uaMatch: function (var_a) {
        var_a = var_a.toLowerCase();
        let var_b;

        if (var_s.exec(var_a) ||
        var_t.exec(var_a) ||
        var_u.exec(var_a) ||
        (var_a.indexOf("compatible") < 0 && var_v.exec(var_a))) {
          if (var_s.exec(var_a) ||
          var_t.exec(var_a) ||
          var_u.exec(var_a)) {
            if (var_s.exec(var_a) ||
            var_t.exec(var_a)) {
              if (var_s.exec(var_a)) {
                var_b = var_s.exec(var_a);
              } else {
                var_b = var_t.exec(var_a);
              }
            } else {
              var_b = var_u.exec(var_a);
            }
          } else {
            if (var_a.indexOf("compatible") < 0) {
              var_b = var_v.exec(var_a);
            } else {
              var_b = var_a.indexOf("compatible") < 0;
            }
          }
        } else {
          var_b = [];
        }

        let result;

        result = {
          browser: var_b[1] || "",
          version: var_b[2] || "0",
        };

        return result;
      },
      sub: function () {
        function var_a(var_b, var_c) {
          let result;
          result = new var_a.fn.init(var_b, var_c);
          return result;
        }
        var_e.extend(true, var_a, this);
        var_a.superclass = this;
        var_a.fn = var_a.prototype = this();
        var_a.fn.constructor = var_a;
        var_a.sub = this.sub;

        var_a.fn.init = function (var_d, var_f) {
          if (var_f && var_f instanceof var_e && !(var_f instanceof var_a)) {
            var_f = var_a(var_f);
          }

          let result;
          result = var_e.fn.init.call(this, var_d, var_f, var_b);
          return result;
        };

        var_a.fn.init.prototype = var_a.fn;
        let var_b;
        var_b = var_a(var_c);
        return var_a;
      },
      browser: {},
    });

    var_e.each(
      "Boolean Number String Function Array Date RegExp Object".split(" "),
      function (var_a, var_b) {
        var_J["[object " + var_b + "]"] = var_b.toLowerCase();
      }
    );

    var_A = var_e.uaMatch(var_z);

    if (var_A.browser) {
      var_e.browser[var_A.browser] = true;
      var_e.browser.version = var_A.version;
    }

    if (var_e.browser.webkit) {
      var_e.browser.safari = true;
    }

    if (var_j.test(" ")) {
      var_k = /^[\s\xA0]+/;
      var_l = /[\s\xA0]+$/;
    }

    var_h = var_e(var_c);

    if (var_c.addEventListener) {
      var_C = function () {
        var_c.removeEventListener("DOMContentLoaded", var_C, false);
        var_e.ready();
      };
    } else {
      if (var_c.attachEvent) {
        var_C = function () {
          if (var_c.readyState === "complete") {
            var_c.detachEvent("onreadystatechange", var_C);
            var_e.ready();
          }
        };
      }
    }

    if (typeof define == "function" &&
    define.amd &&
    define.amd.jQuery) {
      define("jquery", [], function () {
        return var_e;
      });
    }

    return var_e;
  })();

  var_g = {};
  var_f.Callbacks = function (var_a) {
    if (var_a) {
      if (var_g[var_a]) {
        var_a = var_g[var_a];
      } else {
        var_a = var_h(var_a);
      }
    } else {
      var_a = {};
    }

    let var_c, var_d, var_e, var_i, var_j, var_k, var_l, var_m, var_n, var_o;
    var_c = [];
    var_d = [];

    var_m = function (var_b) {
      let var_d, var_e, var_g, var_h, var_i;
      for (var_d = 0, var_e = var_b.length; var_d < var_e; var_d++) {
        var_g = var_b[var_d];
        var_h = var_f.type(var_g);

        if (var_h === "array") {
          var_m(var_g);
        } else {
          if (var_h === "function" && (!var_a.unique || !var_o.has(var_g))) {
            var_c.push(var_g);
          }
        }
      }
    };

    var_n = function (var_b, var_f) {
      if (var_f) {
        var_f = var_f;
      } else {
        var_f = [];
      }

      if (!var_a.memory) {
        var_e = !var_a.memory;
      } else {
        var_e = [var_b, var_f];
      }

      var_i = true;

      if (var_j) {
        var_l = var_j;
      } else {
        var_l = 0;
      }

      var_j = 0;
      var_k = var_c.length;
      for (; var_c && var_l < var_k; var_l++) {
        if (var_c[var_l].apply(var_b, var_f) === false && var_a.stopOnFalse) {
          var_e = true;
          break;
        }
      }
      var_i = false;

      if (var_c) {
        if (var_a.once) {
          if (var_e === true) {
            var_o.disable();
          } else {
            var_c = [];
          }
        } else {
          if (var_d && var_d.length) {
            var_e = var_d.shift();
            var_o.fireWith(var_e[0], var_e[1]);
          }
        }
      }
    };

    var_o = {
      add: function () {
        if (var_c) {
          let var_a;
          var_a = var_c.length;
          var_m(arguments);

          if (var_i) {
            var_k = var_c.length;
          } else {
            if (var_e && var_e !== true) {
              var_j = var_a;
              var_n(var_e[0], var_e[1]);
            }
          }
        }
        let result;
        result = this;
        return result;
      },
      remove: function () {
        if (var_c) {
          let var_b, var_d, var_e;
          var_b = arguments;
          var_d = 0;
          var_e = var_b.length;
          for (; var_d < var_e; var_d++) {
            for (var var_f = 0; var_f < var_c.length; var_f++) {
              if (var_b[var_d] === var_c[var_f]) {
                if (var_i && var_f <= var_k) {
                  var_k--;

                  if (var_f <= var_l) {
                    var_l--;
                  }
                }

                var_c.splice(var_f--, 1);
                if (var_a.unique) {
                  break;
                }
              }
            }
          }
        }
        let result;
        result = this;
        return result;
      },
      has: function (var_a) {
        if (var_c) {
          let var_b, var_d;
          var_b = 0;
          var_d = var_c.length;
          for (; var_b < var_d; var_b++) {
            if (var_a === var_c[var_b]) {
              return true;
            }
          }
        }
        return false;
      },
      empty: function () {
        var_c = [];
        let result;
        result = this;
        return result;
      },
      disable: function () {
        var_c = var_d = var_e = var_b;
        let result;
        result = this;
        return result;
      },
      disabled: function () {
        let result;
        result = !var_c;
        return result;
      },
      lock: function () {
        var_d = var_b;

        if (!var_e || var_e === true) {
          var_o.disable();
        }

        let result;
        result = this;
        return result;
      },
      locked: function () {
        let result;
        result = !var_d;
        return result;
      },
      fireWith: function (var_b, var_c) {
        if (var_d) {
          if (var_i) {
            if (!var_a.once) {
              var_d.push([var_b, var_c]);
            }
          } else {
            if (!var_a.once || !var_e) {
              var_n(var_b, var_c);
            }
          }
        }

        let result;
        result = this;
        return result;
      },
      fire: function () {
        var_o.fireWith(this, arguments);
        let result;
        result = this;
        return result;
      },
      fired: function () {
        let result;
        result = !!var_e;
        return result;
      },
    };

    return var_o;
  };
  let var_i;
  var_i = [].slice;

  var_f.extend({
    Deferred: function (var_a) {
      let var_b, var_c, var_d, var_e, var_g, var_h, var_i, var_j;
      var_b = var_f.Callbacks("once memory");
      var_c = var_f.Callbacks("once memory");
      var_d = var_f.Callbacks("memory");
      var_e = "pending";

      var_g = {
        resolve: var_b,
        reject: var_c,
        notify: var_d,
      };

      var_h = {
        done: var_b.add,
        fail: var_c.add,
        progress: var_d.add,
        state: function () {
          return var_e;
        },
        isResolved: var_b.fired,
        isRejected: var_c.fired,
        then: function (var_a, var_b, var_c) {
          var_i.done(var_a).fail(var_b).progress(var_c);
          let result;
          result = this;
          return result;
        },
        always: function () {
          let result;
          result = var_i.done.apply(var_i, arguments).fail.apply(var_i, arguments);
          return result;
        },
        pipe: function (var_a, var_b, var_c) {
          let result;

          result = var_f
            .Deferred(function (var_d) {
              var_f.each(
                {
                  done: [var_a, "resolve"],
                  fail: [var_b, "reject"],
                  progress: [var_c, "notify"],
                },
                function (var_a, var_b) {
                  let var_c, var_e, var_g;
                  var_c = var_b[0];
                  var_e = var_b[1];

                  if (var_f.isFunction(var_c)) {
                    var_i[var_a](function () {
                      var_g = var_c.apply(this, arguments);

                      if (var_g && var_f.isFunction(var_g.promise)) {
                        var_g.promise().then(var_d.resolve, var_d.reject, var_d.notify);
                      } else {
                        var_d[var_e + "With"](this === var_i ? var_d : this, [var_g]);
                      }
                    });
                  } else {
                    var_i[var_a](var_d[var_e]);
                  }
                }
              );
            })
            .promise();

          return result;
        },
        promise: function (var_a) {
          if (var_a == null) {
            var_a = var_h;
          } else {
            for (var var_b in var_h) {
              var_a[var_b] = var_h[var_b];
            }
          }
          return var_a;
        },
      };

      var_i = var_h.promise({});
      for (var_j in var_g) {
        var_i[var_j] = var_g[var_j].fire;
        var_i[var_j + "With"] = var_g[var_j].fireWith;
      }

      var_i
        .done(
          function () {
            var_e = "resolved";
          },
          var_c.disable,
          var_d.lock
        )
        .fail(
          function () {
            var_e = "rejected";
          },
          var_b.disable,
          var_d.lock
        );

      if (var_a) {
        var_a.call(var_i, var_i);
      }

      return var_i;
    },
    when: function (var_a) {
      function var_m(var_a) {
        let result;

        result = function (var_b) {
          if (arguments.length > 1) {
            var_e[var_a] = var_i.call(arguments, 0);
          } else {
            var_e[var_a] = var_b;
          }

          var_j.notifyWith(var_k, var_e);
        };

        return result;
      }
      function var_l(var_a) {
        let result;

        result = function (var_c) {
          if (arguments.length > 1) {
            var_b[var_a] = var_i.call(arguments, 0);
          } else {
            var_b[var_a] = var_c;
          }

          if (!--var_g) {
            var_j.resolveWith(var_j, var_b);
          }
        };

        return result;
      }
      let var_b, var_c, var_d, var_e, var_g, var_h, var_j, var_k;
      var_b = var_i.call(arguments, 0);
      var_c = 0;
      var_d = var_b.length;
      var_e = Array(var_d);
      var_g = var_d;
      var_h = var_d;

      if (var_d <= 1 && var_a && var_f.isFunction(var_a.promise)) {
        var_j = var_a;
      } else {
        var_j = var_f.Deferred();
      }

      var_k = var_j.promise();
      if (var_d > 1) {
        for (; var_c < var_d; var_c++) {
          if (var_b[var_c] && var_b[var_c].promise && var_f.isFunction(var_b[var_c].promise)) {
            var_b[var_c].promise().then(var_l(var_c), var_j.reject, var_m(var_c));
          } else {
            --var_g;
          }
        }

        if (!var_g) {
          var_j.resolveWith(var_j, var_b);
        }
      } else {
        if (var_j !== var_a) {
          var_j.resolveWith(var_j, var_d ? [var_a] : []);
        }
      }
      return var_k;
    },
  });

  var_f.support = (function () {
    let var_a, var_b, var_d, var_e, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p, var_q, var_r, var_s, var_t, var_u;
    var_a = var_c.createElement("div");
    var_b = var_c.documentElement;
    var_a.setAttribute("className", "t");

    var_a.innerHTML =
        "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/><nav></nav>";

    var_d = var_a.getElementsByTagName("*");
    var_e = var_a.getElementsByTagName("a")[0];
    if (!var_d || !var_d.length || !var_e) {
      let result;
      result = {};
      return result;
    }
    var_g = var_c.createElement("select");
    var_h = var_g.appendChild(var_c.createElement("option"));
    var_i = var_a.getElementsByTagName("input")[0];

    var_k = {
        leadingWhitespace: var_a.firstChild.nodeType === 3,
        tbody: !var_a.getElementsByTagName("tbody").length,
        htmlSerialize: !!var_a.getElementsByTagName("link").length,
        style: /top/.test(var_e.getAttribute("style")),
        hrefNormalized: var_e.getAttribute("href") === "/a",
        opacity: /^0.55/.test(var_e.style.opacity),
        cssFloat: !!var_e.style.cssFloat,
        unknownElems: !!var_a.getElementsByTagName("nav").length,
        checkOn: var_i.value === "on",
        optSelected: var_h.selected,
        getSetAttribute: var_a.className !== "t",
        enctype: !!var_c.createElement("form").enctype,
        submitBubbles: true,
        changeBubbles: true,
        focusinBubbles: false,
        deleteExpando: true,
        noCloneEvent: true,
        inlineBlockNeedsLayout: false,
        shrinkWrapBlocks: false,
        reliableMarginRight: true,
      };

    var_i.checked = true;
    var_k.noCloneChecked = var_i.cloneNode(true).checked;
    var_g.disabled = true;
    var_k.optDisabled = !var_h.disabled;
    try {
      delete var_a.test;
    } catch (var_v) {
      var_k.deleteExpando = false;
    }

    if (!var_a.addEventListener &&
      var_a.attachEvent &&
      var_a.fireEvent) {
      var_a.attachEvent("onclick", function () {
          var_k.noCloneEvent = false;
        });

      var_a.cloneNode(true).fireEvent("onclick");
    }

    var_i = var_c.createElement("input");
    var_i.value = "t";
    var_i.setAttribute("type", "radio");
    var_k.radioValue = var_i.value === "t";
    var_i.setAttribute("checked", "checked");
    var_a.appendChild(var_i);
    var_l = var_c.createDocumentFragment();
    var_l.appendChild(var_a.lastChild);
    var_k.checkClone = var_l.cloneNode(true).cloneNode(true).lastChild.checked;
    var_a.innerHTML = "";
    var_a.style.width = var_a.style.paddingLeft = "1px";
    var_m = var_c.getElementsByTagName("body")[0];
    var_o = var_c.createElement(var_m ? "div" : "body");

    var_p = {
        visibility: "hidden",
        width: 0,
        height: 0,
        border: 0,
        margin: 0,
        background: "none",
      };

    if (var_m) {
      var_f.extend(var_p, {
        position: "absolute",
        left: "-999px",
        top: "-999px",
      });
    }

    for (var_t in var_p) {
      var_o.style[var_t] = var_p[var_t];
    }
    var_o.appendChild(var_a);

    if (var_m) {
      var_n = var_m;
    } else {
      var_n = var_b;
    }

    var_n.insertBefore(var_o, var_n.firstChild);
    var_k.appendChecked = var_i.checked;
    var_k.boxModel = var_a.offsetWidth === 2;

    if ("zoom" in var_a.style) {
      var_a.style.display = "inline";
      var_a.style.zoom = 1;
      var_k.inlineBlockNeedsLayout = var_a.offsetWidth === 2;
      var_a.style.display = "";
      var_a.innerHTML = "<div style='width:4px;'></div>";
      var_k.shrinkWrapBlocks = var_a.offsetWidth !== 2;
    }

    var_a.innerHTML =
        "<table><tr><td style='padding:0;border:0;display:none'></td><td>t</td></tr></table>";

    var_q = var_a.getElementsByTagName("td");
    var_u = var_q[0].offsetHeight === 0;
    var_q[0].style.display = "";
    var_q[1].style.display = "none";

    if (var_u) {
      var_k.reliableHiddenOffsets = var_q[0].offsetHeight === 0;
    } else {
      var_k.reliableHiddenOffsets = var_u;
    }

    var_a.innerHTML = "";

    if (var_c.defaultView &&
    var_c.defaultView.getComputedStyle) {
      var_j = var_c.createElement("div");
      var_j.style.width = "0";
      var_j.style.marginRight = "0";
      var_a.appendChild(var_j);

      var_k.reliableMarginRight =
          (parseInt(
            (
              var_c.defaultView.getComputedStyle(var_j, null) || {
                marginRight: 0,
              }
            ).marginRight,
            10
          ) || 0) === 0;
    }

    if (var_a.attachEvent) {
      for (var_t in {
        submit: 1,
        change: 1,
        focusin: 1,
      }) {
        var_s = "on" + var_t;
        var_u = var_s in var_a;

        if (!var_u) {
          var_a.setAttribute(var_s, "return;");
          var_u = typeof var_a[var_s] == "function";
        }

        var_k[var_t + "Bubbles"] = var_u;
      }
    }

    var_f(function () {
      let var_a, var_b, var_d, var_e, var_g, var_h, var_i, var_j, var_l, var_n, var_p;
      var_i = 1;
      var_j = "position:absolute;top:0;left:0;width:1px;height:1px;margin:0;";
      var_l = "visibility:hidden;border:0;";
      var_n = "style='" + var_j + "border:5px solid #000;padding:0;'";

      var_p = "<div " +
      var_n +
      "><div></div></div>" +
      "<table " +
      var_n +
      " cellpadding='0' cellspacing='0'>" +
      "<tr><td></td></tr></table>";

      var_m = var_c.getElementsByTagName("body")[0];

      if (!!var_m) {
        var_a = var_c.createElement("div");

        var_a.style.cssText =
              var_l +
              "width:0;height:0;position:static;top:0;margin-top:" +
              var_i +
              "px";

        var_m.insertBefore(var_a, var_m.firstChild);
        var_o = var_c.createElement("div");
        var_o.style.cssText = var_j + var_l;
        var_o.innerHTML = var_p;
        var_a.appendChild(var_o);
        var_b = var_o.firstChild;
        var_d = var_b.firstChild;
        var_g = var_b.nextSibling.firstChild.firstChild;

        var_h = {
              doesNotAddBorder: var_d.offsetTop !== 5,
              doesAddBorderForTableAndCells: var_g.offsetTop === 5,
            };

        var_d.style.position = "fixed";
        var_d.style.top = "20px";

        if (var_d.offsetTop === 20) {
          var_h.fixedPosition = var_d.offsetTop === 20;
        } else {
          var_h.fixedPosition = var_d.offsetTop === 15;
        }

        var_d.style.position = var_d.style.top = "";
        var_b.style.overflow = "hidden";
        var_b.style.position = "relative";
        var_h.subtractsBorderForOverflowNotVisible = var_d.offsetTop === -5;
        var_h.doesNotIncludeMarginInBodyOffset = var_m.offsetTop !== var_i;
        var_m.removeChild(var_a);
        var_o = var_a = null;
        var_f.extend(var_k, var_h);
      }
    });

    var_o.innerHTML = "";
    var_n.removeChild(var_o);
    var_o = var_l = var_g = var_h = var_m = var_j = var_a = var_i = null;
    return var_k;
  })();

  var_f.boxModel = var_f.support.boxModel;
  let var_j, var_k;
  var_j = /^(?:\{.*\}|\[.*\])$/;
  var_k = /([A-Z])/g;

  var_f.extend({
    cache: {},
    uuid: 0,
    expando: "jQuery" + (var_f.fn.jquery + Math.random()).replace(/\D/g, ""),
    noData: {
      embed: true,
      object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
      applet: true,
    },
    hasData: function (var_a) {
      if (var_a.nodeType) {
        var_a = var_f.cache[var_a[var_f.expando]];
      } else {
        var_a = var_a[var_f.expando];
      }

      let result;

      if (!!var_a) {
        result = !var_m(var_a);
      } else {
        result = !!var_a;
      }

      return result;
    },
    data: function (var_a, var_c, var_d, var_e) {
      if (!!var_f.acceptData(var_a)) {
        let var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o;
        var_j = var_f.expando;
        var_k = typeof var_c == "string";
        var_l = var_a.nodeType;

        if (var_l) {
          var_m = var_f.cache;
        } else {
          var_m = var_a;
        }

        if (var_l) {
          var_n = var_a[var_f.expando];
        } else {
          if (var_a[var_f.expando]) {
            var_n = var_f.expando;
          } else {
            var_n = var_a[var_f.expando];
          }
        }

        var_o = var_c === "events";
        if ((!var_n || !var_m[var_n] || (!var_o && !var_e && !var_m[var_n].data)) && var_k && var_d === var_b) {
          return;
        }

        if (!var_n) {
          if (var_l) {
            var_a[var_f.expando] = var_n = ++var_f.uuid;
          } else {
            var_n = var_f.expando;
          }
        }

        if (!var_m[var_n]) {
          var_m[var_n] = {};

          if (!var_l) {
            var_m[var_n].toJSON = var_f.noop;
          }
        }

        if (typeof var_c == "object" || typeof var_c == "function") {
          if (var_e) {
            var_m[var_n] = var_f.extend(var_m[var_n], var_c);
          } else {
            var_m[var_n].data = var_f.extend(var_m[var_n].data, var_c);
          }
        }
        var_g = var_h = var_m[var_n];

        if (!var_e) {
          if (!var_h.data) {
            var_h.data = {};
          }

          var_h = var_h.data;
        }

        if (var_d !== var_b) {
          var_h[var_f.camelCase(var_c)] = var_d;
        }

        if (var_o && !var_h[var_c]) {
          let result;
          result = var_g.events;
          return result;
        }

        if (var_k) {
          var_i = var_h[var_c];

          if (var_i == null) {
            var_i = var_h[var_f.camelCase(var_c)];
          }
        } else {
          var_i = var_h;
        }

        return var_i;
      }
    },
    removeData: function (var_a, var_b, var_c) {
      if (!!var_f.acceptData(var_a)) {
        let var_d, var_e, var_g, var_h, var_i, var_j, var_k;
        var_h = var_f.expando;
        var_i = var_a.nodeType;

        if (var_i) {
          var_j = var_f.cache;
        } else {
          var_j = var_a;
        }

        if (var_i) {
          var_k = var_a[var_f.expando];
        } else {
          var_k = var_f.expando;
        }

        if (!var_j[var_k]) {
          return;
        }
        if (var_b) {
          if (var_c) {
            var_d = var_j[var_k];
          } else {
            var_d = var_j[var_k].data;
          }

          if (var_d) {
            if (var_f.isArray(var_b)) {
              var_b = var_b;
            } else {
              if (var_b in var_d) {
                var_b = [var_b];
              } else {
                var_b = var_f.camelCase(var_b);

                if (var_b in var_d) {
                  var_b = [var_b];
                } else {
                  var_b = var_b.split(" ");
                }
              }
            }

            for (var_e = 0, var_g = var_b.length; var_e < var_g; var_e++) {
              delete var_d[var_b[var_e]];
            }
            if (!(var_c ? var_m : var_f.isEmptyObject)(var_d)) {
              return;
            }
          }
        }
        if (!var_c) {
          delete var_j[var_k].data;
          if (!var_m(var_j[var_k])) {
            return;
          }
        }

        if (var_f.support.deleteExpando || !var_j.setInterval) {
          delete var_j[var_k];
        } else {
          var_j[var_k] = null;
        }

        if (var_i) {
          if (var_f.support.deleteExpando) {
            delete var_a[var_f.expando];
          } else {
            if (var_a.removeAttribute) {
              var_a.removeAttribute(var_f.expando);
            } else {
              var_a[var_f.expando] = null;
            }
          }
        }
      }
    },
    _data: function (var_a, var_b, var_c) {
      let result;
      result = var_f.data(var_a, var_b, var_c, true);
      return result;
    },
    acceptData: function (var_a) {
      if (var_a.nodeName) {
        let var_b;
        var_b = var_f.noData[var_a.nodeName.toLowerCase()];
        if (var_b) {
          let result;

          if (var_b !== true) {
            result = var_a.getAttribute("classid") === var_b;
          } else {
            result = var_b !== true;
          }

          return result;
        }
      }
      return true;
    },
  });

  var_f.fn.extend({
    data: function (var_a, var_c) {
      let var_d, var_e, var_g, var_h;
      var_h = null;
      if (typeof var_a == "undefined") {
        if (this.length) {
          var_h = var_f.data(this[0]);
          if (this[0].nodeType === 1 && !var_f._data(this[0], "parsedAttrs")) {
            var_e = this[0].attributes;
            for (var var_i = 0, var_j = var_e.length; var_i < var_j; var_i++) {
              var_g = var_e[var_i].name;

              if (var_g.indexOf("data-") === 0) {
                var_g = var_f.camelCase(var_g.substring(5));
                var_l(this[0], var_g, var_h[var_g]);
              }
            }
            var_f._data(this[0], "parsedAttrs", true);
          }
        }
        return var_h;
      }
      if (typeof var_a == "object") {
        let result;

        result = this.each(function () {
          var_f.data(this, var_a);
        });

        return result;
      }
      var_d = var_a.split(".");

      if (var_d[1]) {
        var_d[1] = "." + var_d[1];
      } else {
        var_d[1] = "";
      }

      if (var_c === var_b) {
        var_h = this.triggerHandler("getData" + var_d[1] + "!", [var_d[0]]);

        if (var_h === var_b &&
        this.length) {
          var_h = var_f.data(this[0], var_a);
          var_h = var_l(this[0], var_a, var_h);
        }

        let result;

        if (var_h === var_b && var_d[1]) {
          result = this.data(var_d[0]);
        } else {
          result = var_h;
        }

        return result;
      }
      let result;

      result = this.each(function () {
        let var_b, var_e;
        var_b = var_f(this);
        var_e = [var_d[0], var_c];
        var_b.triggerHandler("setData" + var_d[1] + "!", var_e);
        var_f.data(this, var_a, var_c);
        var_b.triggerHandler("changeData" + var_d[1] + "!", var_e);
      });

      return result;
    },
    removeData: function (var_a) {
      let result;

      result = this.each(function () {
        var_f.removeData(this, var_a);
      });

      return result;
    },
  });

  var_f.extend({
    _mark: function (var_a, var_b) {
      if (var_a) {
        var_b = (var_b || "fx") + "mark";
        var_f._data(var_a, var_b, (var_f._data(var_a, var_b) || 0) + 1);
      }
    },
    _unmark: function (var_a, var_b, var_c) {
      if (var_a !== true) {
        var_c = var_b;
        var_b = var_a;
        var_a = false;
      }

      if (var_b) {
        if (var_c) {
          var_c = var_c;
        } else {
          var_c = "fx";
        }

        let var_d, var_e;
        var_d = var_c + "mark";

        if (var_a) {
          var_e = 0;
        } else {
          var_e = (var_f._data(var_b, var_d) || 1) - 1;
        }

        if (var_e) {
          var_f._data(var_b, var_d, var_e);
        } else {
          var_f.removeData(var_b, var_d, true);
          var_n(var_b, var_c, "mark");
        }
      }
    },
    queue: function (var_a, var_b, var_c) {
      let var_d;
      if (var_a) {
        var_b = (var_b || "fx") + "queue";
        var_d = var_f._data(var_a, var_b);

        if (var_c) {
          if (!var_d || var_f.isArray(var_c)) {
            var_d = var_f._data(var_a, var_b, var_f.makeArray(var_c));
          } else {
            var_d.push(var_c);
          }
        }

        let result;

        if (var_d) {
          result = var_d;
        } else {
          result = [];
        }

        return result;
      }
    },
    dequeue: function (var_a, var_b) {
      if (var_b) {
        var_b = var_b;
      } else {
        var_b = "fx";
      }

      let var_c, var_d, var_e;
      var_c = var_f.queue(var_a, var_b);
      var_d = var_c.shift();
      var_e = {};

      if (var_d === "inprogress") {
        var_d = var_c.shift();
      }

      if (var_d) {
        if (var_b === "fx") {
          var_c.unshift("inprogress");
        }

        var_f._data(var_a, var_b + ".run", var_e);

        var_d.call(
          var_a,
          function () {
            var_f.dequeue(var_a, var_b);
          },
          var_e
        );
      }

      if (!var_c.length) {
        var_f.removeData(var_a, var_b + "queue " + var_b + ".run", true);
        var_n(var_a, var_b, "queue");
      }
    },
  });

  var_f.fn.extend({
    queue: function (var_a, var_c) {
      if (typeof var_a != "string") {
        var_c = var_a;
        var_a = "fx";
      }

      if (var_c === var_b) {
        let result;
        result = var_f.queue(this[0], var_a);
        return result;
      }
      let result;

      result = this.each(function () {
        let var_b;
        var_b = var_f.queue(this, var_a, var_c);

        if (var_a === "fx" && var_b[0] !== "inprogress") {
          var_f.dequeue(this, var_a);
        }
      });

      return result;
    },
    dequeue: function (var_a) {
      let result;

      result = this.each(function () {
        var_f.dequeue(this, var_a);
      });

      return result;
    },
    delay: function (var_a, var_b) {
      if (var_f.fx) {
        if (var_f.fx.speeds[var_a]) {
          var_a = var_f.fx.speeds[var_a];
        } else {
          var_a = var_a;
        }
      } else {
        var_a = var_a;
      }

      if (var_b) {
        var_b = var_b;
      } else {
        var_b = "fx";
      }

      let result;

      result = this.queue(var_b, function (var_b, var_c) {
        let var_d;
        var_d = setTimeout(var_b, var_a);
        var_c.stop = function () {
          clearTimeout(var_d);
        };
      });

      return result;
    },
    clearQueue: function (var_a) {
      let result;
      result = this.queue(var_a || "fx", []);
      return result;
    },
    promise: function (var_a, var_c) {
      function var_m() {
        if (!--var_h) {
          var_d.resolveWith(var_e, [var_e]);
        }
      }

      if (typeof var_a != "string") {
        var_c = var_a;
        var_a = var_b;
      }

      if (var_a) {
        var_a = var_a;
      } else {
        var_a = "fx";
      }

      let var_d, var_e, var_g, var_h, var_i, var_j, var_k, var_l;
      var_d = var_f.Deferred();
      var_e = this;
      var_g = var_e.length;
      var_h = 1;
      var_i = var_a + "defer";
      var_j = var_a + "queue";
      var_k = var_a + "mark";
      while (var_g--) {
        if (var_l =
            var_f.data(var_e[var_g], var_i, var_b, true) ||
            ((var_f.data(var_e[var_g], var_j, var_b, true) || var_f.data(var_e[var_g], var_k, var_b, true)) &&
              var_f.data(var_e[var_g], var_i, var_f.Callbacks("once memory"), true))) {
          var_h++;
          var_l.add(var_m);
        }
      }
      var_m();
      let result;
      result = var_d.promise();
      return result;
    },
  });

  let var_o, var_p, var_q, var_r, var_s, var_t, var_u, var_v, var_w, var_x, var_y;
  var_o = /[\n\t\r]/g;
  var_p = /\s+/;
  var_q = /\r/g;
  var_r = /^(?:button|input)$/i;
  var_s = /^(?:button|input|object|select|textarea)$/i;
  var_t = /^a(?:rea)?$/i;
  var_u = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i;
  var_v = var_f.support.getSetAttribute;

  var_f.fn.extend({
    attr: function (var_a, var_b) {
      let result;
      result = var_f.access(this, var_a, var_b, true, var_f.attr);
      return result;
    },
    removeAttr: function (var_a) {
      let result;

      result = this.each(function () {
        var_f.removeAttr(this, var_a);
      });

      return result;
    },
    prop: function (var_a, var_b) {
      let result;
      result = var_f.access(this, var_a, var_b, true, var_f.prop);
      return result;
    },
    removeProp: function (var_a) {
      if (var_f.propFix[var_a]) {
        var_a = var_f.propFix[var_a];
      } else {
        var_a = var_a;
      }

      let result;

      result = this.each(function () {
        try {
          this[var_a] = var_b;
          delete this[var_a];
        } catch (var_c) { }
      });

      return result;
    },
    addClass: function (var_a) {
      let var_b, var_c, var_d, var_e, var_g, var_h, var_i;
      if (var_f.isFunction(var_a)) {
        let result;

        result = this.each(function (var_b) {
          var_f(this).addClass(var_a.call(this, var_b, this.className));
        });

        return result;
      }
      if (var_a && typeof var_a == "string") {
        var_b = var_a.split(var_p);
        for (var_c = 0, var_d = this.length; var_c < var_d; var_c++) {
          var_e = this[var_c];
          if (var_e.nodeType === 1) {
            if (!var_e.className && var_b.length === 1) {
              var_e.className = var_a;
            } else {
              var_g = " " + var_e.className + " ";
              for (var_h = 0, var_i = var_b.length; var_h < var_i; var_h++) {
                if (!~var_g.indexOf(" " + var_b[var_h] + " ")) {
                  var_g += var_b[var_h] + " ";
                }
              }
              var_e.className = var_f.trim(var_g);
            }
          }
        }
      }
      let result;
      result = this;
      return result;
    },
    removeClass: function (var_a) {
      let var_c, var_d, var_e, var_g, var_h, var_i, var_j;
      if (var_f.isFunction(var_a)) {
        let result;

        result = this.each(function (var_b) {
          var_f(this).removeClass(var_a.call(this, var_b, this.className));
        });

        return result;
      }
      if ((var_a && typeof var_a == "string") || var_a === var_b) {
        var_c = (var_a || "").split(var_p);
        for (var_d = 0, var_e = this.length; var_d < var_e; var_d++) {
          var_g = this[var_d];
          if (var_g.nodeType === 1 && var_g.className) {
            if (var_a) {
              var_h = (" " + var_g.className + " ").replace(var_o, " ");
              for (var_i = 0, var_j = var_c.length; var_i < var_j; var_i++) {
                var_h = var_h.replace(" " + var_c[var_i] + " ", " ");
              }
              var_g.className = var_f.trim(var_h);
            } else {
              var_g.className = "";
            }
          }
        }
      }
      let result;
      result = this;
      return result;
    },
    toggleClass: function (var_a, var_b) {
      let var_c, var_d;
      var_c = typeof var_a;
      var_d = typeof var_b == "boolean";
      if (var_f.isFunction(var_a)) {
        let result;

        result = this.each(function (var_c) {
          var_f(this).toggleClass(var_a.call(this, var_c, this.className, var_b), var_b);
        });

        return result;
      }
      let result;

      result = this.each(function () {
        if (var_c === "string") {
          let var_e, var_g, var_h, var_i, var_j;
          var_g = 0;
          var_h = var_f(this);
          var_i = var_b;
          var_j = var_a.split(var_p);
          while (var_e = var_j[var_g++]) {
            if (var_d) {
              var_i = var_i;
            } else {
              var_i = !var_h.hasClass(var_e);
            }

            var_h[var_i ? "addClass" : "removeClass"](var_e);
          }
        } else {
          if (var_c === "undefined" || var_c === "boolean") {
            if (this.className) {
              var_f._data(this, "__className__", this.className);
            }

            if (this.className || var_a === false) {
              this.className = "";
            } else {
              if (var_f._data(this, "__className__")) {
                this.className = var_f._data(this, "__className__");
              } else {
                this.className = "";
              }
            }
          }
        }
      });

      return result;
    },
    hasClass: function (var_a) {
      let var_b, var_c, var_d;
      var_b = " " + var_a + " ";
      var_c = 0;
      var_d = this.length;
      for (; var_c < var_d; var_c++) {
        if (this[var_c].nodeType === 1 &&
        (" " + this[var_c].className + " ").replace(var_o, " ").indexOf(var_b) > false) {
          return true;
        }
      }
      return false;
    },
    val: function (var_a) {
      let var_c, var_d, var_e, var_g;
      var_g = this[0];
      if (!arguments.length) {
        if (var_g) {
          if (var_f.valHooks[var_g.nodeName.toLowerCase()]) {
            var_c = var_f.valHooks[var_g.nodeName.toLowerCase()];
          } else {
            var_c = var_f.valHooks[var_g.type];
          }

          if (var_c && "get" in var_c && (var_d = var_c.get(var_g, "value")) !== var_b) {
            return var_d;
          }
          var_d = var_g.value;
          let result;

          if (typeof var_d == "string") {
            result = var_d.replace(var_q, "");
          } else {
            if (var_d == null) {
              result = "";
            } else {
              result = var_d;
            }
          }

          return result;
        }
        return var_b;
      }
      var_e = var_f.isFunction(var_a);
      let result;

      result = this.each(function (var_d) {
        let var_g, var_h;
        var_g = var_f(this);
        if (this.nodeType === 1) {
          if (var_e) {
            var_h = var_a.call(this, var_d, var_g.val());
          } else {
            var_h = var_a;
          }

          if (var_h == null) {
            var_h = "";
          } else {
            if (typeof var_h == "number") {
              var_h += "";
            } else {
              if (var_f.isArray(var_h)) {
                var_h = var_f.map(var_h, function (var_a) {
                      return var_a == null ? "" : var_a + "";
                    });
              }
            }
          }

          if (var_f.valHooks[this.nodeName.toLowerCase()]) {
            var_c = var_f.valHooks[this.nodeName.toLowerCase()];
          } else {
            var_c = var_f.valHooks[this.type];
          }

          if (!var_c || !("set" in var_c) || var_c.set(this, var_h, "value") === var_b) {
            this.value = var_h;
          }
        }
      });

      return result;
    },
  });

  var_f.extend({
    valHooks: {
      option: {
        get: function (var_a) {
          let var_b;
          var_b = var_a.attributes.value;
          let result;

          if (!var_b || var_b.specified) {
            result = var_a.value;
          } else {
            result = var_a.text;
          }

          return result;
        },
      },
      select: {
        get: function (var_a) {
          let var_b, var_c, var_d, var_e, var_g, var_h, var_i, var_j;
          var_g = var_a.selectedIndex;
          var_h = [];
          var_i = var_a.options;
          var_j = var_a.type === "select-one";
          if (var_g < 0) {
            return null;
          }

          if (var_j) {
            var_c = var_g;
          } else {
            var_c = 0;
          }

          if (var_j) {
            var_d = var_g + 1;
          } else {
            var_d = var_i.length;
          }

          for (; var_c < var_d; var_c++) {
            var_e = var_i[var_c];
            if (
              var_e.selected &&
              (var_f.support.optDisabled
                ? !var_e.disabled
                : var_e.getAttribute("disabled") === null) &&
              (!var_e.parentNode.disabled ||
                !var_f.nodeName(var_e.parentNode, "optgroup"))
            ) {
              var_b = var_f(var_e).val();
              if (var_j) {
                return var_b;
              }
              var_h.push(var_b);
            }
          }
          if (var_j && !var_h.length && var_i.length) {
            let result;
            result = var_f(var_i[var_g]).val();
            return result;
          }
          return var_h;
        },
        set: function (var_a, var_b) {
          let var_c;
          var_c = var_f.makeArray(var_b);

          var_f(var_a)
            .find("option")
            .each(function () {
              this.selected = var_f.inArray(var_f(this).val(), var_c) >= 0;
            });

          if (!var_c.length) {
            var_a.selectedIndex = false;
          }

          return var_c;
        },
      },
    },
    attrFn: {
      val: true,
      css: true,
      html: true,
      text: true,
      data: true,
      width: true,
      height: true,
      offset: true,
    },
    attr: function (var_a, var_c, var_d, var_e) {
      let var_g, var_h, var_i, var_j;
      var_j = var_a.nodeType;
      if (!var_a || var_j === 3 || var_j === 8 || var_j === 2) {
        return var_b;
      }
      if (var_e && var_c in var_f.attrFn) {
        let result;
        result = var_f(var_a)[var_c](var_d);
        return result;
      }
      if (!("getAttribute" in var_a)) {
        let result;
        result = var_f.prop(var_a, var_c, var_d);
        return result;
      }

      if (var_j !== 1) {
        var_i = var_j !== 1;
      } else {
        var_i = !var_f.isXMLDoc(var_a);
      }

      if (var_i) {
        var_c = var_c.toLowerCase();

        if (var_f.attrHooks[var_c]) {
          var_h = var_f.attrHooks[var_c];
        } else {
          if (var_u.test(var_c)) {
            var_h = var_x;
          } else {
            var_h = var_w;
          }
        }
      }

      if (var_d !== var_b) {
        if (var_d === null) {
          var_f.removeAttr(var_a, var_c);
          return var_b;
        }
        if (var_h && "set" in var_h && var_i && (var_g = var_h.set(var_a, var_d, var_c)) !== var_b) {
          return var_g;
        }
        var_a.setAttribute(var_c, "" + var_d);
        return var_d;
      }
      if (var_h && "get" in var_h && var_i && (var_g = var_h.get(var_a, var_c)) !== null) {
        return var_g;
      }
      var_g = var_a.getAttribute(var_c);
      let result;

      if (var_g === null) {
        result = var_b;
      } else {
        result = var_g;
      }

      return result;
    },
    removeAttr: function (var_a, var_b) {
      let var_c, var_d, var_e, var_g, var_h;
      var_h = 0;
      if (var_a.nodeType === 1) {
        var_d = (var_b || "").split(var_p);
        var_g = var_d.length;
        for (; var_h < var_g; var_h++) {
          var_e = var_d[var_h].toLowerCase();

          if (var_f.propFix[var_e]) {
            var_c = var_f.propFix[var_e];
          } else {
            var_c = var_e;
          }

          var_f.attr(var_a, var_e, "");
          var_a.removeAttribute(var_v ? var_e : var_c);

          if (var_u.test(var_e) && var_c in var_a) {
            var_a[var_c] = false;
          }
        }
      }
    },
    attrHooks: {
      type: {
        set: function (var_a, var_b) {
          if (var_r.test(var_a.nodeName) && var_a.parentNode) {
            var_f.error("type property can't be changed");
          } else {
            if (
              !var_f.support.radioValue &&
              var_b === "radio" &&
              var_f.nodeName(var_a, "input")
            ) {
              let var_c;
              var_c = var_a.value;
              var_a.setAttribute("type", var_b);

              if (var_c) {
                var_a.value = var_c;
              }

              return var_b;
            }
          }
        },
      },
      value: {
        get: function (var_a, var_b) {
          if (var_w && var_f.nodeName(var_a, "button")) {
            let result;
            result = var_w.get(var_a, var_b);
            return result;
          }
          let result;

          if (var_b in var_a) {
            result = var_a.value;
          } else {
            result = null;
          }

          return result;
        },
        set: function (var_a, var_b, var_c) {
          if (var_w && var_f.nodeName(var_a, "button")) {
            let result;
            result = var_w.set(var_a, var_b, var_c);
            return result;
          }
          var_a.value = var_b;
        },
      },
    },
    propFix: {
      tabindex: "tabIndex",
      readonly: "readOnly",
      for: "htmlFor",
      class: "className",
      maxlength: "maxLength",
      cellspacing: "cellSpacing",
      cellpadding: "cellPadding",
      rowspan: "rowSpan",
      colspan: "colSpan",
      usemap: "useMap",
      frameborder: "frameBorder",
      contenteditable: "contentEditable",
    },
    prop: function (var_a, var_c, var_d) {
      let var_e, var_g, var_h, var_i;
      var_i = var_a.nodeType;
      if (!var_a || var_i === 3 || var_i === 8 || var_i === 2) {
        return var_b;
      }

      if (var_i !== 1) {
        var_h = var_i !== 1;
      } else {
        var_h = !var_f.isXMLDoc(var_a);
      }

      if (var_h) {
        if (var_f.propFix[var_c]) {
          var_c = var_f.propFix[var_c];
        } else {
          var_c = var_c;
        }

        var_g = var_f.propHooks[var_c];
      }

      let result;

      if (var_d !== var_b) {
        if (var_g && "set" in var_g && (var_e = var_g.set(var_a, var_d, var_c)) !== var_b) {
          result = var_e;
        } else {
          result = var_a[var_c] = var_d;
        }
      } else {
        if (var_g && "get" in var_g && (var_e = var_g.get(var_a, var_c)) !== null) {
          result = var_e;
        } else {
          result = var_a[var_c];
        }
      }

      return result;
    },
    propHooks: {
      tabIndex: {
        get: function (var_a) {
          let var_c;
          var_c = var_a.getAttributeNode("tabindex");
          let result;

          if (var_c && var_c.specified) {
            result = parseInt(var_c.value, 10);
          } else {
            if (var_s.test(var_a.nodeName) || (var_t.test(var_a.nodeName) && var_a.href)) {
              result = 0;
            } else {
              result = var_b;
            }
          }

          return result;
        },
      },
    },
  });

  var_f.attrHooks.tabindex = var_f.propHooks.tabIndex;

  var_x = {
      get: function (var_a, var_c) {
        let var_d, var_e;
        var_e = var_f.prop(var_a, var_c);
        let result;

        if (var_e === true ||
          (typeof var_e != "boolean" &&
            (var_d = var_a.getAttributeNode(var_c)) &&
            var_d.nodeValue !== false)) {
          result = var_c.toLowerCase();
        } else {
          result = var_b;
        }

        return result;
      },
      set: function (var_a, var_b, var_c) {
        let var_d;

        if (var_b === false) {
          var_f.removeAttr(var_a, var_c);
        } else {
          if (var_f.propFix[var_c]) {
            var_d = var_f.propFix[var_c];
          } else {
            var_d = var_c;
          }

          if (var_d in var_a) {
            var_a[var_d] = true;
          }

          var_a.setAttribute(var_c, var_c.toLowerCase());
        }

        return var_c;
      },
    };

  if (!var_v) {
    var_y = {
      name: true,
      id: true,
    };

    var_w = var_f.valHooks.button =
      {
        get: function (var_a, var_c) {
          let var_d;
          var_d = var_a.getAttributeNode(var_c);
          let result;

          if (var_d && (var_y[var_c] ? var_d.nodeValue !== "" : var_d.specified)) {
            result = var_d.nodeValue;
          } else {
            result = var_b;
          }

          return result;
        },
        set: function (var_a, var_b, var_d) {
          let var_e;
          var_e = var_a.getAttributeNode(var_d);

          if (!var_e) {
            var_e = var_c.createAttribute(var_d);
            var_a.setAttributeNode(var_e);
          }

          let result;
          result = var_e.nodeValue = var_b + "";
          return result;
        },
      };

    var_f.attrHooks.tabindex.set = var_w.set;

    var_f.each(["width", "height"], function (var_a, var_b) {
      var_f.attrHooks[var_b] = var_f.extend(var_f.attrHooks[var_b], {
        set: function (var_a, var_c) {
          if (var_c === "") {
            var_a.setAttribute(var_b, "auto");
            return var_c;
          }
        },
      });
    });

    var_f.attrHooks.contenteditable = {
        get: var_w.get,
        set: function (var_a, var_b, var_c) {
          if (var_b === "") {
            var_b = "false";
          }

          var_w.set(var_a, var_b, var_c);
        },
      };
  }

  if (!var_f.support.hrefNormalized) {
    var_f.each(["href", "src", "width", "height"], function (var_a, var_c) {
      var_f.attrHooks[var_c] = var_f.extend(var_f.attrHooks[var_c], {
        get: function (var_a) {
          let var_d;
          var_d = var_a.getAttribute(var_c, 2);
          let result;

          if (var_d === null) {
            result = var_b;
          } else {
            result = var_d;
          }

          return result;
        },
      });
    });
  }

  if (!var_f.support.style) {
    var_f.attrHooks.style = {
      get: function (var_a) {
        let result;

        if (var_a.style.cssText.toLowerCase()) {
          result = var_a.style.cssText.toLowerCase();
        } else {
          result = var_b;
        }

        return result;
      },
      set: function (var_a, var_b) {
        let result;
        result = var_a.style.cssText = "" + var_b;
        return result;
      },
    };
  }

  if (!var_f.support.optSelected) {
    var_f.propHooks.selected = var_f.extend(var_f.propHooks.selected, {
      get: function (var_a) {
        let var_b;
        var_b = var_a.parentNode;

        if (var_b) {
          var_b.selectedIndex;

          if (var_b.parentNode) {
            var_b.parentNode.selectedIndex;
          }
        }

        return null;
      },
    });
  }

  if (!var_f.support.enctype) {
    var_f.propFix.enctype = "encoding";
  }

  if (!var_f.support.checkOn) {
    var_f.each(["radio", "checkbox"], function () {
      var_f.valHooks[this] = {
        get: function (var_a) {
          let result;

          if (var_a.getAttribute("value") === null) {
            result = "on";
          } else {
            result = var_a.value;
          }

          return result;
        },
      };
    });
  }

  var_f.each(["radio", "checkbox"], function () {
    var_f.valHooks[this] = var_f.extend(var_f.valHooks[this], {
      set: function (var_a, var_b) {
        if (var_f.isArray(var_b)) {
          let result;
          result = var_a.checked = var_f.inArray(var_f(var_a).val(), var_b) >= 0;
          return result;
        }
      },
    });
  });

  let var_z, var_A, var_B, var_C, var_D, var_E, var_F, var_G, var_H, var_I, var_J, var_K, var_L;
  var_z = /\.(.*)$/;
  var_A = /^(?:textarea|input|select)$/i;
  var_B = /\./g;
  var_C = / /g;
  var_D = /[^\w\s.|`]/g;
  var_E = /^([^\.]*)?(?:\.(.+))?$/;
  var_F = /\bhover(\.\S+)?/;
  var_G = /^key/;
  var_H = /^(?:mouse|contextmenu)|click/;
  var_I = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/;

  var_J = function (var_a) {
    let var_b;
    var_b = var_I.exec(var_a);

    if (var_b) {
      var_b[1] = (var_b[1] || "").toLowerCase();

      if (var_b[3]) {
        var_b[3] = new RegExp("(?:^|\\s)" + var_b[3] + "(?:\\s|$)");
      } else {
        var_b[3] = var_b[3];
      }
    }

    return var_b;
  };

  var_K = function (var_a, var_b) {
    let result;

    if ((!var_b[1] || var_a.nodeName.toLowerCase() === var_b[1]) &&
    (!var_b[2] || var_a.id === var_b[2])) {
      if (!var_b[3]) {
        result = !var_b[3];
      } else {
        result = var_b[3].test(var_a.className);
      }
    } else {
      if (!var_b[1] || var_a.nodeName.toLowerCase() === var_b[1]) {
        if (!var_b[2]) {
          result = !var_b[2];
        } else {
          result = var_a.id === var_b[2];
        }
      } else {
        if (!var_b[1]) {
          result = !var_b[1];
        } else {
          result = var_a.nodeName.toLowerCase() === var_b[1];
        }
      }
    }

    return result;
  };

  var_L = function (var_a) {
    let result;

    if (var_f.event.special.hover) {
      result = var_a;
    } else {
      result = var_a.replace(var_F, "mouseenter$1 mouseleave$1");
    }

    return result;
  };

  var_f.event = {
    add: function (var_a, var_c, var_d, var_e, var_g) {
      let var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p, var_q, var_r, var_s;
      if (
        !(var_a.nodeType === 3 || var_a.nodeType === 8 || !var_c || !var_d || !(var_h = var_f._data(var_a)))
      ) {
        if (var_d.handler) {
          var_p = var_d;
          var_d = var_p.handler;
        }

        if (!var_d.guid) {
          var_d.guid = var_f.guid++;
        }

        var_j = var_h.events;

        if (!var_j) {
          var_h.events = var_j = {};
        }

        var_i = var_h.handle;

        if (!var_i) {
          var_h.handle = var_i =
            function (var_a) {
              let result;

              if (typeof var_f != "undefined" &&
                (!var_a || var_f.event.triggered !== var_a.type)) {
                result = var_f.event.dispatch.apply(var_i.elem, arguments);
              } else {
                result = var_b;
              }

              return result;
            };

          var_i.elem = var_a;
        }

        var_c = var_L(var_c).split(" ");
        for (var_k = 0; var_k < var_c.length; var_k++) {
          if (var_E.exec(var_c[var_k])) {
            var_l = var_E.exec(var_c[var_k]);
          } else {
            var_l = [];
          }

          var_m = var_l[1];
          var_n = (var_l[2] || "").split(".").sort();

          if (var_f.event.special[var_m]) {
            var_s = var_f.event.special[var_m];
          } else {
            var_s = {};
          }

          if (var_g ? var_s.delegateType : var_s.bindType) {
            if (var_g) {
              var_m = var_s.delegateType;
            } else {
              var_m = var_s.bindType;
            }
          } else {
            var_m = var_m;
          }

          if (var_f.event.special[var_m]) {
            var_s = var_f.event.special[var_m];
          } else {
            var_s = {};
          }

          var_o = var_f.extend(
              {
                type: var_m,
                origType: var_l[1],
                data: var_e,
                handler: var_d,
                guid: var_d.guid,
                selector: var_g,
                namespace: var_n.join("."),
              },
              var_p
            );

          if (var_g) {
            var_o.quick = var_J(var_g);

            if (!var_o.quick && var_f.expr.match.POS.test(var_g)) {
              var_o.isPositional = true;
            }
          }

          var_r = var_j[var_m];
          if (!var_r) {
            var_r = var_j[var_m] = [];
            var_r.delegateCount = 0;
            if (!var_s.setup || var_s.setup.call(var_a, var_e, var_n, var_i) === false) {
              if (var_a.addEventListener) {
                var_a.addEventListener(var_m, var_i, false);
              } else {
                if (var_a.attachEvent) {
                  var_a.attachEvent("on" + var_m, var_i);
                }
              }
            }
          }

          if (var_s.add) {
            var_s.add.call(var_a, var_o);

            if (!var_o.handler.guid) {
              var_o.handler.guid = var_d.guid;
            }
          }

          if (var_g) {
            var_r.splice(var_r.delegateCount++, 0, var_o);
          } else {
            var_r.push(var_o);
          }

          var_f.event.global[var_m] = true;
        }
        var_a = null;
      }
    },
    global: {},
    remove: function (var_a, var_b, var_c, var_d) {
      let var_e, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p, var_q;

      if (var_f.hasData(var_a)) {
        var_e = var_f._data(var_a);
      } else {
        var_e = var_f.hasData(var_a);
      }

      if (!!var_e && !!(var_m = var_e.events)) {
        var_b = var_L(var_b || "").split(" ");
        for (var_g = 0; var_g < var_b.length; var_g++) {
          if (var_E.exec(var_b[var_g])) {
            var_h = var_E.exec(var_b[var_g]);
          } else {
            var_h = [];
          }

          var_i = var_h[1];
          var_j = var_h[2];
          if (!var_i) {
            if (var_j) {
              var_j = "." + var_j;
            } else {
              var_j = "";
            }

            for (var_l in var_m) {
              var_f.event.remove(var_a, var_l + var_j, var_c, var_d);
            }
            return;
          }

          if (var_f.event.special[var_i]) {
            var_n = var_f.event.special[var_i];
          } else {
            var_n = {};
          }

          if (var_d ? var_n.delegateType : var_n.bindType) {
            if (var_d) {
              var_i = var_n.delegateType;
            } else {
              var_i = var_n.bindType;
            }
          } else {
            var_i = var_i;
          }

          if (var_m[var_i]) {
            var_p = var_m[var_i];
          } else {
            var_p = [];
          }

          var_k = var_p.length;

          if (var_j) {
            var_j = new RegExp(
                  "(^|\\.)" +
                  var_j.split(".").sort().join("\\.(?:.*\\.)?") +
                  "(\\.|$)"
                );
          } else {
            var_j = null;
          }

          if (var_c || var_j || var_d || var_n.remove) {
            for (var_l = 0; var_l < var_p.length; var_l++) {
              var_q = var_p[var_l];
              if (!var_c || var_c.guid === var_q.guid) {
                if (!var_j || var_j.test(var_q.namespace)) {
                  if (!var_d || var_d === var_q.selector || (var_d === "**" && var_q.selector)) {
                    var_p.splice(var_l--, 1);

                    if (var_q.selector) {
                      var_p.delegateCount--;
                    }

                    if (var_n.remove) {
                      var_n.remove.call(var_a, var_q);
                    }
                  }
                }
              }
            }
          } else {
            var_p.length = 0;
          }

          if (var_p.length === 0 &&
            var_k !== var_p.length) {
            if (!var_n.teardown || var_n.teardown.call(var_a, var_j) === false) {
              var_f.removeEvent(var_a, var_i, var_e.handle);
            }

            delete var_m[var_i];
          }
        }

        if (var_f.isEmptyObject(var_m)) {
          var_o = var_e.handle;

          if (var_o) {
            var_o.elem = null;
          }

          var_f.removeData(var_a, ["events", "handle"], true);
        }
      }
    },
    customEvent: {
      getData: true,
      setData: true,
      changeData: true,
    },
    trigger: function (var_c, var_d, var_e, var_g) {
      if (!var_e || (var_e.nodeType !== 3 && var_e.nodeType !== 8)) {
        let var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p, var_q, var_r, var_s;

        if (var_c.type) {
          var_h = var_c.type;
        } else {
          var_h = var_c;
        }

        var_i = [];

        if (var_h.indexOf("!") >= 0) {
          var_h = var_h.slice(0, false);
          var_k = true;
        }

        if (var_h.indexOf(".") >= 0) {
          var_i = var_h.split(".");
          var_h = var_i.shift();
          var_i.sort();
        }

        if ((!var_e || var_f.event.customEvent[var_h]) && !var_f.event.global[var_h]) {
          return;
        }

        if (typeof var_c == "object") {
          if (var_c[var_f.expando]) {
            var_c = var_c;
          } else {
            var_c = new var_f.Event(var_h, var_c);
          }
        } else {
          var_c = new var_f.Event(var_h);
        }

        var_c.type = var_h;
        var_c.isTrigger = true;
        var_c.exclusive = var_k;
        var_c.namespace = var_i.join(".");

        if (var_c.namespace) {
          var_c.namespace_re = new RegExp("(^|\\.)" + var_i.join("\\.(?:.*\\.)?") + "(\\.|$)");
        } else {
          var_c.namespace_re = null;
        }

        if (var_h.indexOf(":") < 0) {
          var_o = "on" + var_h;
        } else {
          var_o = "";
        }

        if (var_g || !var_e) {
          var_c.preventDefault();
        }

        if (!var_e) {
          var_j = var_f.cache;
          for (var_l in var_j) {
            if (var_j[var_l].events &&
              var_j[var_l].events[var_h]) {
              var_f.event.trigger(var_c, var_d, var_j[var_l].handle.elem, true);
            }
          }
          return;
        }
        var_c.result = var_b;

        if (!var_c.target) {
          var_c.target = var_e;
        }

        if (var_d != null) {
          var_d = var_f.makeArray(var_d);
        } else {
          var_d = [];
        }

        var_d.unshift(var_c);

        if (var_f.event.special[var_h]) {
          var_p = var_f.event.special[var_h];
        } else {
          var_p = {};
        }

        if (var_p.trigger && var_p.trigger.apply(var_e, var_d) === false) {
          return;
        }
        var_r = [[var_e, var_p.bindType || var_h]];
        if (!var_g && !var_p.noBubble && !var_f.isWindow(var_e)) {
          if (var_p.delegateType) {
            var_s = var_p.delegateType;
          } else {
            var_s = var_h;
          }

          var_n = null;
          for (var_m = var_e.parentNode; var_m; var_m = var_m.parentNode) {
            var_r.push([var_m, var_s]);
            var_n = var_m;
          }

          if (var_n &&
            var_n === var_e.ownerDocument) {
            var_r.push([var_n.defaultView || var_n.parentWindow || var_a, var_s]);
          }
        }
        for (var_l = 0; var_l < var_r.length; var_l++) {
          var_m = var_r[var_l][0];
          var_c.type = var_r[var_l][1];

          if ((var_f._data(var_m, "events") || {})[var_c.type]) {
            var_q = var_f._data(var_m, "handle");
          } else {
            var_q = (var_f._data(var_m, "events") || {})[var_c.type];
          }

          if (var_q) {
            var_q.apply(var_m, var_d);
          }

          if (var_o) {
            var_q = var_m[var_o];
          } else {
            var_q = var_o;
          }

          if (var_q && var_f.acceptData(var_m)) {
            var_q.apply(var_m, var_d);
          }

          if (var_c.isPropagationStopped()) {
            break;
          }
        }
        var_c.type = var_h;

        if (!var_c.isDefaultPrevented()) {
          if ((!var_p._default || var_p._default.apply(var_e.ownerDocument, var_d) === false) &&
            (var_h !== "click" || !var_f.nodeName(var_e, "a")) &&
            var_f.acceptData(var_e) &&
            var_o &&
            var_e[var_h] &&
            ((var_h !== "focus" && var_h !== "blur") || var_c.target.offsetWidth !== 0) &&
            !var_f.isWindow(var_e)) {
            var_n = var_e[var_o];

            if (var_n) {
              var_e[var_o] = null;
            }

            var_f.event.triggered = var_h;
            var_e[var_h]();
            var_f.event.triggered = var_b;

            if (var_n) {
              var_e[var_o] = var_n;
            }
          }
        }

        let result;
        result = var_c.result;
        return result;
      }
    },
    dispatch: function (var_c) {
      var_c = var_f.event.fix(var_c || var_a.event);
      let var_d, var_e, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p, var_q, var_r, var_s, var_t, var_u;

      if ((var_f._data(this, "events") || {})[var_c.type]) {
        var_d = (var_f._data(this, "events") || {})[var_c.type];
      } else {
        var_d = [];
      }

      var_e = var_d.delegateCount;
      var_g = [].slice.call(arguments, 0);

      if (!var_c.exclusive) {
        var_h = !var_c.namespace;
      } else {
        var_h = !var_c.exclusive;
      }

      var_i = (var_f.event.special[var_c.type] || {}).handle;
      var_j = [];
      var_g[0] = var_c;
      var_c.delegateTarget = this;
      if (var_e && !var_c.target.disabled && (!var_c.button || var_c.type !== "click")) {
        for (var_m = var_c.target; var_m != this; var_m = var_m.parentNode || this) {
          var_o = {};
          var_q = [];
          for (var_k = 0; var_k < var_e; var_k++) {
            var_r = var_d[var_k];
            var_s = var_r.selector;
            var_t = var_o[var_s];

            if (var_r.isPositional) {
              var_t = (var_t || (var_o[var_s] = var_f(var_s))).index(var_m) >= 0;
            } else {
              if (var_t === var_b) {
                var_t = var_o[var_s] = var_r.quick ? var_K(var_m, var_r.quick) : var_f(var_m).is(var_s);
              }
            }

            if (var_t) {
              var_q.push(var_r);
            }
          }

          if (var_q.length) {
            var_j.push({
              elem: var_m,
              matches: var_q,
            });
          }
        }
      }

      if (var_d.length > var_e) {
        var_j.push({
          elem: this,
          matches: var_d.slice(var_e),
        });
      }

      for (var_k = 0; var_k < var_j.length && !var_c.isPropagationStopped(); var_k++) {
        var_p = var_j[var_k];
        var_c.currentTarget = var_p.elem;
        for (
          var_l = 0;
          var_l < var_p.matches.length && !var_c.isImmediatePropagationStopped();
          var_l++
        ) {
          var_r = var_p.matches[var_l];
          if (var_h ||
          (!var_c.namespace && !var_r.namespace) ||
          (var_c.namespace_re && var_c.namespace_re.test(var_r.namespace))) {
            var_c.data = var_r.data;
            var_c.handleObj = var_r;
            var_n = (var_i || var_r.handler).apply(var_p.elem, var_g);

            if (var_n !== var_b) {
              var_c.result = var_n;

              if (var_n === false) {
                var_c.preventDefault();
                var_c.stopPropagation();
              }
            }
          }
        }
      }
      let result;
      result = var_c.result;
      return result;
    },
    props:
      "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(
        " "
      ),
    fixHooks: {},
    keyHooks: {
      props: "char charCode key keyCode".split(" "),
      filter: function (var_a, var_b) {
        if (var_a.which == null) {
          if (var_b.charCode != null) {
            var_a.which = var_b.charCode;
          } else {
            var_a.which = var_b.keyCode;
          }
        }

        return var_a;
      },
    },
    mouseHooks: {
      props:
        "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement wheelDelta".split(
          " "
        ),
      filter: function (var_a, var_d) {
        let var_e, var_f, var_g, var_h, var_i;
        var_h = var_d.button;
        var_i = var_d.fromElement;

        if (var_a.pageX == null &&
          var_d.clientX != null) {
          if (var_a.target.ownerDocument) {
            var_e = var_a.target.ownerDocument;
          } else {
            var_e = var_c;
          }

          var_f = var_e.documentElement;
          var_g = var_e.body;

          var_a.pageX =
                var_d.clientX +
                ((var_f && var_f.scrollLeft) || (var_g && var_g.scrollLeft) || 0) -
                ((var_f && var_f.clientLeft) || (var_g && var_g.clientLeft) || 0);

          var_a.pageY =
                var_d.clientY +
                ((var_f && var_f.scrollTop) || (var_g && var_g.scrollTop) || 0) -
                ((var_f && var_f.clientTop) || (var_g && var_g.clientTop) || 0);
        }

        if (!var_a.relatedTarget &&
        var_i) {
          if (var_i === var_a.target) {
            var_a.relatedTarget = var_d.toElement;
          } else {
            var_a.relatedTarget = var_i;
          }
        }

        if (!var_a.which &&
        var_h !== var_b) {
          if (var_h & 1) {
            var_a.which = 1;
          } else {
            if (var_h & 2) {
              var_a.which = 3;
            } else {
              if (var_h & 4) {
                var_a.which = 2;
              } else {
                var_a.which = 0;
              }
            }
          }
        }

        return var_a;
      },
    },
    fix: function (var_a) {
      if (var_a[var_f.expando]) {
        return var_a;
      }
      let var_d, var_e, var_g, var_h, var_i;
      var_g = var_a;

      if (var_f.event.fixHooks[var_a.type]) {
        var_h = var_f.event.fixHooks[var_a.type];
      } else {
        var_h = {};
      }

      if (var_h.props) {
        var_i = this.props.concat(var_h.props);
      } else {
        var_i = this.props;
      }

      var_a = var_f.Event(var_g);
      for (var_d = var_i.length; var_d; ) {
        var_e = var_i[--var_d];
        var_a[var_e] = var_g[var_e];
      }

      if (!var_a.target) {
        if (var_g.srcElement) {
          var_a.target = var_g.srcElement;
        } else {
          var_a.target = var_c;
        }
      }

      if (var_a.target.nodeType === 3) {
        var_a.target = var_a.target.parentNode;
      }

      if (var_a.metaKey === var_b) {
        var_a.metaKey = var_a.ctrlKey;
      }

      let result;

      if (var_h.filter) {
        result = var_h.filter(var_a, var_g);
      } else {
        result = var_a;
      }

      return result;
    },
    special: {
      ready: {
        setup: var_f.bindReady,
      },
      focus: {
        delegateType: "focusin",
        noBubble: true,
      },
      blur: {
        delegateType: "focusout",
        noBubble: true,
      },
      beforeunload: {
        setup: function (var_a, var_b, var_c) {
          if (var_f.isWindow(this)) {
            this.onbeforeunload = var_c;
          }
        },
        teardown: function (var_a, var_b) {
          if (this.onbeforeunload === var_b) {
            this.onbeforeunload = null;
          }
        },
      },
    },
    simulate: function (var_a, var_b, var_c, var_d) {
      let var_e;

      var_e = var_f.extend(new var_f.Event(), var_c, {
        type: var_a,
        isSimulated: true,
        originalEvent: {},
      });

      if (var_d) {
        var_f.event.trigger(var_e, null, var_b);
      } else {
        var_f.event.dispatch.call(var_b, var_e);
      }

      if (var_e.isDefaultPrevented()) {
        var_c.preventDefault();
      }
    },
  };

  var_f.event.handle = var_f.event.dispatch;

  if (var_c.removeEventListener) {
    var_f.removeEvent = function (var_a, var_b, var_c) {
      if (var_a.removeEventListener) {
        var_a.removeEventListener(var_b, var_c, false);
      }
    };
  } else {
    var_f.removeEvent = function (var_a, var_b, var_c) {
      if (var_a.detachEvent) {
        var_a.detachEvent("on" + var_b, var_c);
      }
    };
  }

  var_f.Event = function (var_a, var_b) {
    if (!(this instanceof var_f.Event)) {
      let result;
      result = new var_f.Event(var_a, var_b);
      return result;
    }

    if (var_a && var_a.type) {
      this.originalEvent = var_a;
      this.type = var_a.type;

      if (var_a.defaultPrevented ||
        var_a.returnValue === false ||
        (var_a.getPreventDefault && var_a.getPreventDefault())) {
        this.isDefaultPrevented = func_N;
      } else {
        this.isDefaultPrevented = func_M;
      }
    } else {
      this.type = var_a;
    }

    if (var_b) {
      var_f.extend(this, var_b);
    }

    if (var_a && var_a.timeStamp) {
      if (var_a) {
        this.timeStamp = var_a.timeStamp;
      } else {
        this.timeStamp = var_a;
      }
    } else {
      this.timeStamp = var_f.now();
    }

    this[var_f.expando] = true;
  };

  var_f.Event.prototype = {
      preventDefault: function () {
        this.isDefaultPrevented = func_N;
        let var_a;
        var_a = this.originalEvent;

        if (!!var_a) {
          if (var_a.preventDefault) {
            var_a.preventDefault();
          } else {
            var_a.returnValue = false;
          }
        }
      },
      stopPropagation: function () {
        this.isPropagationStopped = func_N;
        let var_a;
        var_a = this.originalEvent;

        if (!!var_a) {
          if (var_a.stopPropagation) {
            var_a.stopPropagation();
          }

          var_a.cancelBubble = true;
        }
      },
      stopImmediatePropagation: function () {
        this.isImmediatePropagationStopped = func_N;
        this.stopPropagation();
      },
      isDefaultPrevented: func_M,
      isPropagationStopped: func_M,
      isImmediatePropagationStopped: func_M,
    };

  var_f.each(
    {
      mouseenter: "mouseover",
      mouseleave: "mouseout",
    },
    function (var_a, var_b) {
      var_f.event.special[var_a] = var_f.event.special[var_b] = {
        delegateType: var_b,
        bindType: var_b,
        handle: function (var_a) {
          let var_b, var_c, var_d, var_e, var_g, var_h;
          var_b = this;
          var_c = var_a.relatedTarget;
          var_d = var_a.handleObj;
          var_e = var_d.selector;
          if (!var_c || var_d.origType === var_a.type || (var_c !== var_b && !var_f.contains(var_b, var_c))) {
            var_g = var_a.type;
            var_a.type = var_d.origType;
            var_h = var_d.handler.apply(this, arguments);
            var_a.type = var_g;
          }
          return var_h;
        },
      };
    }
  );

  if (!var_f.support.submitBubbles) {
    var_f.event.special.submit = {
      setup: function () {
        if (var_f.nodeName(this, "form")) {
          return false;
        }
        var_f.event.add(this, "click._submit keypress._submit", function (var_a) {
          let var_c, var_d;
          var_c = var_a.target;

          if (var_f.nodeName(var_c, "input") || var_f.nodeName(var_c, "button")) {
            var_d = var_c.form;
          } else {
            var_d = var_b;
          }

          if (var_d &&
            !var_d._submit_attached) {
            var_f.event.add(var_d, "submit._submit", function (var_a) {
              if (this.parentNode) {
                var_f.event.simulate("submit", this.parentNode, var_a, true);
              }
            });

            var_d._submit_attached = true;
          }
        });
      },
      teardown: function () {
        if (var_f.nodeName(this, "form")) {
          return false;
        }
        var_f.event.remove(this, "._submit");
      },
    };
  }

  if (!var_f.support.changeBubbles) {
    var_f.event.special.change = {
      setup: function () {
        if (var_A.test(this.nodeName)) {
          if (this.type === "checkbox" || this.type === "radio") {
            var_f.event.add(this, "propertychange._change", function (var_a) {
              if (var_a.originalEvent.propertyName === "checked") {
                this._just_changed = true;
              }
            });

            var_f.event.add(this, "click._change", function (var_a) {
              if (this._just_changed) {
                this._just_changed = false;
                var_f.event.simulate("change", this, var_a, true);
              }
            });
          }
          return false;
        }
        var_f.event.add(this, "beforeactivate._change", function (var_a) {
          let var_b;
          var_b = var_a.target;

          if (var_A.test(var_b.nodeName) &&
            !var_b._change_attached) {
            var_f.event.add(var_b, "change._change", function (var_a) {
              if (this.parentNode &&
                !var_a.isSimulated) {
                var_f.event.simulate("change", this.parentNode, var_a, true);
              }
            });

            var_b._change_attached = true;
          }
        });
      },
      handle: function (var_a) {
        let var_b;
        var_b = var_a.target;
        if (this !== var_b ||
        var_a.isSimulated ||
        var_a.isTrigger ||
        (var_b.type !== "radio" && var_b.type !== "checkbox")) {
          let result;
          result = var_a.handleObj.handler.apply(this, arguments);
          return result;
        }
      },
      teardown: function () {
        var_f.event.remove(this, "._change");
        let result;
        result = var_A.test(this.nodeName);
        return result;
      },
    };
  }

  if (!var_f.support.focusinBubbles) {
    var_f.each(
      {
        focus: "focusin",
        blur: "focusout",
      },
      function (var_a, var_b) {
        let var_d, var_e;
        var_d = 0;

        var_e = function (var_a) {
          var_f.event.simulate(var_b, var_a.target, var_f.event.fix(var_a), true);
        };

        var_f.event.special[var_b] = {
          setup: function () {
            if (var_d++ === 0) {
              var_c.addEventListener(var_a, var_e, true);
            }
          },
          teardown: function () {
            if (--var_d === 0) {
              var_c.removeEventListener(var_a, var_e, true);
            }
          },
        };
      }
    );
  }

  var_f.fn.extend({
    on: function (var_a, var_c, var_d, var_e, var_g) {
      let var_h, var_i;
      if (typeof var_a == "object") {
        if (typeof var_c != "string") {
          var_d = var_c;
          var_c = var_b;
        }

        for (var_i in var_a) {
          this.on(var_i, var_c, var_d, var_a[var_i], var_g);
        }
        let result;
        result = this;
        return result;
      }

      if (var_d == null && var_e == null) {
        var_e = var_c;
        var_d = var_c = var_b;
      } else {
        if (var_e == null) {
          if (typeof var_c == "string") {
            var_e = var_d;
            var_d = var_b;
          } else {
            var_e = var_d;
            var_d = var_c;
            var_c = var_b;
          }
        }
      }

      if (var_e === false) {
        var_e = func_M;
      } else {
        if (!var_e) {
          let result;
          result = this;
          return result;
        }
      }

      if (var_g === 1) {
        var_h = var_e;

        var_e = function (var_a) {
          var_f().off(var_a);
          let result;
          result = var_h.apply(this, arguments);
          return result;
        };

        if (var_h.guid) {
          var_e.guid = var_h.guid;
        } else {
          var_e.guid = var_h.guid = var_f.guid++;
        }
      }

      let result;

      result = this.each(function () {
        var_f.event.add(this, var_a, var_e, var_d, var_c);
      });

      return result;
    },
    one: function (var_a, var_b, var_c, var_d) {
      let result;
      result = this.on.call(this, var_a, var_b, var_c, var_d, 1);
      return result;
    },
    off: function (var_a, var_c, var_d) {
      if (var_a && var_a.preventDefault && var_a.handleObj) {
        let var_e;
        var_e = var_a.handleObj;
        var_f(var_a.delegateTarget).off(
          var_e.namespace ? var_e.type + "." + var_e.namespace : var_e.type,
          var_e.selector,
          var_e.handler
        );
        let result;
        result = this;
        return result;
      }
      if (typeof var_a == "object") {
        for (var var_g in var_a) {
          this.off(var_g, var_c, var_a[var_g]);
        }
        let result;
        result = this;
        return result;
      }
      if (var_c === false || typeof var_c == "function") {
        var_d = var_c;
        var_c = var_b;
      }

      if (var_d === false) {
        var_d = func_M;
      }

      let result;

      result = this.each(function () {
        var_f.event.remove(this, var_a, var_d, var_c);
      });

      return result;
    },
    bind: function (var_a, var_b, var_c) {
      let result;
      result = this.on(var_a, null, var_b, var_c);
      return result;
    },
    unbind: function (var_a, var_b) {
      let result;
      result = this.off(var_a, null, var_b);
      return result;
    },
    live: function (var_a, var_b, var_c) {
      var_f(this.context).on(var_a, this.selector, var_b, var_c);
      let result;
      result = this;
      return result;
    },
    die: function (var_a, var_b) {
      var_f(this.context).off(var_a, this.selector || "**", var_b);
      let result;
      result = this;
      return result;
    },
    delegate: function (var_a, var_b, var_c, var_d) {
      let result;
      result = this.on(var_b, var_a, var_c, var_d);
      return result;
    },
    undelegate: function (var_a, var_b, var_c) {
      let result;

      if (arguments.length == 1) {
        result = this.off(var_a, "**");
      } else {
        result = this.off(var_b, var_a, var_c);
      }

      return result;
    },
    trigger: function (var_a, var_b) {
      let result;

      result = this.each(function () {
        var_f.event.trigger(var_a, var_b, this);
      });

      return result;
    },
    triggerHandler: function (var_a, var_b) {
      if (this[0]) {
        let result;
        result = var_f.event.trigger(var_a, var_b, this[0], true);
        return result;
      }
    },
    toggle: function (var_a) {
      let var_b, var_c, var_d, var_e;
      var_b = arguments;

      if (var_a.guid) {
        var_c = var_a.guid;
      } else {
        var_c = var_f.guid++;
      }

      var_d = 0;

      var_e = function (var_c) {
        let var_e;
        var_e = (var_f._data(this, "lastToggle" + var_a.guid) || 0) % var_d;
        var_f._data(this, "lastToggle" + var_a.guid, var_e + 1);
        var_c.preventDefault();
        let result;

        if (var_b[var_e].apply(this, arguments)) {
          result = var_b[var_e].apply(this, arguments);
        } else {
          result = false;
        }

        return result;
      };

      var_e.guid = var_c;
      while (var_d < var_b.length) {
        var_b[var_d++].guid = var_c;
      }
      let result;
      result = this.click(var_e);
      return result;
    },
    hover: function (var_a, var_b) {
      let result;
      result = this.mouseenter(var_a).mouseleave(var_b || var_a);
      return result;
    },
  });

  var_f.each(
    "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(
      " "
    ),
    function (var_a, var_b) {
      var_f.fn[var_b] = function (var_a, var_c) {
        if (var_c == null) {
          var_c = var_a;
          var_a = null;
        }

        let result;

        if (arguments.length > 0) {
          result = this.bind(var_b, var_a, var_c);
        } else {
          result = this.trigger(var_b);
        }

        return result;
      };

      if (var_f.attrFn) {
        var_f.attrFn[var_b] = true;
      }

      if (var_G.test(var_b)) {
        var_f.event.fixHooks[var_b] = var_f.event.keyHooks;
      }

      if (var_H.test(var_b)) {
        var_f.event.fixHooks[var_b] = var_f.event.mouseHooks;
      }
    }
  );

  (function () {
    function var_x(var_a, var_b, var_c, var_e, var_f, var_g) {
      for (var var_h = 0, var_i = var_e.length; var_h < var_i; var_h++) {
        let var_j;
        var_j = var_e[var_h];
        if (var_j) {
          let var_k;
          var_k = false;
          var_j = var_j[var_a];
          while (var_j) {
            if (var_j[var_d] === var_c) {
              var_k = var_e[var_j.sizset];
              break;
            }
            if (var_j.nodeType === 1) {
              if (!var_g) {
                var_j[var_d] = var_c;
                var_j.sizset = var_h;
              }

              if (typeof var_b != "string") {
                if (var_j === var_b) {
                  var_k = true;
                  break;
                }
              } else {
                if (var_m.filter(var_b, [var_j]).length > 0) {
                  var_k = var_j;
                  break;
                }
              }
            }
            var_j = var_j[var_a];
          }
          var_e[var_h] = var_k;
        }
      }
    }
    function var_w(var_a, var_b, var_c, var_e, var_f, var_g) {
      for (var var_h = 0, var_i = var_e.length; var_h < var_i; var_h++) {
        let var_j;
        var_j = var_e[var_h];
        if (var_j) {
          let var_k;
          var_k = false;
          var_j = var_j[var_a];
          while (var_j) {
            if (var_j[var_d] === var_c) {
              var_k = var_e[var_j.sizset];
              break;
            }

            if (var_j.nodeType === 1 && !var_g) {
              var_j[var_d] = var_c;
              var_j.sizset = var_h;
            }

            if (var_j.nodeName.toLowerCase() === var_b) {
              var_k = var_j;
              break;
            }
            var_j = var_j[var_a];
          }
          var_e[var_h] = var_k;
        }
      }
    }
    let var_a, var_d, var_e, var_g, var_h, var_i, var_j, var_k, var_l;
    var_a = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g;
    var_d = "sizcache" + (Math.random() + "").replace(".", "");
    var_e = 0;
    var_g = Object.prototype.toString;
    var_h = false;
    var_i = true;
    var_j = /\\/g;
    var_k = /\r\n/g;
    var_l = /\W/;
    [0, 0].sort(function () {
      var_i = false;
      return 0;
    });
    let var_m;

    var_m = function (var_b, var_d, var_e, var_f) {
      if (var_e) {
        var_e = var_e;
      } else {
        var_e = [];
      }

      if (var_d) {
        var_d = var_d;
      } else {
        var_d = var_c;
      }

      let var_h;
      var_h = var_d;
      if (var_d.nodeType !== 1 && var_d.nodeType !== 9) {
        let result;
        result = [];
        return result;
      }
      if (!var_b || typeof var_b != "string") {
        return var_e;
      }
      let var_i, var_j, var_k, var_l, var_n, var_q, var_r, var_t, var_u, var_v, var_w, var_x;
      var_u = true;
      var_v = var_m.isXML(var_d);
      var_w = [];
      var_x = var_b;
      do {
        var_a.exec("");
        var_i = var_a.exec(var_x);
        if (var_i) {
          var_x = var_i[3];
          var_w.push(var_i[1]);
          if (var_i[2]) {
            var_l = var_i[3];
            break;
          }
        }
      } while (var_i);
      if (var_w.length > 1 && var_p.exec(var_b)) {
        if (var_w.length === 2 && var_o.relative[var_w[0]]) {
          var_j = var_y(var_w[0] + var_w[1], var_d, var_f);
        } else {
          if (var_o.relative[var_w[0]]) {
            var_j = [var_d];
          } else {
            var_j = var_m(var_w.shift(), var_d);
          }

          while (var_w.length) {
            var_b = var_w.shift();

            if (var_o.relative[var_b]) {
              var_b += var_w.shift();
            }

            var_j = var_y(var_b, var_j, var_f);
          }
        }
      } else {
        if (!var_f &&
          var_w.length > 1 &&
          var_d.nodeType === 9 &&
          !var_v &&
          var_o.match.ID.test(var_w[0]) &&
          !var_o.match.ID.test(var_w[var_w.length - 1])) {
          var_n = var_m.find(var_w.shift(), var_d, var_v);

          if (var_n.expr) {
            var_d = var_m.filter(var_n.expr, var_n.set)[0];
          } else {
            var_d = var_n.set[0];
          }
        }

        if (var_d) {
          if (var_f) {
            var_n = {
                expr: var_w.pop(),
                set: var_s(var_f),
              };
          } else {
            var_n = var_m.find(
                var_w.pop(),
                var_w.length === 1 &&
                  (var_w[0] === "~" || var_w[0] === "+") &&
                  var_d.parentNode
                  ? var_d.parentNode
                  : var_d,
                var_v
              );
          }

          if (var_n.expr) {
            var_j = var_m.filter(var_n.expr, var_n.set);
          } else {
            var_j = var_n.set;
          }

          if (var_w.length > 0) {
            var_k = var_s(var_j);
          } else {
            var_u = false;
          }

          while (var_w.length) {
            var_q = var_w.pop();
            var_r = var_q;

            if (var_o.relative[var_q]) {
              var_r = var_w.pop();
            } else {
              var_q = "";
            }

            if (var_r == null) {
              var_r = var_d;
            }

            var_o.relative[var_q](var_k, var_r, var_v);
          }
        } else {
          var_k = var_w = [];
        }
      }

      if (!var_k) {
        var_k = var_j;
      }

      if (!var_k) {
        var_m.error(var_q || var_b);
      }

      if (var_g.call(var_k) === "[object Array]") {
        if (!var_u) {
          var_e.push.apply(var_e, var_k);
        } else {
          if (var_d && var_d.nodeType === 1) {
            for (var_t = 0; var_k[var_t] != null; var_t++) {
              if (var_k[var_t] &&
                (var_k[var_t] === true || (var_k[var_t].nodeType === 1 && var_m.contains(var_d, var_k[var_t])))) {
                var_e.push(var_j[var_t]);
              }
            }
          } else {
            for (var_t = 0; var_k[var_t] != null; var_t++) {
              if (var_k[var_t] && var_k[var_t].nodeType === 1) {
                var_e.push(var_j[var_t]);
              }
            }
          }
        }
      } else {
        var_s(var_k, var_e);
      }

      if (var_l) {
        var_m(var_l, var_h, var_e, var_f);
        var_m.uniqueSort(var_e);
      }

      return var_e;
    };

    var_m.uniqueSort = function (var_a) {
      if (var_u) {
        var_h = var_i;
        var_a.sort(var_u);
        if (var_h) {
          for (var var_b = 1; var_b < var_a.length; var_b++) {
            if (var_a[var_b] === var_a[var_b - 1]) {
              var_a.splice(var_b--, 1);
            }
          }
        }
      }
      return var_a;
    };

    var_m.matches = function (var_a, var_b) {
      let result;
      result = var_m(var_a, null, null, var_b);
      return result;
    };

    var_m.matchesSelector = function (var_a, var_b) {
      let result;
      result = var_m(var_b, null, null, [var_a]).length > 0;
      return result;
    };

    var_m.find = function (var_a, var_b, var_c) {
      let var_d, var_e, var_f, var_g, var_h, var_i;
      if (!var_a) {
        let result;
        result = [];
        return result;
      }
      for (var_e = 0, var_f = var_o.order.length; var_e < var_f; var_e++) {
        var_h = var_o.order[var_e];
        if ((var_g = var_o.leftMatch[var_h].exec(var_a))) {
          var_i = var_g[1];
          var_g.splice(1, 1);
          if (var_i.substr(var_i.length - 1) !== "\\") {
            var_g[1] = (var_g[1] || "").replace(var_j, "");
            var_d = var_o.find[var_h](var_g, var_b, var_c);
            if (var_d != null) {
              var_a = var_a.replace(var_o.match[var_h], "");
              break;
            }
          }
        }
      }

      if (!var_d) {
        if (typeof var_b.getElementsByTagName != "undefined") {
          var_d = var_b.getElementsByTagName("*");
        } else {
          var_d = [];
        }
      }

      let result;

      result = {
        set: var_d,
        expr: var_a,
      };

      return result;
    };

    var_m.filter = function (var_a, var_c, var_d, var_e) {
      let var_f, var_g, var_h, var_i, var_j, var_k, var_l, var_n, var_p, var_q, var_r, var_s, var_t;
      var_q = var_a;
      var_r = [];
      var_s = var_c;

      if (var_c && var_c[0]) {
        var_t = var_m.isXML(var_c[0]);
      } else {
        if (var_c) {
          var_t = var_c[0];
        } else {
          var_t = var_c;
        }
      }

      while (var_a && var_c.length) {
        for (var_h in var_o.filter) {
          if ((var_f = var_o.leftMatch[var_h].exec(var_a)) != null && var_f[2]) {
            var_k = var_o.filter[var_h];
            var_l = var_f[1];
            var_g = false;
            var_f.splice(1, 1);
            if (var_l.substr(var_l.length - 1) === "\\") {
              continue;
            }

            if (var_s === var_r) {
              var_r = [];
            }

            if (var_o.preFilter[var_h]) {
              var_f = var_o.preFilter[var_h](var_f, var_s, var_d, var_r, var_e, var_t);
              if (!var_f) {
                var_g = var_i = true;
              } else {
                if (var_f === true) {
                  continue;
                }
              }
            }
            if (var_f) {
              for (var_n = 0; (var_j = var_s[var_n]) != null; var_n++) {
                if (var_j) {
                  var_i = var_k(var_j, var_f, var_n, var_s);
                  var_p = var_e ^ var_i;

                  if (var_d && var_i != null) {
                    if (var_p) {
                      var_g = true;
                    } else {
                      var_s[var_n] = false;
                    }
                  } else {
                    if (var_p) {
                      var_r.push(var_j);
                      var_g = true;
                    }
                  }
                }
              }
            }
            if (var_i !== var_b) {
              if (!var_d) {
                var_s = var_r;
              }

              var_a = var_a.replace(var_o.match[var_h], "");
              if (!var_g) {
                let result;
                result = [];
                return result;
              }
              break;
            }
          }
        }
        if (var_a === var_q) {
          if (var_g == null) {
            var_m.error(var_a);
          } else {
            break;
          }
        }
        var_q = var_a;
      }
      return var_s;
    };

    var_m.error = function (var_a) {
        throw "Syntax error, unrecognized expression: " + var_a;
      };

    let var_n, var_o, var_p, var_q;

    var_n = var_m.getText = function (var_a) {
      let var_b, var_c, var_d, var_e;
      var_d = var_a.nodeType;
      var_e = "";
      if (var_d) {
        if (var_d === 1) {
          if (typeof var_a.textContent == "string") {
            let result;
            result = var_a.textContent;
            return result;
          }
          if (typeof var_a.innerText == "string") {
            let result;
            result = var_a.innerText.replace(var_k, "");
            return result;
          }
          for (var_a = var_a.firstChild; var_a; var_a = var_a.nextSibling) {
            var_e += var_n(var_a);
          }
        } else {
          if (var_d === 3 || var_d === 4) {
            let result;
            result = var_a.nodeValue;
            return result;
          }
        }
      } else {
        for (var_b = 0; var_c = var_a[var_b]; var_b++) {
          if (var_c.nodeType !== 8) {
            var_e += var_n(var_c);
          }
        }
      }
      return var_e;
    };

    var_o = var_m.selectors = {
      order: ["ID", "NAME", "TAG"],
      match: {
        ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
        CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
        NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
        ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,
        TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
        CHILD:
          /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
        POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,
        PSEUDO:
          /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/,
      },
      leftMatch: {},
      attrMap: {
        class: "className",
        for: "htmlFor",
      },
      attrHandle: {
        href: function (var_a) {
          let result;
          result = var_a.getAttribute("href");
          return result;
        },
        type: function (var_a) {
          let result;
          result = var_a.getAttribute("type");
          return result;
        },
      },
      relative: {
        "+": function (var_a, var_b) {
          let var_c, var_d, var_e;
          var_c = typeof var_b == "string";

          if (var_c) {
            var_d = !var_l.test(var_b);
          } else {
            var_d = var_c;
          }

          if (var_c) {
            var_e = !var_d;
          } else {
            var_e = var_c;
          }

          if (var_d) {
            var_b = var_b.toLowerCase();
          }

          for (var var_f = 0, var_g = var_a.length, var_h; var_f < var_g; var_f++) {
            if ((var_h = var_a[var_f])) {
              while ((var_h = var_h.previousSibling) && var_h.nodeType !== 1)
                {}

              if (var_e || (var_h && var_h.nodeName.toLowerCase() === var_b)) {
                if (var_h) {
                  var_a[var_f] = var_h;
                } else {
                  var_a[var_f] = false;
                }
              } else {
                var_a[var_f] = var_h === var_b;
              }
            }
          }

          if (var_e) {
            var_m.filter(var_b, var_a, true);
          }
        },
        ">": function (var_a, var_b) {
          let var_c, var_d, var_e, var_f;
          var_d = typeof var_b == "string";
          var_e = 0;
          var_f = var_a.length;
          if (var_d && !var_l.test(var_b)) {
            var_b = var_b.toLowerCase();
            for (; var_e < var_f; var_e++) {
              var_c = var_a[var_e];
              if (var_c) {
                let var_g;
                var_g = var_c.parentNode;

                if (var_g.nodeName.toLowerCase() === var_b) {
                  var_a[var_e] = var_g;
                } else {
                  var_a[var_e] = false;
                }
              }
            }
          } else {
            for (; var_e < var_f; var_e++) {
              var_c = var_a[var_e];

              if (var_c) {
                if (var_d) {
                  var_a[var_e] = var_c.parentNode;
                } else {
                  var_a[var_e] = var_c.parentNode === var_b;
                }
              }
            }

            if (var_d) {
              var_m.filter(var_b, var_a, true);
            }
          }
        },
        "": function (var_a, var_b, var_c) {
          let var_d, var_f, var_g;
          var_f = var_e++;
          var_g = var_x;

          if (typeof var_b == "string" &&
            !var_l.test(var_b)) {
            var_b = var_b.toLowerCase();
            var_d = var_b;
            var_g = var_w;
          }

          var_g("parentNode", var_b, var_f, var_a, var_d, var_c);
        },
        "~": function (var_a, var_b, var_c) {
          let var_d, var_f, var_g;
          var_f = var_e++;
          var_g = var_x;

          if (typeof var_b == "string" &&
            !var_l.test(var_b)) {
            var_b = var_b.toLowerCase();
            var_d = var_b;
            var_g = var_w;
          }

          var_g("previousSibling", var_b, var_f, var_a, var_d, var_c);
        },
      },
      find: {
        ID: function (var_a, var_b, var_c) {
          if (typeof var_b.getElementById != "undefined" && !var_c) {
            let var_d;
            var_d = var_b.getElementById(var_a[1]);
            let result;

            if (var_d && var_d.parentNode) {
              result = [var_d];
            } else {
              result = [];
            }

            return result;
          }
        },
        NAME: function (var_a, var_b) {
          if (typeof var_b.getElementsByName != "undefined") {
            let var_c, var_d;
            var_c = [];
            var_d = var_b.getElementsByName(var_a[1]);
            for (var var_e = 0, var_f = var_d.length; var_e < var_f; var_e++) {
              if (var_d[var_e].getAttribute("name") === var_a[1]) {
                var_c.push(var_d[var_e]);
              }
            }
            let result;

            if (var_c.length === 0) {
              result = null;
            } else {
              result = var_c;
            }

            return result;
          }
        },
        TAG: function (var_a, var_b) {
          if (typeof var_b.getElementsByTagName != "undefined") {
            let result;
            result = var_b.getElementsByTagName(var_a[1]);
            return result;
          }
        },
      },
      preFilter: {
        CLASS: function (var_a, var_b, var_c, var_d, var_e, var_f) {
          var_a = " " + var_a[1].replace(var_j, "") + " ";
          if (var_f) {
            return var_a;
          }
          for (var var_g = 0, var_h; (var_h = var_b[var_g]) != null; var_g++) {
            if (var_h) {
              if (var_e ^
                  (var_h.className &&
                    (" " + var_h.className + " ")
                      .replace(/[\t\n\r]/g, " ")
                      .indexOf(var_a) >= 0)) {
                if (!var_c) {
                  var_d.push(var_h);
                }
              } else {
                if (var_c) {
                  var_b[var_g] = false;
                }
              }
            }
          }
          return false;
        },
        ID: function (var_a) {
          let result;
          result = var_a[1].replace(var_j, "");
          return result;
        },
        TAG: function (var_a, var_b) {
          let result;
          result = var_a[1].replace(var_j, "").toLowerCase();
          return result;
        },
        CHILD: function (var_a) {
          if (var_a[1] === "nth") {
            if (!var_a[2]) {
              var_m.error(var_a[0]);
            }

            var_a[2] = var_a[2].replace(/^\+|\s*/g, "");
            let var_b;

            var_b = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(
              (var_a[2] === "even" && "2n") ||
              (var_a[2] === "odd" && "2n+1") ||
              (!/\D/.test(var_a[2]) && "0n+" + var_a[2]) ||
              var_a[2]
            );

            var_a[2] = var_b[1] + (var_b[2] || 1) - 0;
            var_a[3] = var_b[3] - 0;
          } else {
            if (var_a[2]) {
              var_m.error(var_a[0]);
            }
          }
          var_a[0] = var_e++;
          return var_a;
        },
        ATTR: function (var_a, var_b, var_c, var_d, var_e, var_f) {
          let var_g;
          var_g = var_a[1] = var_a[1].replace(var_j, "");

          if (!var_f && var_o.attrMap[var_g]) {
            var_a[1] = var_o.attrMap[var_g];
          }

          var_a[4] = (var_a[4] || var_a[5] || "").replace(var_j, "");

          if (var_a[2] === "~=") {
            var_a[4] = " " + var_a[4] + " ";
          }

          return var_a;
        },
        PSEUDO: function (var_b, var_c, var_d, var_e, var_f) {
          if (var_b[1] === "not") {
            if ((var_a.exec(var_b[3]) || "").length > 1 || /^\w/.test(var_b[3])) {
              var_b[3] = var_m(var_b[3], null, null, var_c);
            } else {
              let var_g;
              var_g = var_m.filter(var_b[3], var_c, var_d, true ^ var_f);

              if (!var_d) {
                var_e.push.apply(var_e, var_g);
              }

              return false;
            }
          } else {
            if (var_o.match.POS.test(var_b[0]) || var_o.match.CHILD.test(var_b[0])) {
              return true;
            }
          }
          return var_b;
        },
        POS: function (var_a) {
          var_a.unshift(true);
          return var_a;
        },
      },
      filters: {
        enabled: function (var_a) {
          let result;

          if (var_a.disabled === false) {
            result = var_a.type !== "hidden";
          } else {
            result = var_a.disabled === false;
          }

          return result;
        },
        disabled: function (var_a) {
          let result;
          result = var_a.disabled === true;
          return result;
        },
        checked: function (var_a) {
          let result;
          result = var_a.checked === true;
          return result;
        },
        selected: function (var_a) {
          if (var_a.parentNode) {
            var_a.parentNode.selectedIndex;
          }

          let result;
          result = var_a.selected === true;
          return result;
        },
        parent: function (var_a) {
          let result;
          result = !!var_a.firstChild;
          return result;
        },
        empty: function (var_a) {
          let result;
          result = !var_a.firstChild;
          return result;
        },
        has: function (var_a, var_b, var_c) {
          let result;
          result = !!var_m(var_c[3], var_a).length;
          return result;
        },
        header: function (var_a) {
          let result;
          result = /h\d/i.test(var_a.nodeName);
          return result;
        },
        text: function (var_a) {
          let var_b, var_c;
          var_b = var_a.getAttribute("type");
          var_c = var_a.type;
          let result;

          if (var_a.nodeName.toLowerCase() === "input" &&
          "text" === var_c) {
            if (var_b === var_c) {
              result = var_b === var_c;
            } else {
              result = var_b === null;
            }
          } else {
            if (var_a.nodeName.toLowerCase() === "input") {
              result = "text" === var_c;
            } else {
              result = var_a.nodeName.toLowerCase() === "input";
            }
          }

          return result;
        },
        radio: function (var_a) {
          let result;

          if (var_a.nodeName.toLowerCase() === "input") {
            result = "radio" === var_a.type;
          } else {
            result = var_a.nodeName.toLowerCase() === "input";
          }

          return result;
        },
        checkbox: function (var_a) {
          let result;

          if (var_a.nodeName.toLowerCase() === "input") {
            result = "checkbox" === var_a.type;
          } else {
            result = var_a.nodeName.toLowerCase() === "input";
          }

          return result;
        },
        file: function (var_a) {
          let result;

          if (var_a.nodeName.toLowerCase() === "input") {
            result = "file" === var_a.type;
          } else {
            result = var_a.nodeName.toLowerCase() === "input";
          }

          return result;
        },
        password: function (var_a) {
          let result;

          if (var_a.nodeName.toLowerCase() === "input") {
            result = "password" === var_a.type;
          } else {
            result = var_a.nodeName.toLowerCase() === "input";
          }

          return result;
        },
        submit: function (var_a) {
          let var_b;
          var_b = var_a.nodeName.toLowerCase();
          let result;

          if (var_b === "input" || var_b === "button") {
            result = "submit" === var_a.type;
          } else {
            if (var_b === "input") {
              result = var_b === "input";
            } else {
              result = var_b === "button";
            }
          }

          return result;
        },
        image: function (var_a) {
          let result;

          if (var_a.nodeName.toLowerCase() === "input") {
            result = "image" === var_a.type;
          } else {
            result = var_a.nodeName.toLowerCase() === "input";
          }

          return result;
        },
        reset: function (var_a) {
          let var_b;
          var_b = var_a.nodeName.toLowerCase();
          let result;

          if (var_b === "input" || var_b === "button") {
            result = "reset" === var_a.type;
          } else {
            if (var_b === "input") {
              result = var_b === "input";
            } else {
              result = var_b === "button";
            }
          }

          return result;
        },
        button: function (var_a) {
          let var_b;
          var_b = var_a.nodeName.toLowerCase();
          let result;

          if (var_b === "input" && "button" === var_a.type) {
            if (var_b === "input") {
              result = "button" === var_a.type;
            } else {
              result = var_b === "input";
            }
          } else {
            result = var_b === "button";
          }

          return result;
        },
        input: function (var_a) {
          let result;
          result = /input|select|textarea|button/i.test(var_a.nodeName);
          return result;
        },
        focus: function (var_a) {
          let result;
          result = var_a === var_a.ownerDocument.activeElement;
          return result;
        },
      },
      setFilters: {
        first: function (var_a, var_b) {
          let result;
          result = var_b === 0;
          return result;
        },
        last: function (var_a, var_b, var_c, var_d) {
          let result;
          result = var_b === var_d.length - 1;
          return result;
        },
        even: function (var_a, var_b) {
          let result;
          result = var_b % 2 === 0;
          return result;
        },
        odd: function (var_a, var_b) {
          let result;
          result = var_b % 2 === 1;
          return result;
        },
        lt: function (var_a, var_b, var_c) {
          let result;
          result = var_b < var_c[3] - 0;
          return result;
        },
        gt: function (var_a, var_b, var_c) {
          let result;
          result = var_b > var_c[3] - 0;
          return result;
        },
        nth: function (var_a, var_b, var_c) {
          let result;
          result = var_c[3] - 0 === var_b;
          return result;
        },
        eq: function (var_a, var_b, var_c) {
          let result;
          result = var_c[3] - 0 === var_b;
          return result;
        },
      },
      filter: {
        PSEUDO: function (var_a, var_b, var_c, var_d) {
          let var_e, var_f;
          var_e = var_b[1];
          var_f = var_o.filters[var_e];
          if (var_f) {
            let result;
            result = var_f(var_a, var_c, var_b, var_d);
            return result;
          }
          if (var_e === "contains") {
            let result;

            result = (var_a.textContent || var_a.innerText || var_n([var_a]) || "").indexOf(
              var_b[3]
            ) >= 0;

            return result;
          }
          if (var_e === "not") {
            let var_g;
            var_g = var_b[3];
            for (var var_h = 0, var_i = var_g.length; var_h < var_i; var_h++) {
              if (var_g[var_h] === var_a) {
                return false;
              }
            }
            return true;
          }
          var_m.error(var_e);
        },
        CHILD: function (var_a, var_b) {
          let var_c, var_e, var_f, var_g, var_h, var_i, var_j, var_k, var_l;
          var_k = var_b[1];
          var_l = var_a;
          switch (var_k) {
            case "only":
            case "first":
              while (var_l = var_l.previousSibling) {
                if (var_l.nodeType === 1) {
                  return false;
                }
              }
              if (var_k === "first") {
                return true;
              }
              var_l = var_a;
            case "last":
              while (var_l = var_l.nextSibling) {
                if (var_l.nodeType === 1) {
                  return false;
                }
              }
              return true;
            case "nth":
              var_c = var_b[2];
              var_e = var_b[3];
              if (var_c === 1 && var_e === 0) {
                return true;
              }
              var_f = var_b[0];
              var_g = var_a.parentNode;
              if (var_g && (var_g[var_d] !== var_f || !var_a.nodeIndex)) {
                var_i = 0;
                for (var_l = var_g.firstChild; var_l; var_l = var_l.nextSibling) {
                  if (var_l.nodeType === 1) {
                    var_l.nodeIndex = ++var_i;
                  }
                }
                var_g[var_d] = var_f;
              }
              var_j = var_a.nodeIndex - var_e;
              let result;

              if (var_c === 0) {
                result = var_j === 0;
              } else {
                if (var_j % var_c === 0) {
                  result = var_j / var_c >= 0;
                } else {
                  result = var_j % var_c === 0;
                }
              }

              return result;
          }
        },
        ID: function (var_a, var_b) {
          let result;

          if (var_a.nodeType === 1) {
            result = var_a.getAttribute("id") === var_b;
          } else {
            result = var_a.nodeType === 1;
          }

          return result;
        },
        TAG: function (var_a, var_b) {
          let result;

          if (var_b === "*" && var_a.nodeType === 1) {
            if (var_b === "*") {
              result = var_a.nodeType === 1;
            } else {
              result = var_b === "*";
            }
          } else {
            if (!!var_a.nodeName) {
              result = var_a.nodeName.toLowerCase() === var_b;
            } else {
              result = !!var_a.nodeName;
            }
          }

          return result;
        },
        CLASS: function (var_a, var_b) {
          let result;

          result = (" " + (var_a.className || var_a.getAttribute("class")) + " ").indexOf(
            var_b
          ) > false;

          return result;
        },
        ATTR: function (var_a, var_b) {
          let var_c, var_d, var_e, var_f, var_g;
          var_c = var_b[1];

          if (var_m.attr) {
            var_d = var_m.attr(var_a, var_c);
          } else {
            if (var_o.attrHandle[var_c]) {
              var_d = var_o.attrHandle[var_c](var_a);
            } else {
              if (var_a[var_c] != null) {
                var_d = var_a[var_c];
              } else {
                var_d = var_a.getAttribute(var_c);
              }
            }
          }

          var_e = var_d + "";
          var_f = var_b[2];
          var_g = var_b[4];
          let result;

          if (var_d == null) {
            result = var_f === "!=";
          } else {
            if (!var_f && var_m.attr) {
              result = var_d != null;
            } else {
              if (var_f === "=") {
                result = var_e === var_g;
              } else {
                if (var_f === "*=") {
                  result = var_e.indexOf(var_g) >= 0;
                } else {
                  if (var_f === "~=") {
                    result = (" " + var_e + " ").indexOf(var_g) >= 0;
                  } else {
                    if (var_g) {
                      if (var_f === "!=") {
                        result = var_e !== var_g;
                      } else {
                        if (var_f === "^=") {
                          result = var_e.indexOf(var_g) === 0;
                        } else {
                          if (var_f === "$=") {
                            result = var_e.substr(var_e.length - var_g.length) === var_g;
                          } else {
                            if (var_f === "|=") {
                              if (var_e === var_g) {
                                result = var_e === var_g;
                              } else {
                                result = var_e.substr(0, var_g.length + 1) === var_g + "-";
                              }
                            } else {
                              result = false;
                            }
                          }
                        }
                      }
                    } else {
                      if (var_e) {
                        result = var_d !== false;
                      } else {
                        result = var_e;
                      }
                    }
                  }
                }
              }
            }
          }

          return result;
        },
        POS: function (var_a, var_b, var_c, var_d) {
          let var_e, var_f;
          var_e = var_b[2];
          var_f = var_o.setFilters[var_e];
          if (var_f) {
            let result;
            result = var_f(var_a, var_c, var_b, var_d);
            return result;
          }
        },
      },
    };

    var_p = var_o.match.POS;

    var_q = function (var_a, var_b) {
      let result;
      result = "\\" + (var_b - 0 + 1);
      return result;
    };

    for (var var_r in var_o.match) {
      var_o.match[var_r] = new RegExp(
        var_o.match[var_r].source + /(?![^\[]*\])(?![^\(]*\))/.source
      );

      var_o.leftMatch[var_r] = new RegExp(
          /(^(?:.|\r|\n)*?)/.source + var_o.match[var_r].source.replace(/\\(\d+)/g, var_q)
        );
    }
    let var_s;

    var_s = function (var_a, var_b) {
      var_a = Array.prototype.slice.call(var_a, 0);
      if (var_b) {
        var_b.push.apply(var_b, var_a);
        return var_b;
      }
      return var_a;
    };

    try {
      Array.prototype.slice.call(var_c.documentElement.childNodes, 0)[0].nodeType;
    } catch (var_t) {
      var_s = function (var_a, var_b) {
        let var_c, var_d;
        var_c = 0;

        if (var_b) {
          var_d = var_b;
        } else {
          var_d = [];
        }

        if (var_g.call(var_a) === "[object Array]") {
          Array.prototype.push.apply(var_d, var_a);
        } else {
          if (typeof var_a.length == "number") {
            for (var var_e = var_a.length; var_c < var_e; var_c++) {
              var_d.push(var_a[var_c]);
            }
          } else {
            for (; var_a[var_c]; var_c++) {
              var_d.push(var_a[var_c]);
            }
          }
        }
        return var_d;
      };
    }
    let var_u, var_v;

    if (var_c.documentElement.compareDocumentPosition) {
      var_u = function (var_a, var_b) {
        if (var_a === var_b) {
          var_h = true;
          return 0;
        }
        if (!var_a.compareDocumentPosition || !var_b.compareDocumentPosition) {
          let result;

          if (var_a.compareDocumentPosition) {
            result = false;
          } else {
            result = 1;
          }

          return result;
        }
        let result;

        if (var_a.compareDocumentPosition(var_b) & 4) {
          result = false;
        } else {
          result = 1;
        }

        return result;
      };
    } else {
      var_u = function (var_a, var_b) {
        if (var_a === var_b) {
          var_h = true;
          return 0;
        }
        if (var_a.sourceIndex && var_b.sourceIndex) {
          let result;
          result = var_a.sourceIndex - var_b.sourceIndex;
          return result;
        }
        let var_c, var_d, var_e, var_f, var_g, var_i, var_j;
        var_e = [];
        var_f = [];
        var_g = var_a.parentNode;
        var_i = var_b.parentNode;
        var_j = var_g;
        if (var_g === var_i) {
          let result;
          result = var_v(var_a, var_b);
          return result;
        }
        if (!var_g) {
          return false;
        }
        if (!var_i) {
          return 1;
        }
        while (var_j) {
          var_e.unshift(var_j);
          var_j = var_j.parentNode;
        }
        var_j = var_i;
        while (var_j) {
          var_f.unshift(var_j);
          var_j = var_j.parentNode;
        }
        var_c = var_e.length;
        var_d = var_f.length;
        for (var var_k = 0; var_k < var_c && var_k < var_d; var_k++) {
          if (var_e[var_k] !== var_f[var_k]) {
            let result;
            result = var_v(var_e[var_k], var_f[var_k]);
            return result;
          }
        }
        let result;

        if (var_k === var_c) {
          result = var_v(var_a, var_f[var_k], false);
        } else {
          result = var_v(var_e[var_k], var_b, 1);
        }

        return result;
      };

      var_v = function (var_a, var_b, var_c) {
        if (var_a === var_b) {
          return var_c;
        }
        let var_d;
        var_d = var_a.nextSibling;
        while (var_d) {
          if (var_d === var_b) {
            return false;
          }
          var_d = var_d.nextSibling;
        }
        return 1;
      };
    }

    (function () {
      let var_a, var_d, var_e;
      var_a = var_c.createElement("div");
      var_d = "script" + new Date().getTime();
      var_e = var_c.documentElement;
      var_a.innerHTML = "<a name='" + var_d + "'/>";
      var_e.insertBefore(var_a, var_e.firstChild);

      if (var_c.getElementById(var_d)) {
        var_o.find.ID = function (var_a, var_c, var_d) {
          if (typeof var_c.getElementById != "undefined" && !var_d) {
            let var_e;
            var_e = var_c.getElementById(var_a[1]);
            let result;

            if (var_e) {
              if (var_e.id === var_a[1] ||
                  (typeof var_e.getAttributeNode != "undefined" &&
                    var_e.getAttributeNode("id").nodeValue === var_a[1])) {
                result = [var_e];
              } else {
                result = var_b;
              }
            } else {
              result = [];
            }

            return result;
          }
        };

        var_o.filter.ID = function (var_a, var_b) {
          let var_c;

          if (typeof var_a.getAttributeNode != "undefined") {
            var_c = var_a.getAttributeNode("id");
          } else {
            var_c = typeof var_a.getAttributeNode != "undefined";
          }

          let result;

          if (var_a.nodeType === 1 && var_c) {
            result = var_c.nodeValue === var_b;
          } else {
            if (var_a.nodeType === 1) {
              result = var_c;
            } else {
              result = var_a.nodeType === 1;
            }
          }

          return result;
        };
      }

      var_e.removeChild(var_a);
      var_e = var_a = null;
    })();

    (function () {
      let var_a;
      var_a = var_c.createElement("div");
      var_a.appendChild(var_c.createComment(""));

      if (var_a.getElementsByTagName("*").length > 0) {
        var_o.find.TAG = function (var_a, var_b) {
          let var_c;
          var_c = var_b.getElementsByTagName(var_a[1]);
          if (var_a[1] === "*") {
            let var_d;
            var_d = [];
            for (var var_e = 0; var_c[var_e]; var_e++) {
              if (var_c[var_e].nodeType === 1) {
                var_d.push(var_c[var_e]);
              }
            }
            var_c = var_d;
          }
          return var_c;
        };
      }

      var_a.innerHTML = "<a href='#'></a>";

      if (var_a.firstChild &&
      typeof var_a.firstChild.getAttribute != "undefined" &&
      var_a.firstChild.getAttribute("href") !== "#") {
        var_o.attrHandle.href = function (var_a) {
          let result;
          result = var_a.getAttribute("href", 2);
          return result;
        };
      }

      var_a = null;
    })();

    if (var_c.querySelectorAll) {
      (function () {
        let var_a, var_b, var_d;
        var_a = var_m;
        var_b = var_c.createElement("div");
        var_d = "__sizzle__";
        var_b.innerHTML = "<p class='TEST'></p>";
        if (
          !var_b.querySelectorAll ||
          var_b.querySelectorAll(".TEST").length !== 0
        ) {
          var_m = function (var_b, var_e, var_f, var_g) {
            if (var_e) {
              var_e = var_e;
            } else {
              var_e = var_c;
            }

            if (!var_g && !var_m.isXML(var_e)) {
              let var_h;
              var_h = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(var_b);
              if (var_h && (var_e.nodeType === 1 || var_e.nodeType === 9)) {
                if (var_h[1]) {
                  let result;
                  result = var_s(var_e.getElementsByTagName(var_b), var_f);
                  return result;
                }
                if (var_h[2] && var_o.find.CLASS && var_e.getElementsByClassName) {
                  let result;
                  result = var_s(var_e.getElementsByClassName(var_h[2]), var_f);
                  return result;
                }
              }
              if (var_e.nodeType === 9) {
                if (var_b === "body" && var_e.body) {
                  let result;
                  result = var_s([var_e.body], var_f);
                  return result;
                }
                if (var_h && var_h[3]) {
                  let var_i;
                  var_i = var_e.getElementById(var_h[3]);
                  if (!var_i || !var_i.parentNode) {
                    let result;
                    result = var_s([], var_f);
                    return result;
                  }
                  if (var_i.id === var_h[3]) {
                    let result;
                    result = var_s([var_i], var_f);
                    return result;
                  }
                }
                try {
                  let result;
                  result = var_s(var_e.querySelectorAll(var_b), var_f);
                  return result;
                } catch (var_j) { }
              } else {
                if (
                  var_e.nodeType === 1 &&
                  var_e.nodeName.toLowerCase() !== "object"
                ) {
                  let var_k, var_l, var_n, var_p, var_q;
                  var_k = var_e;
                  var_l = var_e.getAttribute("id");

                  if (var_l) {
                    var_n = var_l;
                  } else {
                    var_n = var_d;
                  }

                  var_p = var_e.parentNode;
                  var_q = /^\s*[+~]/.test(var_b);

                  if (var_l) {
                    var_n = var_n.replace(/'/g, "\\$&");
                  } else {
                    var_e.setAttribute("id", var_n);
                  }

                  if (var_q && var_p) {
                    var_e = var_e.parentNode;
                  }

                  try {
                    if (!var_q || var_p) {
                      let result;

                      result = var_s(
                        var_e.querySelectorAll("[id='" + var_n + "'] " + var_b),
                        var_f
                      );

                      return result;
                    }
                  } catch (var_r) {
                  } finally {
                    if (!var_l) {
                      var_k.removeAttribute("id");
                    }
                  }
                }
              }
            }
            let result;
            result = var_a(var_b, var_e, var_f, var_g);
            return result;
          };
          for (var var_e in var_a) {
            var_m[var_e] = var_a[var_e];
          }
          var_b = null;
        }
      })();
    }

    (function () {
      let var_a, var_b;
      var_a = var_c.documentElement;

      if (var_a.matchesSelector ||
      var_a.mozMatchesSelector ||
      var_a.webkitMatchesSelector) {
        if (var_a.matchesSelector ||
        var_a.mozMatchesSelector) {
          if (var_a.matchesSelector) {
            var_b = var_a.matchesSelector;
          } else {
            var_b = var_a.mozMatchesSelector;
          }
        } else {
          var_b = var_a.webkitMatchesSelector;
        }
      } else {
        var_b = var_a.msMatchesSelector;
      }

      if (var_b) {
        let var_d, var_e;
        var_d = !var_b.call(var_c.createElement("div"), "div");
        var_e = false;
        try {
          var_b.call(var_c.documentElement, "[test!='']:sizzle");
        } catch (var_f) {
          var_e = true;
        }
        var_m.matchesSelector = function (var_a, var_c) {
          var_c = var_c.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");
          if (!var_m.isXML(var_a)) {
            try {
              if (var_e || (!var_o.match.PSEUDO.test(var_c) && !/!=/.test(var_c))) {
                let var_f;
                var_f = var_b.call(var_a, var_c);
                if (var_f || !var_d || (var_a.document && var_a.document.nodeType !== 11)) {
                  return var_f;
                }
              }
            } catch (var_g) { }
          }
          let result;
          result = var_m(var_c, null, null, [var_a]).length > 0;
          return result;
        };
      }
    })();

    (function () {
      let var_a;
      var_a = var_c.createElement("div");
      var_a.innerHTML = "<div class='test e'></div><div class='test'></div>";
      if (
        !!var_a.getElementsByClassName &&
        var_a.getElementsByClassName("e").length !== 0
      ) {
        var_a.lastChild.className = "e";
        if (var_a.getElementsByClassName("e").length === 1) {
          return;
        }
        var_o.order.splice(1, 0, "CLASS");

        var_o.find.CLASS = function (var_a, var_b, var_c) {
            if (typeof var_b.getElementsByClassName != "undefined" && !var_c) {
              let result;
              result = var_b.getElementsByClassName(var_a[1]);
              return result;
            }
          };

        var_a = null;
      }
    })();

    if (var_c.documentElement.contains) {
      var_m.contains = function (var_a, var_b) {
        let result;

        if (var_a !== var_b) {
          if (var_a.contains) {
            result = var_a.contains(var_b);
          } else {
            result = true;
          }
        } else {
          result = var_a !== var_b;
        }

        return result;
      };
    } else {
      if (var_c.documentElement.compareDocumentPosition) {
        var_m.contains = function (var_a, var_b) {
          let result;
          result = !!(var_a.compareDocumentPosition(var_b) & 16);
          return result;
        };
      } else {
        var_m.contains = function () {
              return false;
            };
      }
    }

    var_m.isXML = function (var_a) {
      let var_b;
      var_b = (var_a ? var_a.ownerDocument || var_a : 0).documentElement;
      let result;

      if (var_b) {
        result = var_b.nodeName !== "HTML";
      } else {
        result = false;
      }

      return result;
    };

    let var_y;

    var_y = function (var_a, var_b, var_c) {
      let var_d, var_e, var_f, var_g;
      var_e = [];
      var_f = "";

      if (var_b.nodeType) {
        var_g = [var_b];
      } else {
        var_g = var_b;
      }

      while (var_d = var_o.match.PSEUDO.exec(var_a)) {
        var_f += var_d[0];
        var_a = var_a.replace(var_o.match.PSEUDO, "");
      }

      if (var_o.relative[var_a]) {
        var_a = var_a + "*";
      } else {
        var_a = var_a;
      }

      for (var var_h = 0, var_i = var_g.length; var_h < var_i; var_h++) {
        var_m(var_a, var_g[var_h], var_e, var_c);
      }
      let result;
      result = var_m.filter(var_f, var_e);
      return result;
    };

    var_m.attr = var_f.attr;
    var_m.selectors.attrMap = {};
    var_f.find = var_m;
    var_f.expr = var_m.selectors;
    var_f.expr[":"] = var_f.expr.filters;
    var_f.unique = var_m.uniqueSort;
    var_f.text = var_m.getText;
    var_f.isXMLDoc = var_m.isXML;
    var_f.contains = var_m.contains;
  })();

  let var_O, var_P, var_Q, var_R, var_S, var_T, var_U;
  var_O = /Until$/;
  var_P = /^(?:parents|prevUntil|prevAll)/;
  var_Q = /,/;
  var_R = /^.[^:#\[\.,]*$/;
  var_S = Array.prototype.slice;
  var_T = var_f.expr.match.POS;

  var_U = {
    children: true,
    contents: true,
    next: true,
    prev: true,
  };

  var_f.fn.extend({
    find: function (var_a) {
      let var_b, var_c, var_d;
      var_b = this;
      if (typeof var_a != "string") {
        let result;

        result = var_f(var_a).filter(function () {
          for (var_c = 0, var_d = var_b.length; var_c < var_d; var_c++) {
            if (var_f.contains(var_b[var_c], this)) {
              return true;
            }
          }
        });

        return result;
      }
      let var_e, var_g, var_h, var_i;
      var_e = this.pushStack("", "find", var_a);
      for (var_c = 0, var_d = this.length; var_c < var_d; var_c++) {
        var_g = var_e.length;
        var_f.find(var_a, this[var_c], var_e);
        if (var_c > 0) {
          for (var_h = var_g; var_h < var_e.length; var_h++) {
            for (var_i = 0; var_i < var_g; var_i++) {
              if (var_e[var_i] === var_e[var_h]) {
                var_e.splice(var_h--, 1);
                break;
              }
            }
          }
        }
      }
      return var_e;
    },
    has: function (var_a) {
      let var_b;
      var_b = var_f(var_a);
      let result;

      result = this.filter(function () {
        for (var var_a = 0, var_c = var_b.length; var_a < var_c; var_a++) {
          if (var_f.contains(this, var_b[var_a])) {
            return true;
          }
        }
      });

      return result;
    },
    not: function (var_a) {
      let result;
      result = this.pushStack(func_W(this, var_a, false), "not", var_a);
      return result;
    },
    filter: function (var_a) {
      let result;
      result = this.pushStack(func_W(this, var_a, true), "filter", var_a);
      return result;
    },
    is: function (var_a) {
      let result;

      if (!!var_a) {
        if (typeof var_a == "string") {
          if (var_T.test(var_a)) {
            result = var_f(var_a, this.context).index(this[0]) >= 0;
          } else {
            result = var_f.filter(var_a, this).length > 0;
          }
        } else {
          result = this.filter(var_a).length > 0;
        }
      } else {
        result = !!var_a;
      }

      return result;
    },
    closest: function (var_a, var_b) {
      let var_c, var_d, var_e, var_g;
      var_c = [];
      var_g = this[0];
      if (var_f.isArray(var_a)) {
        let var_h;
        var_h = 1;
        while (var_g && var_g.ownerDocument && var_g !== var_b) {
          for (var_d = 0; var_d < var_a.length; var_d++) {
            if (var_f(var_g).is(var_a[var_d])) {
              var_c.push({
                selector: var_a[var_d],
                elem: var_g,
                level: var_h,
              });
            }
          }
          var_g = var_g.parentNode;
          var_h++;
        }
        return var_c;
      }
      let var_i;

      if (var_T.test(var_a) || typeof var_a != "string") {
        var_i = var_f(var_a, var_b || this.context);
      } else {
        var_i = 0;
      }

      for (var_d = 0, var_e = this.length; var_d < var_e; var_d++) {
        var_g = this[var_d];
        while (var_g) {
          if (var_i ? var_i.index(var_g) > false : var_f.find.matchesSelector(var_g, var_a)) {
            var_c.push(var_g);
            break;
          }
          var_g = var_g.parentNode;
          if (!var_g || !var_g.ownerDocument || var_g === var_b || var_g.nodeType === 11) {
            break;
          }
        }
      }

      if (var_c.length > 1) {
        var_c = var_f.unique(var_c);
      } else {
        var_c = var_c;
      }

      let result;
      result = this.pushStack(var_c, "closest", var_a);
      return result;
    },
    index: function (var_a) {
      if (!var_a) {
        let result;

        if (this[0] && this[0].parentNode) {
          result = this.prevAll().length;
        } else {
          result = false;
        }

        return result;
      }
      if (typeof var_a == "string") {
        let result;
        result = var_f.inArray(this[0], var_f(var_a));
        return result;
      }
      let result;
      result = var_f.inArray(var_a.jquery ? var_a[0] : var_a, this);
      return result;
    },
    add: function (var_a, var_b) {
      let var_c, var_d;

      if (typeof var_a == "string") {
        var_c = var_f(var_a, var_b);
      } else {
        var_c = var_f.makeArray(var_a && var_a.nodeType ? [var_a] : var_a);
      }

      var_d = var_f.merge(this.get(), var_c);
      let result;
      result = this.pushStack(func_V(var_c[0]) || func_V(var_d[0]) ? var_d : var_f.unique(var_d));
      return result;
    },
    andSelf: function () {
      let result;
      result = this.add(this.prevObject);
      return result;
    },
  });

  var_f.each(
    {
      parent: function (var_a) {
        let var_b;
        var_b = var_a.parentNode;
        let result;

        if (var_b && var_b.nodeType !== 11) {
          result = var_b;
        } else {
          result = null;
        }

        return result;
      },
      parents: function (var_a) {
        let result;
        result = var_f.dir(var_a, "parentNode");
        return result;
      },
      parentsUntil: function (var_a, var_b, var_c) {
        let result;
        result = var_f.dir(var_a, "parentNode", var_c);
        return result;
      },
      next: function (var_a) {
        let result;
        result = var_f.nth(var_a, 2, "nextSibling");
        return result;
      },
      prev: function (var_a) {
        let result;
        result = var_f.nth(var_a, 2, "previousSibling");
        return result;
      },
      nextAll: function (var_a) {
        let result;
        result = var_f.dir(var_a, "nextSibling");
        return result;
      },
      prevAll: function (var_a) {
        let result;
        result = var_f.dir(var_a, "previousSibling");
        return result;
      },
      nextUntil: function (var_a, var_b, var_c) {
        let result;
        result = var_f.dir(var_a, "nextSibling", var_c);
        return result;
      },
      prevUntil: function (var_a, var_b, var_c) {
        let result;
        result = var_f.dir(var_a, "previousSibling", var_c);
        return result;
      },
      siblings: function (var_a) {
        let result;
        result = var_f.sibling(var_a.parentNode.firstChild, var_a);
        return result;
      },
      children: function (var_a) {
        let result;
        result = var_f.sibling(var_a.firstChild);
        return result;
      },
      contents: function (var_a) {
        let result;

        if (var_f.nodeName(var_a, "iframe")) {
          if (var_a.contentDocument) {
            result = var_a.contentDocument;
          } else {
            result = var_a.contentWindow.document;
          }
        } else {
          result = var_f.makeArray(var_a.childNodes);
        }

        return result;
      },
    },
    function (var_a, var_b) {
      var_f.fn[var_a] = function (var_c, var_d) {
        let var_e, var_g;
        var_e = var_f.map(this, var_b, var_c);
        var_g = var_S.call(arguments);

        if (!var_O.test(var_a)) {
          var_d = var_c;
        }

        if (var_d && typeof var_d == "string") {
          var_e = var_f.filter(var_d, var_e);
        }

        if (this.length > 1 && !var_U[var_a]) {
          var_e = var_f.unique(var_e);
        } else {
          var_e = var_e;
        }

        if ((this.length > 1 || var_Q.test(var_d)) && var_P.test(var_a)) {
          var_e = var_e.reverse();
        }

        let result;
        result = this.pushStack(var_e, var_a, var_g.join(","));
        return result;
      };
    }
  );

  var_f.extend({
    filter: function (var_a, var_b, var_c) {
      if (var_c) {
        var_a = ":not(" + var_a + ")";
      }

      let result;

      if (var_b.length === 1) {
        if (var_f.find.matchesSelector(var_b[0], var_a)) {
          result = [var_b[0]];
        } else {
          result = [];
        }
      } else {
        result = var_f.find.matches(var_a, var_b);
      }

      return result;
    },
    dir: function (var_a, var_c, var_d) {
      let var_e, var_g;
      var_e = [];
      var_g = var_a[var_c];
      while (var_g &&
      var_g.nodeType !== 9 &&
      (var_d === var_b || var_g.nodeType !== 1 || !var_f(var_g).is(var_d))) {
        if (var_g.nodeType === 1) {
          var_e.push(var_g);
        }

        var_g = var_g[var_c];
      }
      return var_e;
    },
    nth: function (var_a, var_b, var_c, var_d) {
      if (var_b) {
        var_b = var_b;
      } else {
        var_b = 1;
      }

      let var_e;
      var_e = 0;
      for (; var_a; var_a = var_a[var_c]) {
        if (var_a.nodeType === 1 && ++var_e === var_b) {
          break;
        }
      }
      return var_a;
    },
    sibling: function (var_a, var_b) {
      let var_c;
      var_c = [];
      for (; var_a; var_a = var_a.nextSibling) {
        if (var_a.nodeType === 1 && var_a !== var_b) {
          var_c.push(var_a);
        }
      }
      return var_c;
    },
  });

  let var_Y, var_Z, $, var__, ba, bb, bc, bd, be, bf, bg, bh, bi, bj, bk;
  var_Y = "abbr article aside audio canvas datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video";
  var_Z = / jQuery\d+="(?:\d+|null)"/g;
  $ = /^\s+/;
  var__ = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi;
  ba = /<([\w:]+)/;
  bb = /<tbody/i;
  bc = /<|&#?\w+;/;
  bd = /<(?:script|style)/i;
  be = /<(?:script|object|embed|option|style)/i;
  bf = new RegExp("<(?:" + var_Y.replace(" ", "|") + ")", "i");
  bg = /checked\s*(?:[^=]|=\s*.checked.)/i;
  bh = /\/(java|ecma)script/i;
  bi = /^\s*<!(?:\[CDATA\[|\-\-)/;

  bj = {
    option: [1, "<select multiple='multiple'>", "</select>"],
    legend: [1, "<fieldset>", "</fieldset>"],
    thead: [1, "<table>", "</table>"],
    tr: [2, "<table><tbody>", "</tbody></table>"],
    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
    col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
    area: [1, "<map>", "</map>"],
    _default: [0, "", ""],
  };

  bk = func_X(var_c);
  bj.optgroup = bj.option;
  bj.tbody = bj.tfoot = bj.colgroup = bj.caption = bj.thead;
  bj.th = bj.td;

  if (!var_f.support.htmlSerialize) {
    bj._default = [1, "div<div>", "</div>"];
  }

  var_f.fn.extend({
    text: function (var_a) {
      if (var_f.isFunction(var_a)) {
        let result;

        result = this.each(function (var_b) {
          let var_c;
          var_c = var_f(this);
          var_c.text(var_a.call(this, var_b, var_c.text()));
        });

        return result;
      }
      if (typeof var_a != "object" && var_a !== var_b) {
        let result;

        result = this.empty().append(
          ((this[0] && this[0].ownerDocument) || var_c).createTextNode(var_a)
        );

        return result;
      }
      let result;
      result = var_f.text(this);
      return result;
    },
    wrapAll: function (var_a) {
      if (var_f.isFunction(var_a)) {
        let result;

        result = this.each(function (var_b) {
          var_f(this).wrapAll(var_a.call(this, var_b));
        });

        return result;
      }
      if (this[0]) {
        let var_b;
        var_b = var_f(var_a, this[0].ownerDocument).eq(0).clone(true);

        if (this[0].parentNode) {
          var_b.insertBefore(this[0]);
        }

        var_b
          .map(function () {
          let var_a;
          var_a = this;
          while (var_a.firstChild && var_a.firstChild.nodeType === 1) {
            var_a = var_a.firstChild;
          }
          return var_a;
        })
          .append(this);
      }
      let result;
      result = this;
      return result;
    },
    wrapInner: function (var_a) {
      if (var_f.isFunction(var_a)) {
        let result;

        result = this.each(function (var_b) {
          var_f(this).wrapInner(var_a.call(this, var_b));
        });

        return result;
      }
      let result;

      result = this.each(function () {
        let var_b, var_c;
        var_b = var_f(this);
        var_c = var_b.contents();

        if (var_c.length) {
          var_c.wrapAll(var_a);
        } else {
          var_b.append(var_a);
        }
      });

      return result;
    },
    wrap: function (var_a) {
      let result;

      result = this.each(function () {
        var_f(this).wrapAll(var_a);
      });

      return result;
    },
    unwrap: function () {
      let result;

      result = this.parent()
        .each(function () {
        if (!var_f.nodeName(this, "body")) {
          var_f(this).replaceWith(this.childNodes);
        }
      })
        .end();

      return result;
    },
    append: function () {
      let result;

      result = this.domManip(arguments, true, function (var_a) {
        if (this.nodeType === 1) {
          this.appendChild(var_a);
        }
      });

      return result;
    },
    prepend: function () {
      let result;

      result = this.domManip(arguments, true, function (var_a) {
        if (this.nodeType === 1) {
          this.insertBefore(var_a, this.firstChild);
        }
      });

      return result;
    },
    before: function () {
      if (this[0] && this[0].parentNode) {
        let result;

        result = this.domManip(arguments, false, function (var_a) {
          this.parentNode.insertBefore(var_a, this);
        });

        return result;
      }
      if (arguments.length) {
        let var_a;
        var_a = var_f(arguments[0]);
        var_a.push.apply(var_a, this.toArray());
        let result;
        result = this.pushStack(var_a, "before", arguments);
        return result;
      }
    },
    after: function () {
      if (this[0] && this[0].parentNode) {
        let result;

        result = this.domManip(arguments, false, function (var_a) {
          this.parentNode.insertBefore(var_a, this.nextSibling);
        });

        return result;
      }
      if (arguments.length) {
        let var_a;
        var_a = this.pushStack(this, "after", arguments);
        var_a.push.apply(var_a, var_f(arguments[0]).toArray());
        return var_a;
      }
    },
    remove: function (var_a, var_b) {
      for (var var_c = 0, var_d; (var_d = this[var_c]) != null; var_c++) {
        if (!var_a || var_f.filter(var_a, [var_d]).length) {
          if (!var_b &&
            var_d.nodeType === 1) {
            var_f.cleanData(var_d.getElementsByTagName("*"));
            var_f.cleanData([var_d]);
          }

          if (var_d.parentNode) {
            var_d.parentNode.removeChild(var_d);
          }
        }
      }
      let result;
      result = this;
      return result;
    },
    empty: function () {
      for (var var_a = 0, var_b; (var_b = this[var_a]) != null; var_a++) {
        if (var_b.nodeType === 1) {
          var_f.cleanData(var_b.getElementsByTagName("*"));
        }

        while (var_b.firstChild) {
          var_b.removeChild(var_b.firstChild);
        }
      }
      let result;
      result = this;
      return result;
    },
    clone: function (var_a, var_b) {
      if (var_a == null) {
        var_a = false;
      } else {
        var_a = var_a;
      }

      if (var_b == null) {
        var_b = var_a;
      } else {
        var_b = var_b;
      }

      let result;

      result = this.map(function () {
        return var_f.clone(this, var_a, var_b);
      });

      return result;
    },
    html: function (var_a) {
      if (var_a === var_b) {
        let result;

        if (this[0] && this[0].nodeType === 1) {
          result = this[0].innerHTML.replace(var_Z, "");
        } else {
          result = null;
        }

        return result;
      }
      if (typeof var_a == "string" &&
      !bd.test(var_a) &&
      (var_f.support.leadingWhitespace || !$.test(var_a)) &&
      !bj[(ba.exec(var_a) || ["", ""])[1].toLowerCase()]) {
        var_a = var_a.replace(var__, "<$1></$2>");
        try {
          for (var var_c = 0, var_d = this.length; var_c < var_d; var_c++) {
            if (this[var_c].nodeType === 1) {
              var_f.cleanData(this[var_c].getElementsByTagName("*"));
              this[var_c].innerHTML = var_a;
            }
          }
        } catch (var_e) {
          this.empty().append(var_a);
        }
      } else {
        if (var_f.isFunction(var_a)) {
          this.each(function (var_b) {
            let var_c;
            var_c = var_f(this);
            var_c.html(var_a.call(this, var_b, var_c.html()));
          });
        } else {
          this.empty().append(var_a);
        }
      }
      let result;
      result = this;
      return result;
    },
    replaceWith: function (var_a) {
      if (this[0] && this[0].parentNode) {
        if (var_f.isFunction(var_a)) {
          let result;

          result = this.each(function (var_b) {
            let var_c, var_d;
            var_c = var_f(this);
            var_d = var_c.html();
            var_c.replaceWith(var_a.call(this, var_b, var_d));
          });

          return result;
        }

        if (typeof var_a != "string") {
          var_a = var_f(var_a).detach();
        }

        let result;

        result = this.each(function () {
          let var_b, var_c;
          var_b = this.nextSibling;
          var_c = this.parentNode;
          var_f(this).remove();

          if (var_b) {
            var_f(var_b).before(var_a);
          } else {
            var_f(var_c).append(var_a);
          }
        });

        return result;
      }
      let result;

      if (this.length) {
        result = this.pushStack(var_f(var_f.isFunction(var_a) ? var_a() : var_a), "replaceWith", var_a);
      } else {
        result = this;
      }

      return result;
    },
    detach: function (var_a) {
      let result;
      result = this.remove(var_a, true);
      return result;
    },
    domManip: function (var_a, var_c, var_d) {
      let var_e, var_g, var_h, var_i, var_j, var_k;
      var_j = var_a[0];
      var_k = [];
      if (!var_f.support.checkClone &&
      arguments.length === 3 &&
      typeof var_j == "string" &&
      bg.test(var_j)) {
        let result;

        result = this.each(function () {
          var_f(this).domManip(var_a, var_c, var_d, true);
        });

        return result;
      }
      if (var_f.isFunction(var_j)) {
        let result;

        result = this.each(function (var_e) {
          let var_g;
          var_g = var_f(this);
          var_a[0] = var_j.call(this, var_e, var_c ? var_g.html() : var_b);
          var_g.domManip(var_a, var_c, var_d);
        });

        return result;
      }
      if (this[0]) {
        if (var_j) {
          var_i = var_j.parentNode;
        } else {
          var_i = var_j;
        }

        if (var_f.support.parentNode &&
          var_i &&
          var_i.nodeType === 11 &&
          var_i.childNodes.length === this.length) {
          var_e = {
              fragment: var_i,
            };
        } else {
          var_e = var_f.buildFragment(var_a, this, var_k);
        }

        var_h = var_e.fragment;

        if (var_h.childNodes.length === 1) {
          var_g = var_h = var_h.firstChild;
        } else {
          var_g = var_h.firstChild;
        }

        if (var_g) {
          if (var_c) {
            var_c = var_f.nodeName(var_g, "tr");
          } else {
            var_c = var_c;
          }

          for (var var_l = 0, var_m = this.length, var_n = var_m - 1; var_l < var_m; var_l++) {
            var_d.call(
              var_c ? bl(this[var_l], var_g) : this[var_l],
              var_e.cacheable || (var_m > 1 && var_l < var_n) ? var_f.clone(var_h, true, true) : var_h
            );
          }
        }

        if (var_k.length) {
          var_f.each(var_k, br);
        }
      }
      let result;
      result = this;
      return result;
    },
  });

  var_f.buildFragment = function (var_a, var_b, var_d) {
    let var_e, var_g, var_h, var_i, var_j;
    var_j = var_a[0];

    if (var_b && var_b[0]) {
      if (var_b[0].ownerDocument) {
        var_i = var_b[0].ownerDocument;
      } else {
        var_i = var_b[0];
      }
    }

    if (!var_i.createDocumentFragment) {
      var_i = var_c;
    }

    if (var_a.length === 1 &&
    typeof var_j == "string" &&
    var_j.length < 512 &&
    var_i === var_c &&
    var_j.charAt(0) === "<" &&
    !be.test(var_j) &&
    (var_f.support.checkClone || !bg.test(var_j)) &&
    !var_f.support.unknownElems &&
    bf.test(var_j)) {
      var_g = true;
      var_h = var_f.fragments[var_j];

      if (var_h && var_h !== 1) {
        var_e = var_h;
      }
    }

    if (!var_e) {
      var_e = var_i.createDocumentFragment();
      var_f.clean(var_a, var_i, var_e, var_d);
    }

    if (var_g) {
      if (var_h) {
        var_f.fragments[var_j] = var_e;
      } else {
        var_f.fragments[var_j] = 1;
      }
    }

    let result;

    result = {
      fragment: var_e,
      cacheable: var_g,
    };

    return result;
  };

  var_f.fragments = {};

  var_f.each(
    {
      appendTo: "append",
      prependTo: "prepend",
      insertBefore: "before",
      insertAfter: "after",
      replaceAll: "replaceWith",
    },
    function (var_a, var_b) {
      var_f.fn[var_a] = function (var_c) {
        let var_d, var_e, var_g;
        var_d = [];
        var_e = var_f(var_c);

        if (this.length === 1) {
          var_g = this[0].parentNode;
        } else {
          var_g = this.length === 1;
        }

        if (
          var_g &&
          var_g.nodeType === 11 &&
          var_g.childNodes.length === 1 &&
          var_e.length === 1
        ) {
          var_e[var_b](this[0]);
          let result;
          result = this;
          return result;
        }
        for (var var_h = 0, var_i = var_e.length; var_h < var_i; var_h++) {
          let var_j;
          var_j = (var_h > 0 ? this.clone(true) : this).get();
          var_f(var_e[var_h])[var_b](var_j);
          var_d = var_d.concat(var_j);
        }
        let result;
        result = this.pushStack(var_d, var_a, var_e.selector);
        return result;
      };
    }
  );

  var_f.extend({
    clone: function (var_a, var_b, var_c) {
      let var_d, var_e, var_g, var_h;
      var_d = var_a.cloneNode(true);
      if (
        (!var_f.support.noCloneEvent || !var_f.support.noCloneChecked) &&
        (var_a.nodeType === 1 || var_a.nodeType === 11) &&
        !var_f.isXMLDoc(var_a)
      ) {
        bn(var_a, var_d);
        var_e = bo(var_a);
        var_g = bo(var_d);
        for (var_h = 0; var_e[var_h]; ++var_h) {
          if (var_g[var_h]) {
            bn(var_e[var_h], var_g[var_h]);
          }
        }
      }
      if (var_b) {
        bm(var_a, var_d);
        if (var_c) {
          var_e = bo(var_a);
          var_g = bo(var_d);
          for (var_h = 0; var_e[var_h]; ++var_h) {
            bm(var_e[var_h], var_g[var_h]);
          }
        }
      }
      var_e = var_g = null;
      return var_d;
    },
    clean: function (var_a, var_b, var_d, var_e) {
      let var_g;

      if (var_b) {
        var_b = var_b;
      } else {
        var_b = var_c;
      }

      if (typeof var_b.createElement == "undefined") {
        if (var_b.ownerDocument || (var_b[0] && var_b[0].ownerDocument)) {
          if (var_b.ownerDocument) {
            var_b = var_b.ownerDocument;
          } else {
            if (var_b[0]) {
              var_b = var_b[0].ownerDocument;
            } else {
              var_b = var_b[0];
            }
          }
        } else {
          var_b = var_c;
        }
      }

      let var_h, var_i;
      var_h = [];
      for (var var_j = 0, var_k; (var_k = var_a[var_j]) != null; var_j++) {
        if (typeof var_k == "number") {
          var_k += "";
        }

        if (!var_k) {
          continue;
        }
        if (typeof var_k == "string") {
          if (!bc.test(var_k)) {
            var_k = var_b.createTextNode(var_k);
          } else {
            var_k = var_k.replace(var__, "<$1></$2>");
            let var_l, var_m, var_n, var_o;
            var_l = (ba.exec(var_k) || ["", ""])[1].toLowerCase();

            if (bj[var_l]) {
              var_m = bj[var_l];
            } else {
              var_m = bj._default;
            }

            var_n = var_m[0];
            var_o = var_b.createElement("div");

            if (var_b === var_c) {
              bk.appendChild(var_o);
            } else {
              func_X(var_b).appendChild(var_o);
            }

            var_o.innerHTML = var_m[1] + var_k + var_m[2];
            while (var_n--) {
              var_o = var_o.lastChild;
            }
            if (!var_f.support.tbody) {
              let var_p, var_q;
              var_p = bb.test(var_k);

              if (var_l === "table" && !var_p) {
                if (var_o.firstChild) {
                  var_q = var_o.firstChild.childNodes;
                } else {
                  var_q = var_o.firstChild;
                }
              } else {
                if (var_m[1] === "<table>" && !var_p) {
                  var_q = var_o.childNodes;
                } else {
                  var_q = [];
                }
              }

              for (var_i = var_q.length - 1; var_i >= 0; --var_i) {
                if (var_f.nodeName(var_q[var_i], "tbody") &&
                  !var_q[var_i].childNodes.length) {
                  var_q[var_i].parentNode.removeChild(var_q[var_i]);
                }
              }
            }

            if (!var_f.support.leadingWhitespace &&
              $.test(var_k)) {
              var_o.insertBefore(var_b.createTextNode($.exec(var_k)[0]), var_o.firstChild);
            }

            var_k = var_o.childNodes;
          }
        }
        let var_r;
        if (!var_f.support.appendChecked) {
          if (var_k[0] && typeof (var_r = var_k.length) == "number") {
            for (var_i = 0; var_i < var_r; var_i++) {
              bq(var_k[var_i]);
            }
          } else {
            bq(var_k);
          }
        }

        if (var_k.nodeType) {
          var_h.push(var_k);
        } else {
          var_h = var_f.merge(var_h, var_k);
        }
      }
      if (var_d) {
        var_g = function (var_a) {
          let result;

          if (!var_a.type) {
            result = !var_a.type;
          } else {
            result = bh.test(var_a.type);
          }

          return result;
        };
        for (var_j = 0; var_h[var_j]; var_j++) {
          if (var_e &&
          var_f.nodeName(var_h[var_j], "script") &&
          (!var_h[var_j].type || var_h[var_j].type.toLowerCase() === "text/javascript")) {
            var_e.push(
              var_h[var_j].parentNode ? var_h[var_j].parentNode.removeChild(var_h[var_j]) : var_h[var_j]
            );
          } else {
            if (var_h[var_j].nodeType === 1) {
              let var_s;
              var_s = var_f.grep(var_h[var_j].getElementsByTagName("script"), var_g);
              var_h.splice.apply(var_h, [var_j + 1, 0].concat(var_s));
            }
            var_d.appendChild(var_h[var_j]);
          }
        }
      }
      return var_h;
    },
    cleanData: function (var_a) {
      let var_b, var_c, var_d, var_e, var_g;
      var_d = var_f.cache;
      var_e = var_f.event.special;
      var_g = var_f.support.deleteExpando;
      for (var var_h = 0, var_i; (var_i = var_a[var_h]) != null; var_h++) {
        if (var_i.nodeName && var_f.noData[var_i.nodeName.toLowerCase()]) {
          continue;
        }
        var_c = var_i[var_f.expando];
        if (var_c) {
          var_b = var_d[var_c];
          if (var_b && var_b.events) {
            for (var var_j in var_b.events) {
              if (var_e[var_j]) {
                var_f.event.remove(var_i, var_j);
              } else {
                var_f.removeEvent(var_i, var_j, var_b.handle);
              }
            }

            if (var_b.handle) {
              var_b.handle.elem = null;
            }
          }

          if (var_g) {
            delete var_i[var_f.expando];
          } else {
            if (var_i.removeAttribute) {
              var_i.removeAttribute(var_f.expando);
            }
          }

          delete var_d[var_c];
        }
      }
    },
  });

  let bs, bt, bu, bv, bw, bx, by, bz, bA, bB, bC, bD;
  bs = /alpha\([^)]*\)/i;
  bt = /opacity=([^)]*)/;
  bu = /([A-Z]|^ms)/g;
  bv = /^-?\d+(?:px)?$/i;
  bw = /^-?\d/;
  bx = /^([\-+])=([\-+.\de]+)/;

  by = {
    position: "absolute",
    visibility: "hidden",
    display: "block",
  };

  bz = ["Left", "Right"];
  bA = ["Top", "Bottom"];

  var_f.fn.css = function (var_a, var_c) {
    if (arguments.length === 2 && var_c === var_b) {
      let result;
      result = this;
      return result;
    }
    let result;

    result = var_f.access(this, var_a, var_c, true, function (var_a, var_c, var_d) {
      return var_d !== var_b ? var_f.style(var_a, var_c, var_d) : var_f.css(var_a, var_c);
    });

    return result;
  };

  var_f.extend({
    cssHooks: {
      opacity: {
        get: function (var_a, var_b) {
          if (var_b) {
            let var_c;
            var_c = bB(var_a, "opacity", "opacity");
            let result;

            if (var_c === "") {
              result = "1";
            } else {
              result = var_c;
            }

            return result;
          }
          let result;
          result = var_a.style.opacity;
          return result;
        },
      },
    },
    cssNumber: {
      fillOpacity: true,
      fontWeight: true,
      lineHeight: true,
      opacity: true,
      orphans: true,
      widows: true,
      zIndex: true,
      zoom: true,
    },
    cssProps: {
      float: var_f.support.cssFloat ? "cssFloat" : "styleFloat",
    },
    style: function (var_a, var_c, var_d, var_e) {
      if (!!var_a && var_a.nodeType !== 3 && var_a.nodeType !== 8 && !!var_a.style) {
        let var_g, var_h, var_i, var_j, var_k;
        var_i = var_f.camelCase(var_c);
        var_j = var_a.style;
        var_k = var_f.cssHooks[var_i];

        if (var_f.cssProps[var_i]) {
          var_c = var_f.cssProps[var_i];
        } else {
          var_c = var_i;
        }

        if (var_d === var_b) {
          if (var_k && "get" in var_k && (var_g = var_k.get(var_a, false, var_e)) !== var_b) {
            return var_g;
          }
          let result;
          result = var_j[var_c];
          return result;
        }
        var_h = typeof var_d;

        if (var_h === "string" &&
        (var_g = bx.exec(var_d))) {
          var_d = +(var_g[1] + 1) * +var_g[2] + parseFloat(var_f.css(var_a, var_c));
          var_h = "number";
        }

        if (var_d == null || (var_h === "number" && isNaN(var_d))) {
          return;
        }

        if (var_h === "number" && !var_f.cssNumber[var_i]) {
          var_d += "px";
        }

        if (!var_k || !("set" in var_k) || (var_d = var_k.set(var_a, var_d)) !== var_b) {
          try {
            var_j[var_c] = var_d;
          } catch (var_l) { }
        }
      }
    },
    css: function (var_a, var_c, var_d) {
      let var_e, var_g;
      var_c = var_f.camelCase(var_c);
      var_g = var_f.cssHooks[var_c];

      if (var_f.cssProps[var_c]) {
        var_c = var_f.cssProps[var_c];
      } else {
        var_c = var_c;
      }

      if (var_c === "cssFloat") {
        var_c = "float";
      }

      if (var_g && "get" in var_g && (var_e = var_g.get(var_a, true, var_d)) !== var_b) {
        return var_e;
      }
      if (bB) {
        let result;
        result = bB(var_a, var_c);
        return result;
      }
    },
    swap: function (var_a, var_b, var_c) {
      let var_d;
      var_d = {};
      for (var var_e in var_b) {
        var_d[var_e] = var_a.style[var_e];
        var_a.style[var_e] = var_b[var_e];
      }
      var_c.call(var_a);
      for (var_e in var_b) {
        var_a.style[var_e] = var_d[var_e];
      }
    },
  });

  var_f.curCSS = var_f.css;

  var_f.each(["height", "width"], function (var_a, var_b) {
    var_f.cssHooks[var_b] = {
      get: function (var_a, var_c, var_d) {
        let var_e;
        if (var_c) {
          if (var_a.offsetWidth !== 0) {
            let result;
            result = bE(var_a, var_b, var_d);
            return result;
          }
          var_f.swap(var_a, by, function () {
            var_e = bE(var_a, var_b, var_d);
          });
          return var_e;
        }
      },
      set: function (var_a, var_b) {
        if (!bv.test(var_b)) {
          return var_b;
        }
        var_b = parseFloat(var_b);
        if (var_b >= 0) {
          let result;
          result = var_b + "px";
          return result;
        }
      },
    };
  });

  if (!var_f.support.opacity) {
    var_f.cssHooks.opacity = {
      get: function (var_a, var_b) {
        let result;

        if (bt.test(
          (var_b && var_a.currentStyle ? var_a.currentStyle.filter : var_a.style.filter) || ""
        )) {
          result = parseFloat(RegExp.$1) / 100 + "";
        } else {
          if (var_b) {
            result = "1";
          } else {
            result = "";
          }
        }

        return result;
      },
      set: function (var_a, var_b) {
        let var_c, var_d, var_e, var_g;
        var_c = var_a.style;
        var_d = var_a.currentStyle;

        if (var_f.isNumeric(var_b)) {
          var_e = "alpha(opacity=" + var_b * 100 + ")";
        } else {
          var_e = "";
        }

        if ((var_d && var_d.filter) || var_c.filter) {
          if (var_d && var_d.filter) {
            if (var_d) {
              var_g = var_d.filter;
            } else {
              var_g = var_d;
            }
          } else {
            var_g = var_c.filter;
          }
        } else {
          var_g = "";
        }

        var_c.zoom = 1;
        if (var_b >= 1 && var_f.trim(var_g.replace(bs, "")) === "") {
          var_c.removeAttribute("filter");
          if (var_d && !var_d.filter) {
            return;
          }
        }

        if (bs.test(var_g)) {
          var_c.filter = var_g.replace(bs, var_e);
        } else {
          var_c.filter = var_g + " " + var_e;
        }
      },
    };
  }

  var_f(function () {
    if (!var_f.support.reliableMarginRight) {
      var_f.cssHooks.marginRight = {
          get: function (var_a, var_b) {
            let var_c;
            var_f.swap(
              var_a,
              {
                display: "inline-block",
              },
              function () {
                if (var_b) {
                  var_c = bB(var_a, "margin-right", "marginRight");
                } else {
                  var_c = var_a.style.marginRight;
                }
              }
            );
            return var_c;
          },
        };
    }
  });

  if (var_c.defaultView &&
  var_c.defaultView.getComputedStyle) {
    bC = function (var_a, var_c) {
      let var_d, var_e, var_g;
      var_c = var_c.replace(bu, "-$1").toLowerCase();
      if (!(var_e = var_a.ownerDocument.defaultView)) {
        return var_b;
      }
      if (var_g = var_e.getComputedStyle(var_a, null)) {
        var_d = var_g.getPropertyValue(var_c);

        if (var_d === "" &&
        !var_f.contains(var_a.ownerDocument.documentElement, var_a)) {
          var_d = var_f.style(var_a, var_c);
        }
      }
      return var_d;
    };
  }

  if (var_c.documentElement.currentStyle) {
    bD = function (var_a, var_b) {
      let var_c, var_d, var_e, var_f, var_g;

      if (var_a.currentStyle) {
        var_f = var_a.currentStyle[var_b];
      } else {
        var_f = var_a.currentStyle;
      }

      var_g = var_a.style;

      if (var_f === null && var_g && (var_e = var_g[var_b])) {
        var_f = var_e;
      }

      if (!bv.test(var_f) &&
      bw.test(var_f)) {
        var_c = var_g.left;

        if (var_a.runtimeStyle) {
          var_d = var_a.runtimeStyle.left;
        } else {
          var_d = var_a.runtimeStyle;
        }

        if (var_d) {
          var_a.runtimeStyle.left = var_a.currentStyle.left;
        }

        if (var_b === "fontSize") {
          var_g.left = "1em";
        } else {
          if (var_f) {
            var_g.left = var_f;
          } else {
            var_g.left = 0;
          }
        }

        var_f = var_g.pixelLeft + "px";
        var_g.left = var_c;

        if (var_d) {
          var_a.runtimeStyle.left = var_d;
        }
      }

      let result;

      if (var_f === "") {
        result = "auto";
      } else {
        result = var_f;
      }

      return result;
    };
  }

  if (bC) {
    bB = bC;
  } else {
    bB = bD;
  }

  if (var_f.expr &&
  var_f.expr.filters) {
    var_f.expr.filters.hidden = function (var_a) {
      let var_b, var_c;
      var_b = var_a.offsetWidth;
      var_c = var_a.offsetHeight;
      let result;

      if (var_b === 0 && var_c === 0) {
        if (var_b === 0) {
          result = var_c === 0;
        } else {
          result = var_b === 0;
        }
      } else {
        if (!var_f.support.reliableHiddenOffsets) {
          result = ((var_a.style && var_a.style.display) || var_f.css(var_a, "display")) === "none";
        } else {
          result = !var_f.support.reliableHiddenOffsets;
        }
      }

      return result;
    };

    var_f.expr.filters.visible = function (var_a) {
      let result;
      result = !var_f.expr.filters.hidden(var_a);
      return result;
    };
  }

  let bF, bG, bH, bI, bJ, bK, bL, bM, bN, bO, bP, bQ, bR, bS, bT, bU, bV, bW, bX, bY, bZ;
  bF = /%20/g;
  bG = /\[\]$/;
  bH = /\r?\n/g;
  bI = /#.*$/;
  bJ = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm;
  bK = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i;
  bL = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/;
  bM = /^(?:GET|HEAD)$/;
  bN = /^\/\//;
  bO = /\?/;
  bP = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
  bQ = /^(?:select|textarea)/i;
  bR = /\s+/;
  bS = /([?&])_=[^&]*/;
  bT = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/;
  bU = var_f.fn.load;
  bV = {};
  bW = {};
  bZ = ["*/"] + ["*"];
  try {
    bX = var_e.href;
  } catch (b$) {
    bX = var_c.createElement("a");
    bX.href = "";
    bX = bX.href;
  }

  if (bT.exec(bX.toLowerCase())) {
    bY = bT.exec(bX.toLowerCase());
  } else {
    bY = [];
  }

  var_f.fn.extend({
    load: function (var_a, var_c, var_d) {
      if (typeof var_a != "string" && bU) {
        let result;
        result = bU.apply(this, arguments);
        return result;
      }
      if (!this.length) {
        let result;
        result = this;
        return result;
      }
      let var_e;
      var_e = var_a.indexOf(" ");
      if (var_e >= 0) {
        let var_g;
        var_g = var_a.slice(var_e, var_a.length);
        var_a = var_a.slice(0, var_e);
      }
      let var_h;
      var_h = "GET";

      if (var_c) {
        if (var_f.isFunction(var_c)) {
          var_d = var_c;
          var_c = var_b;
        } else {
          if (typeof var_c == "object") {
            var_c = var_f.param(var_c, var_f.ajaxSettings.traditional);
            var_h = "POST";
          }
        }
      }

      let var_i;
      var_i = this;
      var_f.ajax({
        url: var_a,
        type: var_h,
        dataType: "html",
        data: var_c,
        complete: function (var_a, var_b, var_c) {
          var_c = var_a.responseText;

          if (var_a.isResolved()) {
            var_a.done(function (var_a) {
              var_c = var_a;
            });

            var_i.html(var_g ? var_f("<div>").append(var_c.replace(bP, "")).find(var_g) : var_c);
          }

          if (var_d) {
            var_i.each(var_d, [var_c, var_b, var_a]);
          }
        },
      });
      let result;
      result = this;
      return result;
    },
    serialize: function () {
      let result;
      result = var_f.param(this.serializeArray());
      return result;
    },
    serializeArray: function () {
      let result;

      result = this.map(function () {
        return this.elements ? var_f.makeArray(this.elements) : this;
      })
        .filter(function () {
          return (
            this.name &&
            !this.disabled &&
            (this.checked || bQ.test(this.nodeName) || bK.test(this.type))
          );
        })
        .map(function (var_a, var_b) {
        let var_c;
        var_c = var_f(this).val();
        return var_c == null
          ? null
          : var_f.isArray(var_c)
            ? var_f.map(var_c, function (var_a, var_c) {
              return {
                name: var_b.name,
                value: var_a.replace(bH, "\r\n"),
              };
            })
            : {
              name: var_b.name,
              value: var_c.replace(bH, "\r\n"),
            };
      })
        .get();

      return result;
    },
  });

  var_f.each(
    "ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(
      " "
    ),
    function (var_a, var_b) {
      var_f.fn[var_b] = function (var_a) {
        let result;
        result = this.bind(var_b, var_a);
        return result;
      };
    }
  );

  var_f.each(["get", "post"], function (var_a, var_c) {
    var_f[var_c] = function (var_a, var_d, var_e, var_g) {
      if (var_f.isFunction(var_d)) {
        if (var_g) {
          var_g = var_g;
        } else {
          var_g = var_e;
        }

        var_e = var_d;
        var_d = var_b;
      }

      let result;

      result = var_f.ajax({
        type: var_c,
        url: var_a,
        data: var_d,
        success: var_e,
        dataType: var_g,
      });

      return result;
    };
  });

  var_f.extend({
    getScript: function (var_a, var_c) {
      let result;
      result = var_f.get(var_a, var_b, var_c, "script");
      return result;
    },
    getJSON: function (var_a, var_b, var_c) {
      let result;
      result = var_f.get(var_a, var_b, var_c, "json");
      return result;
    },
    ajaxSetup: function (var_a, var_b) {
      if (var_b) {
        cb(var_a, var_f.ajaxSettings);
      } else {
        var_b = var_a;
        var_a = var_f.ajaxSettings;
      }

      cb(var_a, var_b);
      return var_a;
    },
    ajaxSettings: {
      url: bX,
      isLocal: bL.test(bY[1]),
      global: true,
      type: "GET",
      contentType: "application/x-www-form-urlencoded",
      processData: true,
      async: true,
      accepts: {
        xml: "application/xml, text/xml",
        html: "text/html",
        text: "text/plain",
        json: "application/json, text/javascript",
        "*": bZ,
      },
      contents: {
        xml: /xml/,
        html: /html/,
        json: /json/,
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
      },
      converters: {
        "* text": var_a.String,
        "text html": true,
        "text json": var_f.parseJSON,
        "text xml": var_f.parseXML,
      },
      flatOptions: {
        context: true,
        url: true,
      },
    },
    ajaxPrefilter: b_(bV),
    ajaxTransport: b_(bW),
    ajax: function (var_a, var_c) {
      function var_w(var_a, var_c, var_l, var_m) {
        if (var_s !== 2) {
          var_s = 2;

          if (var_q) {
            clearTimeout(var_q);
          }

          var_p = var_b;

          if (var_m) {
            var_n = var_m;
          } else {
            var_n = "";
          }

          if (var_a > 0) {
            var_v.readyState = 4;
          } else {
            var_v.readyState = 0;
          }

          let var_o, var_r, var_u, var_w, var_x, var_y, var_z;
          var_w = var_c;

          if (var_l) {
            var_x = cd(var_d, var_v, var_l);
          } else {
            var_x = var_b;
          }

          if ((var_a >= 200 && var_a < 300) || var_a === 304) {
            if (var_d.ifModified) {
              if (var_y = var_v.getResponseHeader("Last-Modified")) {
                var_f.lastModified[var_k] = var_y;
              }
              if (var_z = var_v.getResponseHeader("Etag")) {
                var_f.etag[var_k] = var_z;
              }
            }
            if (var_a === 304) {
              var_w = "notmodified";
              var_o = true;
            } else {
              try {
                var_r = ce(var_d, var_x);
                var_w = "success";
                var_o = true;
              } catch (var_A) {
                var_w = "parsererror";
                var_u = var_A;
              }
            }
          } else {
            var_u = var_w;
            if (!var_w || var_a) {
              var_w = "error";

              if (var_a < 0) {
                var_a = 0;
              }
            }
          }
          var_v.status = var_a;
          var_v.statusText = "" + (var_c || var_w);

          if (var_o) {
            var_h.resolveWith(var_e, [var_r, var_w, var_v]);
          } else {
            var_h.rejectWith(var_e, [var_v, var_w, var_u]);
          }

          var_v.statusCode(var_j);
          var_j = var_b;

          if (var_t) {
            var_g.trigger("ajax" + (var_o ? "Success" : "Error"), [
              var_v,
              var_d,
              var_o ? var_r : var_u,
            ]);
          }

          var_i.fireWith(var_e, [var_v, var_w]);

          if (var_t) {
            var_g.trigger("ajaxComplete", [var_v, var_d]);

            if (!--var_f.active) {
              var_f.event.trigger("ajaxStop");
            }
          }
        }
      }

      if (typeof var_a == "object") {
        var_c = var_a;
        var_a = var_b;
      }

      if (var_c) {
        var_c = var_c;
      } else {
        var_c = {};
      }

      let var_d, var_e, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o, var_p, var_q, var_r, var_s, var_t, var_u, var_v;
      var_d = var_f.ajaxSetup({}, var_c);

      if (var_d.context) {
        var_e = var_d.context;
      } else {
        var_e = var_d;
      }

      if (var_e !== var_d && (var_e.nodeType || var_e instanceof var_f)) {
        var_g = var_f(var_e);
      } else {
        var_g = var_f.event;
      }

      var_h = var_f.Deferred();
      var_i = var_f.Callbacks("once memory");

      if (var_d.statusCode) {
        var_j = var_d.statusCode;
      } else {
        var_j = {};
      }

      var_l = {};
      var_m = {};
      var_s = 0;

      var_v = {
        readyState: 0,
        setRequestHeader: function (var_a, var_b) {
          if (!var_s) {
            let var_c;
            var_c = var_a.toLowerCase();
            var_a = var_m[var_c] = var_m[var_c] || var_a;
            var_l[var_a] = var_b;
          }
          let result;
          result = this;
          return result;
        },
        getAllResponseHeaders: function () {
          let result;

          if (var_s === 2) {
            result = var_n;
          } else {
            result = null;
          }

          return result;
        },
        getResponseHeader: function (var_a) {
          let var_c;
          if (var_s === 2) {
            if (!var_o) {
              var_o = {};
              while (var_c = bJ.exec(var_n)) {
                var_o[var_c[1].toLowerCase()] = var_c[2];
              }
            }
            var_c = var_o[var_a.toLowerCase()];
          }
          let result;

          if (var_c === var_b) {
            result = null;
          } else {
            result = var_c;
          }

          return result;
        },
        overrideMimeType: function (var_a) {
          if (!var_s) {
            var_d.mimeType = var_a;
          }

          let result;
          result = this;
          return result;
        },
        abort: function (var_a) {
          if (var_a) {
            var_a = var_a;
          } else {
            var_a = "abort";
          }

          if (var_p) {
            var_p.abort(var_a);
          }

          var_w(0, var_a);
          let result;
          result = this;
          return result;
        },
      };

      var_h.promise(var_v);
      var_v.success = var_v.done;
      var_v.error = var_v.fail;
      var_v.complete = var_i.add;

      var_v.statusCode = function (var_a) {
        if (var_a) {
          let var_b;
          if (var_s < 2) {
            for (var_b in var_a) {
              var_j[var_b] = [var_j[var_b], var_a[var_b]];
            }
          } else {
            var_b = var_a[var_v.status];
            var_v.then(var_b, var_b);
          }
        }
        let result;
        result = this;
        return result;
      };

      var_d.url = ((var_a || var_d.url) + "")
          .replace(bI, "")
          .replace(bN, bY[1] + "//");

      var_d.dataTypes = var_f
          .trim(var_d.dataType || "*")
          .toLowerCase()
          .split(bR);

      if (var_d.crossDomain == null) {
        var_r = bT.exec(var_d.url.toLowerCase());

        var_d.crossDomain = !(
            !var_r ||
            (var_r[1] == bY[1] &&
              var_r[2] == bY[2] &&
              (var_r[3] || (var_r[1] === "http:" ? 80 : 443)) ==
              (bY[3] || (bY[1] === "http:" ? 80 : 443)))
          );
      }

      if (var_d.data &&
      var_d.processData &&
      typeof var_d.data != "string") {
        var_d.data = var_f.param(var_d.data, var_d.traditional);
      }

      ca(bV, var_d, var_c, var_v);
      if (var_s === 2) {
        return false;
      }
      var_t = var_d.global;
      var_d.type = var_d.type.toUpperCase();
      var_d.hasContent = !bM.test(var_d.type);

      if (var_t && var_f.active++ === 0) {
        var_f.event.trigger("ajaxStart");
      }

      if (!var_d.hasContent) {
        if (var_d.data) {
          var_d.url += (bO.test(var_d.url) ? "&" : "?") + var_d.data;
          delete var_d.data;
        }

        var_k = var_d.url;
        if (var_d.cache === false) {
          let var_x, var_y;
          var_x = var_f.now();
          var_y = var_d.url.replace(bS, "$1_=" + var_x);
          var_d.url =
            var_y + (var_y === var_d.url ? (bO.test(var_d.url) ? "&" : "?") + "_=" + var_x : "");
        }
      }

      if ((var_d.data && var_d.hasContent && var_d.contentType !== false) || var_c.contentType) {
        var_v.setRequestHeader("Content-Type", var_d.contentType);
      }

      if (var_d.ifModified) {
        if (var_k) {
          var_k = var_k;
        } else {
          var_k = var_d.url;
        }

        if (var_f.lastModified[var_k]) {
          var_v.setRequestHeader("If-Modified-Since", var_f.lastModified[var_k]);
        }

        if (var_f.etag[var_k]) {
          var_v.setRequestHeader("If-None-Match", var_f.etag[var_k]);
        }
      }

      var_v.setRequestHeader(
        "Accept",
        var_d.dataTypes[0] && var_d.accepts[var_d.dataTypes[0]]
          ? var_d.accepts[var_d.dataTypes[0]] +
          (var_d.dataTypes[0] !== "*" ? ", " + bZ + "; q=0.01" : "")
          : var_d.accepts["*"]
      );

      for (var_u in var_d.headers) {
        var_v.setRequestHeader(var_u, var_d.headers[var_u]);
      }
      if (var_d.beforeSend && (var_d.beforeSend.call(var_e, var_v, var_d) === false || var_s === 2)) {
        var_v.abort();
        return false;
      }
      for (var_u in {
        success: 1,
        error: 1,
        complete: 1,
      }) {
        var_v[var_u](var_d[var_u]);
      }
      var_p = ca(bW, var_d, var_c, var_v);
      if (!var_p) {
        var_w(false, "No Transport");
      } else {
        var_v.readyState = 1;

        if (var_t) {
          var_g.trigger("ajaxSend", [var_v, var_d]);
        }

        if (var_d.async &&
        var_d.timeout > 0) {
          var_q = setTimeout(function () {
            var_v.abort("timeout");
          }, var_d.timeout);
        }

        try {
          var_s = 1;
          var_p.send(var_l, var_w);
        } catch (var_z) {
          if (var_s < 2) {
            var_w(false, var_z);
          } else {
            var_f.error(var_z);
          }
        }
      }
      return var_v;
    },
    param: function (var_a, var_c) {
      let var_d, var_e;
      var_d = [];

      var_e = function (var_a, var_b) {
        if (var_f.isFunction(var_b)) {
          var_b = var_b();
        } else {
          var_b = var_b;
        }

        var_d[var_d.length] =
            encodeURIComponent(var_a) + "=" + encodeURIComponent(var_b);
      };

      if (var_c === var_b) {
        var_c = var_f.ajaxSettings.traditional;
      }

      if (var_f.isArray(var_a) || (var_a.jquery && !var_f.isPlainObject(var_a))) {
        var_f.each(var_a, function () {
          var_e(this.name, this.value);
        });
      } else {
        for (var var_g in var_a) {
          cc(var_g, var_a[var_g], var_c, var_e);
        }
      }
      let result;
      result = var_d.join("&").replace(bF, "+");
      return result;
    },
  });

  var_f.extend({
    active: 0,
    lastModified: {},
    etag: {},
  });

  let cf, cg;
  cf = var_f.now();
  cg = /(\=)\?(&|$)|\?\?/i;

  var_f.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function () {
      let result;
      result = var_f.expando + "_" + cf++;
      return result;
    },
  });

  var_f.ajaxPrefilter("json jsonp", function (var_b, var_c, var_d) {
    let var_e;

    if (var_b.contentType === "application/x-www-form-urlencoded") {
      var_e = typeof var_b.data == "string";
    } else {
      var_e = var_b.contentType === "application/x-www-form-urlencoded";
    }

    if (
      var_b.dataTypes[0] === "jsonp" ||
      (var_b.jsonp !== false && (cg.test(var_b.url) || (var_e && cg.test(var_b.data))))
    ) {
      let var_g, var_h, var_i, var_j, var_k, var_l;

      var_h = var_b.jsonpCallback = var_f.isFunction(var_b.jsonpCallback)
        ? var_b.jsonpCallback()
        : var_b.jsonpCallback;

      var_i = var_a[var_h];
      var_j = var_b.url;
      var_k = var_b.data;
      var_l = "$1" + var_h + "$2";

      if (var_b.jsonp !== false) {
        var_j = var_j.replace(cg, var_l);

        if (var_b.url === var_j) {
          if (var_e) {
            var_k = var_k.replace(cg, var_l);
          }

          if (var_b.data === var_k) {
            var_j += (/\?/.test(var_j) ? "&" : "?") + var_b.jsonp + "=" + var_h;
          }
        }
      }

      var_b.url = var_j;
      var_b.data = var_k;

      var_a[var_h] = function (var_a) {
          var_g = [var_a];
        };

      var_d.always(function () {
        var_a[var_h] = var_i;

        if (var_g && var_f.isFunction(var_i)) {
          var_a[var_h](var_g[0]);
        }
      });

      var_b.converters["script json"] = function () {
        if (!var_g) {
          var_f.error(var_h + " was not called");
        }

        let result;
        result = var_g[0];
        return result;
      };

      var_b.dataTypes[0] = "json";
      return "script";
    }
  });

  var_f.ajaxSetup({
    accepts: {
      script:
        "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript",
    },
    contents: {
      script: /javascript|ecmascript/,
    },
    converters: {
      "text script": function (var_a) {
        var_f.globalEval(var_a);
        return var_a;
      },
    },
  });

  var_f.ajaxPrefilter("script", function (var_a) {
    if (var_a.cache === var_b) {
      var_a.cache = false;
    }

    if (var_a.crossDomain) {
      var_a.type = "GET";
      var_a.global = false;
    }
  });

  var_f.ajaxTransport("script", function (var_a) {
    if (var_a.crossDomain) {
      let var_d, var_e;

      if (var_c.head || var_c.getElementsByTagName("head")[0]) {
        if (var_c.head) {
          var_e = var_c.head;
        } else {
          var_e = var_c.getElementsByTagName("head")[0];
        }
      } else {
        var_e = var_c.documentElement;
      }

      let result;

      result = {
        send: function (var_f, var_g) {
          var_d = var_c.createElement("script");
          var_d.async = "async";

          if (var_a.scriptCharset) {
            var_d.charset = var_a.scriptCharset;
          }

          var_d.src = var_a.url;

          var_d.onload = var_d.onreadystatechange =
              function (var_a, var_c) {
                if (var_c ||
                !var_d.readyState ||
                /loaded|complete/.test(var_d.readyState)) {
                  var_d.onload = var_d.onreadystatechange = null;

                  if (var_e && var_d.parentNode) {
                    var_e.removeChild(var_d);
                  }

                  var_d = var_b;

                  if (!var_c) {
                    var_g(200, "success");
                  }
                }
              };

          var_e.insertBefore(var_d, var_e.firstChild);
        },
        abort: function () {
          if (var_d) {
            var_d.onload(0, 1);
          }
        },
      };

      return result;
    }
  });

  let ch, ci, cj;

  if (var_a.ActiveXObject) {
    ch = function () {
        for (var var_a in cj) {
          cj[var_a](0, 1);
        }
      };
  } else {
    ch = false;
  }

  ci = 0;

  if (var_a.ActiveXObject) {
    var_f.ajaxSettings.xhr = function () {
      let result;

      if (!this.isLocal && ck()) {
        if (!this.isLocal) {
          result = ck();
        } else {
          result = !this.isLocal;
        }
      } else {
        result = cl();
      }

      return result;
    };
  } else {
    var_f.ajaxSettings.xhr = ck;
  }

  (function (var_a) {
    var_f.extend(var_f.support, {
      ajax: !!var_a,
      cors: !!var_a && "withCredentials" in var_a,
    });
  })(var_f.ajaxSettings.xhr());

  if (var_f.support.ajax) {
    var_f.ajaxTransport(function (var_c) {
      if (!var_c.crossDomain || var_f.support.cors) {
        let var_d;
        let result;

        result = {
          send: function (var_e, var_g) {
            let var_h, var_i, var_j;
            var_h = var_c.xhr();

            if (var_c.username) {
              var_h.open(var_c.type, var_c.url, var_c.async, var_c.username, var_c.password);
            } else {
              var_h.open(var_c.type, var_c.url, var_c.async);
            }

            if (var_c.xhrFields) {
              for (var_j in var_c.xhrFields) {
                var_h[var_j] = var_c.xhrFields[var_j];
              }
            }

            if (var_c.mimeType &&
              var_h.overrideMimeType) {
              var_h.overrideMimeType(var_c.mimeType);
            }

            if (!var_c.crossDomain &&
            !var_e["X-Requested-With"]) {
              var_e["X-Requested-With"] = "XMLHttpRequest";
            }

            try {
              for (var_j in var_e) {
                var_h.setRequestHeader(var_j, var_e[var_j]);
              }
            } catch (var_k) { }
            var_h.send((var_c.hasContent && var_c.data) || null);

            var_d = function (var_a, var_e) {
              let var_j, var_k, var_l, var_m, var_n;
              try {
                if (var_d && (var_e || var_h.readyState === 4)) {
                  var_d = var_b;

                  if (var_i) {
                    var_h.onreadystatechange = var_f.noop;

                    if (ch) {
                      delete cj[var_i];
                    }
                  }

                  if (var_e) {
                    if (var_h.readyState !== 4) {
                      var_h.abort();
                    }
                  } else {
                    var_j = var_h.status;
                    var_l = var_h.getAllResponseHeaders();
                    var_m = {};
                    var_n = var_h.responseXML;

                    if (var_n && var_n.documentElement) {
                      var_m.xml = var_n;
                    }

                    var_m.text = var_h.responseText;
                    try {
                      var_k = var_h.statusText;
                    } catch (var_o) {
                      var_k = "";
                    }

                    if (!var_j && var_c.isLocal && !var_c.crossDomain) {
                      if (var_m.text) {
                        var_j = 200;
                      } else {
                        var_j = 404;
                      }
                    } else {
                      if (var_j === 1223) {
                        var_j = 204;
                      }
                    }
                  }
                }
              } catch (var_p) {
                if (!var_e) {
                  var_g(false, var_p);
                }
              }

              if (var_m) {
                var_g(var_j, var_k, var_m, var_l);
              }
            };

            if (!var_c.async || var_h.readyState === 4) {
              var_d();
            } else {
              var_i = ++ci;

              if (ch) {
                if (!cj) {
                  cj = {};
                  var_f(var_a).unload(ch);
                }

                cj[var_i] = var_d;
              }

              var_h.onreadystatechange = var_d;
            }
          },
          abort: function () {
            if (var_d) {
              var_d(0, 1);
            }
          },
        };

        return result;
      }
    });
  }

  let cm, cn, co, cp, cq, cr, cs, ct;
  cm = {};
  cp = /^(?:toggle|show|hide)$/;
  cq = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i;

  cs = [
    ["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"],
    ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"],
    ["opacity"],
  ];

  var_f.fn.extend({
    show: function (var_a, var_b, var_c) {
      let var_d, var_e;
      if (var_a || var_a === 0) {
        let result;
        result = this.animate(cw("show", 3), var_a, var_b, var_c);
        return result;
      }
      for (var var_g = 0, var_h = this.length; var_g < var_h; var_g++) {
        var_d = this[var_g];

        if (var_d.style) {
          var_e = var_d.style.display;

          if (!var_f._data(var_d, "olddisplay") &&
          var_e === "none") {
            var_e = var_d.style.display = "";
          }

          if (var_e === "" &&
          var_f.css(var_d, "display") === "none") {
            var_f._data(var_d, "olddisplay", cx(var_d.nodeName));
          }
        }
      }
      for (var_g = 0; var_g < var_h; var_g++) {
        var_d = this[var_g];
        if (var_d.style) {
          var_e = var_d.style.display;
          if (var_e === "" || var_e === "none") {
            if (var_f._data(var_d, "olddisplay")) {
              var_d.style.display = var_f._data(var_d, "olddisplay");
            } else {
              var_d.style.display = "";
            }
          }
        }
      }
      let result;
      result = this;
      return result;
    },
    hide: function (var_a, var_b, var_c) {
      if (var_a || var_a === 0) {
        let result;
        result = this.animate(cw("hide", 3), var_a, var_b, var_c);
        return result;
      }
      let var_d, var_e, var_g, var_h;
      var_g = 0;
      var_h = this.length;
      for (; var_g < var_h; var_g++) {
        var_d = this[var_g];

        if (var_d.style) {
          var_e = var_f.css(var_d, "display");

          if (var_e !== "none" &&
          !var_f._data(var_d, "olddisplay")) {
            var_f._data(var_d, "olddisplay", var_e);
          }
        }
      }
      for (var_g = 0; var_g < var_h; var_g++) {
        if (this[var_g].style) {
          this[var_g].style.display = "none";
        }
      }
      let result;
      result = this;
      return result;
    },
    _toggle: var_f.fn.toggle,
    toggle: function (var_a, var_b, var_c) {
      let var_d;
      var_d = typeof var_a == "boolean";

      if (var_f.isFunction(var_a) && var_f.isFunction(var_b)) {
        this._toggle.apply(this, arguments);
      } else {
        if (var_a == null || var_d) {
          this.each(function () {
            let var_b;

            if (var_d) {
              var_b = var_a;
            } else {
              var_b = var_f(this).is(":hidden");
            }

            var_f(this)[var_b ? "show" : "hide"]();
          });
        } else {
          this.animate(cw("toggle", 3), var_a, var_b, var_c);
        }
      }

      let result;
      result = this;
      return result;
    },
    fadeTo: function (var_a, var_b, var_c, var_d) {
      let result;

      result = this.filter(":hidden").css("opacity", 0).show().end().animate(
        {
          opacity: var_b,
        },
        var_a,
        var_c,
        var_d
      );

      return result;
    },
    animate: function (var_a, var_b, var_c, var_d) {
      function var_g() {
        if (var_e.queue === false) {
          var_f._mark(this);
        }

        let var_b, var_c, var_d, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o;
        var_b = var_f.extend({}, var_e);
        var_c = this.nodeType === 1;

        if (var_c) {
          var_d = var_f(this).is(":hidden");
        } else {
          var_d = var_c;
        }

        var_b.animatedProperties = {};
        for (var_i in var_a) {
          var_g = var_f.camelCase(var_i);

          if (var_i !== var_g) {
            var_a[var_g] = var_a[var_i];
            delete var_a[var_i];
          }

          var_h = var_a[var_g];

          if (var_f.isArray(var_h)) {
            var_b.animatedProperties[var_g] = var_h[1];
            var_h = var_a[var_g] = var_h[0];
          } else {
            if ((var_b.specialEasing && var_b.specialEasing[var_g]) ||
            var_b.easing) {
              if (var_b.specialEasing && var_b.specialEasing[var_g]) {
                if (var_b.specialEasing) {
                  var_b.animatedProperties[var_g] = var_b.specialEasing[var_g];
                } else {
                  var_b.animatedProperties[var_g] = var_b.specialEasing;
                }
              } else {
                var_b.animatedProperties[var_g] = var_b.easing;
              }
            } else {
              var_b.animatedProperties[var_g] = "swing";
            }
          }

          if ((var_h === "hide" && var_d) || (var_h === "show" && !var_d)) {
            let result;
            result = var_b.complete.call(this);
            return result;
          }

          if (var_c &&
            (var_g === "height" || var_g === "width")) {
            var_b.overflow = [
                this.style.overflow,
                this.style.overflowX,
                this.style.overflowY,
              ];

            if (var_f.css(this, "display") === "inline" &&
            var_f.css(this, "float") === "none") {
              if (!var_f.support.inlineBlockNeedsLayout ||
                cx(this.nodeName) === "inline") {
                this.style.display = "inline-block";
              } else {
                this.style.zoom = 1;
              }
            }
          }
        }

        if (var_b.overflow != null) {
          this.style.overflow = "hidden";
        }

        for (var_i in var_a) {
          var_j = new var_f.fx(this, var_b, var_i);
          var_h = var_a[var_i];

          if (cp.test(var_h)) {
            if (var_f._data(this, "toggle" + var_i)) {
              var_o = var_f._data(this, "toggle" + var_i);
            } else {
              if (var_h === "toggle") {
                if (var_d) {
                  var_o = "show";
                } else {
                  var_o = "hide";
                }
              } else {
                var_o = 0;
              }
            }

            if (var_o) {
              var_f._data(
                  this,
                  "toggle" + var_i,
                  var_o === "show" ? "hide" : "show"
                );

              var_j[var_o]();
            } else {
              var_j[var_h]();
            }
          } else {
            var_k = cq.exec(var_h);
            var_l = var_j.cur();

            if (var_k) {
              var_m = parseFloat(var_k[2]);

              if (var_k[3]) {
                var_n = var_k[3];
              } else {
                if (var_f.cssNumber[var_i]) {
                  var_n = "";
                } else {
                  var_n = "px";
                }
              }

              if (var_n !== "px") {
                var_f.style(this, var_i, (var_m || 1) + var_n);
                var_l = ((var_m || 1) / var_j.cur()) * var_l;
                var_f.style(this, var_i, var_l + var_n);
              }

              if (var_k[1]) {
                var_m = (var_k[1] === "-=" ? false : 1) * var_m + var_l;
              }

              var_j.custom(var_l, var_m, var_n);
            } else {
              var_j.custom(var_l, var_h, "");
            }
          }
        }
        return true;
      }
      let var_e;
      var_e = var_f.speed(var_b, var_c, var_d);
      if (var_f.isEmptyObject(var_a)) {
        let result;
        result = this.each(var_e.complete, [false]);
        return result;
      }
      var_a = var_f.extend({}, var_a);
      let result;

      if (var_e.queue === false) {
        result = this.each(var_g);
      } else {
        result = this.queue(var_e.queue, var_g);
      }

      return result;
    },
    stop: function (var_a, var_c, var_d) {
      if (typeof var_a != "string") {
        var_d = var_c;
        var_c = var_a;
        var_a = var_b;
      }

      if (var_c && var_a !== false) {
        this.queue(var_a || "fx", []);
      }

      let result;

      result = this.each(function () {
        function var_h(var_a, var_b, var_c) {
          let var_e;
          var_e = var_b[var_c];
          var_f.removeData(var_a, var_c, true);
          var_e.stop(var_d);
        }
        let var_b, var_c, var_e, var_g;
        var_c = false;
        var_e = var_f.timers;
        var_g = var_f._data(this);

        if (!var_d) {
          var_f._unmark(true, this);
        }

        if (var_a == null) {
          for (var_b in var_g) {
            if (var_g[var_b].stop && var_b.indexOf(".run") === var_b.length - 4) {
              var_h(this, var_g, var_b);
            }
          }
        } else {
          if (var_g[(var_b = var_a + ".run")] && var_g[var_b].stop) {
            var_h(this, var_g, var_b);
          }
        }
        for (var_b = var_e.length; var_b--; ) {
          if (var_e[var_b].elem === this &&
            (var_a == null || var_e[var_b].queue === var_a)) {
            if (var_d) {
              var_e[var_b](true);
            } else {
              var_e[var_b].saveState();
            }

            var_c = true;
            var_e.splice(var_b, 1);
          }
        }

        if (!var_d || !var_c) {
          var_f.dequeue(this, var_a);
        }
      });

      return result;
    },
  });

  var_f.each(
    {
      slideDown: cw("show", 1),
      slideUp: cw("hide", 1),
      slideToggle: cw("toggle", 1),
      fadeIn: {
        opacity: "show",
      },
      fadeOut: {
        opacity: "hide",
      },
      fadeToggle: {
        opacity: "toggle",
      },
    },
    function (var_a, var_b) {
      var_f.fn[var_a] = function (var_a, var_c, var_d) {
        let result;
        result = this.animate(var_b, var_a, var_c, var_d);
        return result;
      };
    }
  );

  var_f.extend({
    speed: function (var_a, var_b, var_c) {
      let var_d;

      if (var_a && typeof var_a == "object") {
        var_d = var_f.extend({}, var_a);
      } else {
        var_d = {
            complete: var_c || (!var_c && var_b) || (var_f.isFunction(var_a) && var_a),
            duration: var_a,
            easing: (var_c && var_b) || (var_b && !var_f.isFunction(var_b) && var_b),
          };
      }

      if (var_f.fx.off) {
        var_d.duration = 0;
      } else {
        if (typeof var_d.duration == "number") {
          var_d.duration = var_d.duration;
        } else {
          if (var_d.duration in var_f.fx.speeds) {
            var_d.duration = var_f.fx.speeds[var_d.duration];
          } else {
            var_d.duration = var_f.fx.speeds._default;
          }
        }
      }

      if (var_d.queue == null || var_d.queue === true) {
        var_d.queue = "fx";
      }
      var_d.old = var_d.complete;

      var_d.complete = function (var_a) {
        if (var_f.isFunction(var_d.old)) {
          var_d.old.call(this);
        }

        if (var_d.queue) {
          var_f.dequeue(this, var_d.queue);
        } else {
          if (var_a !== false) {
            var_f._unmark(this);
          }
        }
      };

      return var_d;
    },
    easing: {
      linear: function (var_a, var_b, var_c, var_d) {
        let result;
        result = var_c + var_d * var_a;
        return result;
      },
      swing: function (var_a, var_b, var_c, var_d) {
        let result;
        result = (-Math.cos(var_a * Math.PI) / 2 + 0.5) * var_d + var_c;
        return result;
      },
    },
    timers: [],
    fx: function (var_a, var_b, var_c) {
      this.options = var_b;
      this.elem = var_a;
      this.prop = var_c;

      if (var_b.orig) {
        var_b.orig = var_b.orig;
      } else {
        var_b.orig = {};
      }
    },
  });

  var_f.fx.prototype = {
      update: function () {
        if (this.options.step) {
          this.options.step.call(this.elem, this.now, this);
        }

        (var_f.fx.step[this.prop] || var_f.fx.step._default)(this);
      },
      cur: function () {
        if (this.elem[this.prop] != null &&
        (!this.elem.style || this.elem.style[this.prop] == null)) {
          let result;
          result = this.elem[this.prop];
          return result;
        }
        let var_a, var_b;
        var_b = var_f.css(this.elem, this.prop);
        let result;

        if (isNaN((var_a = parseFloat(var_b)))) {
          if (!var_b || var_b === "auto") {
            result = 0;
          } else {
            result = var_b;
          }
        } else {
          result = var_a;
        }

        return result;
      },
      custom: function (var_a, var_c, var_d) {
        function var_h(var_a) {
          let result;
          result = var_e.step(var_a);
          return result;
        }
        let var_e, var_g;
        var_e = this;
        var_g = var_f.fx;

        if (ct) {
          this.startTime = ct;
        } else {
          this.startTime = cu();
        }

        this.end = var_c;
        this.now = this.start = var_a;
        this.pos = this.state = 0;

        if (var_d || this.unit) {
          if (var_d) {
            this.unit = var_d;
          } else {
            this.unit = this.unit;
          }
        } else {
          if (var_f.cssNumber[this.prop]) {
            this.unit = "";
          } else {
            this.unit = "px";
          }
        }

        var_h.queue = this.options.queue;
        var_h.elem = this.elem;

        var_h.saveState = function () {
          if (var_e.options.hide &&
            var_f._data(var_e.elem, "fxshow" + var_e.prop) === var_b) {
            var_f._data(var_e.elem, "fxshow" + var_e.prop, var_e.start);
          }
        };

        if (var_h() &&
        var_f.timers.push(var_h) &&
        !cr) {
          cr = setInterval(var_g.tick, var_g.interval);
        }
      },
      show: function () {
        let var_a;
        var_a = var_f._data(this.elem, "fxshow" + this.prop);

        if (var_a) {
          this.options.orig[this.prop] = var_a;
        } else {
          this.options.orig[this.prop] = var_f.style(this.elem, this.prop);
        }

        this.options.show = true;

        if (var_a !== var_b) {
          this.custom(this.cur(), var_a);
        } else {
          this.custom(
              this.prop === "width" || this.prop === "height" ? 1 : 0,
              this.cur()
            );
        }

        var_f(this.elem).show();
      },
      hide: function () {
        if (var_f._data(this.elem, "fxshow" + this.prop)) {
          this.options.orig[this.prop] = var_f._data(this.elem, "fxshow" + this.prop);
        } else {
          this.options.orig[this.prop] = var_f.style(this.elem, this.prop);
        }

        this.options.hide = true;
        this.custom(this.cur(), 0);
      },
      step: function (var_a) {
        let var_b, var_c, var_d, var_e, var_g, var_h, var_i;

        if (ct) {
          var_e = ct;
        } else {
          var_e = cu();
        }

        var_g = true;
        var_h = this.elem;
        var_i = this.options;
        if (var_a || var_e >= var_i.duration + this.startTime) {
          this.now = this.end;
          this.pos = this.state = 1;
          this.update();
          var_i.animatedProperties[this.prop] = true;
          for (var_b in var_i.animatedProperties) {
            if (var_i.animatedProperties[var_b] !== true) {
              var_g = false;
            }
          }
          if (var_g) {
            if (var_i.overflow != null &&
              !var_f.support.shrinkWrapBlocks) {
              var_f.each(["", "X", "Y"], function (var_a, var_b) {
                var_h.style["overflow" + var_b] = var_i.overflow[var_a];
              });
            }

            if (var_i.hide) {
              var_f(var_h).hide();
            }

            if (var_i.hide || var_i.show) {
              for (var_b in var_i.animatedProperties) {
                var_f.style(var_h, var_b, var_i.orig[var_b]);
                var_f.removeData(var_h, "fxshow" + var_b, true);
                var_f.removeData(var_h, "toggle" + var_b, true);
              }
            }
            var_d = var_i.complete;

            if (var_d) {
              var_i.complete = false;
              var_d.call(var_h);
            }
          }
          return false;
        }

        if (var_i.duration == Infinity) {
          this.now = var_e;
        } else {
          var_c = var_e - this.startTime;
          this.state = var_c / var_i.duration;

          this.pos = var_f.easing[var_i.animatedProperties[this.prop]](
                this.state,
                var_c,
                0,
                1,
                var_i.duration
              );

          this.now = this.start + (this.end - this.start) * this.pos;
        }

        this.update();
        return true;
      },
    };

  var_f.extend(var_f.fx, {
    tick: function () {
      let var_a, var_b, var_c;
      var_b = var_f.timers;
      var_c = 0;
      for (; var_c < var_b.length; var_c++) {
        var_a = var_b[var_c];

        if (!var_a() && var_b[var_c] === var_a) {
          var_b.splice(var_c--, 1);
        }
      }

      if (!var_b.length) {
        var_f.fx.stop();
      }
    },
    interval: 13,
    stop: function () {
      clearInterval(cr);
      cr = null;
    },
    speeds: {
      slow: 600,
      fast: 200,
      _default: 400,
    },
    step: {
      opacity: function (var_a) {
        var_f.style(var_a.elem, "opacity", var_a.now);
      },
      _default: function (var_a) {
        if (var_a.elem.style && var_a.elem.style[var_a.prop] != null) {
          var_a.elem.style[var_a.prop] = var_a.now + var_a.unit;
        } else {
          var_a.elem[var_a.prop] = var_a.now;
        }
      },
    },
  });

  var_f.each(["width", "height"], function (var_a, var_b) {
    var_f.fx.step[var_b] = function (var_a) {
      var_f.style(var_a.elem, var_b, Math.max(0, var_a.now));
    };
  });

  if (var_f.expr &&
  var_f.expr.filters) {
    var_f.expr.filters.animated = function (var_a) {
      let result;

      result = var_f.grep(var_f.timers, function (var_b) {
        return var_a === var_b.elem;
      }).length;

      return result;
    };
  }

  let cy, cz;
  cy = /^t(?:able|d|h)$/i;
  cz = /^(?:body|html)$/i;

  if ("getBoundingClientRect" in var_c.documentElement) {
    var_f.fn.offset = function (var_a) {
      let var_b, var_c;
      var_b = this[0];
      if (var_a) {
        let result;

        result = this.each(function (var_b) {
          var_f.offset.setOffset(this, var_a, var_b);
        });

        return result;
      }
      if (!var_b || !var_b.ownerDocument) {
        return null;
      }
      if (var_b === var_b.ownerDocument.body) {
        let result;
        result = var_f.offset.bodyOffset(var_b);
        return result;
      }
      try {
        var_c = var_b.getBoundingClientRect();
      } catch (var_d) { }
      let var_e, var_g;
      var_e = var_b.ownerDocument;
      var_g = var_e.documentElement;
      if (!var_c || !var_f.contains(var_g, var_b)) {
        let result;

        if (var_c) {
          result = {
              top: var_c.top,
              left: var_c.left,
            };
        } else {
          result = {
              top: 0,
              left: 0,
            };
        }

        return result;
      }
      let var_h, var_i, var_j, var_k, var_l, var_m, var_n, var_o;
      var_h = var_e.body;
      var_i = cA(var_e);

      if (var_g.clientTop || var_h.clientTop) {
        if (var_g.clientTop) {
          var_j = var_g.clientTop;
        } else {
          var_j = var_h.clientTop;
        }
      } else {
        var_j = 0;
      }

      if (var_g.clientLeft || var_h.clientLeft) {
        if (var_g.clientLeft) {
          var_k = var_g.clientLeft;
        } else {
          var_k = var_h.clientLeft;
        }
      } else {
        var_k = 0;
      }

      if (var_i.pageYOffset || (var_f.support.boxModel && var_g.scrollTop)) {
        if (var_i.pageYOffset) {
          var_l = var_i.pageYOffset;
        } else {
          if (var_f.support.boxModel) {
            var_l = var_g.scrollTop;
          } else {
            var_l = var_f.support.boxModel;
          }
        }
      } else {
        var_l = var_h.scrollTop;
      }

      if (var_i.pageXOffset ||
      (var_f.support.boxModel && var_g.scrollLeft)) {
        if (var_i.pageXOffset) {
          var_m = var_i.pageXOffset;
        } else {
          if (var_f.support.boxModel) {
            var_m = var_g.scrollLeft;
          } else {
            var_m = var_f.support.boxModel;
          }
        }
      } else {
        var_m = var_h.scrollLeft;
      }

      var_n = var_c.top + var_l - var_j;
      var_o = var_c.left + var_m - var_k;
      let result;

      result = {
        top: var_n,
        left: var_o,
      };

      return result;
    };
  } else {
    var_f.fn.offset = function (var_a) {
      let var_b;
      var_b = this[0];
      if (var_a) {
        let result;

        result = this.each(function (var_b) {
          var_f.offset.setOffset(this, var_a, var_b);
        });

        return result;
      }
      if (!var_b || !var_b.ownerDocument) {
        return null;
      }
      if (var_b === var_b.ownerDocument.body) {
        let result;
        result = var_f.offset.bodyOffset(var_b);
        return result;
      }
      let var_c, var_d, var_e, var_g, var_h, var_i, var_j, var_k, var_l, var_m;
      var_d = var_b.offsetParent;
      var_e = var_b;
      var_g = var_b.ownerDocument;
      var_h = var_g.documentElement;
      var_i = var_g.body;
      var_j = var_g.defaultView;

      if (var_j) {
        var_k = var_j.getComputedStyle(var_b, null);
      } else {
        var_k = var_b.currentStyle;
      }

      var_l = var_b.offsetTop;
      var_m = var_b.offsetLeft;
      while ((var_b = var_b.parentNode) && var_b !== var_i && var_b !== var_h) {
        if (var_f.support.fixedPosition && var_k.position === "fixed") {
          break;
        }

        if (var_j) {
          var_c = var_j.getComputedStyle(var_b, null);
        } else {
          var_c = var_b.currentStyle;
        }

        var_l -= var_b.scrollTop;
        var_m -= var_b.scrollLeft;

        if (var_b === var_d) {
          var_l += var_b.offsetTop;
          var_m += var_b.offsetLeft;

          if (var_f.support.doesNotAddBorder &&
          (!var_f.support.doesAddBorderForTableAndCells ||
            !cy.test(var_b.nodeName))) {
            var_l += parseFloat(var_c.borderTopWidth) || 0;
            var_m += parseFloat(var_c.borderLeftWidth) || 0;
          }

          var_e = var_d;
          var_d = var_b.offsetParent;
        }

        if (var_f.support.subtractsBorderForOverflowNotVisible &&
        var_c.overflow !== "visible") {
          var_l += parseFloat(var_c.borderTopWidth) || 0;
          var_m += parseFloat(var_c.borderLeftWidth) || 0;
        }

        var_k = var_c;
      }
      if (var_k.position === "relative" || var_k.position === "static") {
        var_l += var_i.offsetTop;
        var_m += var_i.offsetLeft;
      }

      if (var_f.support.fixedPosition &&
        var_k.position === "fixed") {
        var_l += Math.max(var_h.scrollTop, var_i.scrollTop);
        var_m += Math.max(var_h.scrollLeft, var_i.scrollLeft);
      }

      let result;

      result = {
        top: var_l,
        left: var_m,
      };

      return result;
    };
  }

  var_f.offset = {
      bodyOffset: function (var_a) {
        let var_b, var_c;
        var_b = var_a.offsetTop;
        var_c = var_a.offsetLeft;

        if (var_f.support.doesNotIncludeMarginInBodyOffset) {
          var_b += parseFloat(var_f.css(var_a, "marginTop")) || 0;
          var_c += parseFloat(var_f.css(var_a, "marginLeft")) || 0;
        }

        let result;

        result = {
          top: var_b,
          left: var_c,
        };

        return result;
      },
      setOffset: function (var_a, var_b, var_c) {
        let var_d;
        var_d = var_f.css(var_a, "position");

        if (var_d === "static") {
          var_a.style.position = "relative";
        }

        let var_e, var_g, var_h, var_i, var_j, var_k, var_l, var_m, var_n;
        var_e = var_f(var_a);
        var_g = var_e.offset();
        var_h = var_f.css(var_a, "top");
        var_i = var_f.css(var_a, "left");

        if (var_d === "absolute" || var_d === "fixed") {
          var_j = var_f.inArray("auto", [var_h, var_i]) > false;
        } else {
          if (var_d === "absolute") {
            var_j = var_d === "absolute";
          } else {
            var_j = var_d === "fixed";
          }
        }

        var_k = {};
        var_l = {};

        if (var_j) {
          var_l = var_e.position();
          var_m = var_l.top;
          var_n = var_l.left;
        } else {
          if (parseFloat(var_h)) {
            var_m = parseFloat(var_h);
          } else {
            var_m = 0;
          }

          if (parseFloat(var_i)) {
            var_n = parseFloat(var_i);
          } else {
            var_n = 0;
          }
        }

        if (var_f.isFunction(var_b)) {
          var_b = var_b.call(var_a, var_c, var_g);
        }

        if (var_b.top != null) {
          var_k.top = var_b.top - var_g.top + var_m;
        }

        if (var_b.left != null) {
          var_k.left = var_b.left - var_g.left + var_n;
        }

        if ("using" in var_b) {
          var_b.using.call(var_a, var_k);
        } else {
          var_e.css(var_k);
        }
      },
    };

  var_f.fn.extend({
    position: function () {
      if (!this[0]) {
        return null;
      }
      let var_a, var_b, var_c, var_d;
      var_a = this[0];
      var_b = this.offsetParent();
      var_c = this.offset();

      if (cz.test(var_b[0].nodeName)) {
        var_d = {
            top: 0,
            left: 0,
          };
      } else {
        var_d = var_b.offset();
      }

      var_c.top -= parseFloat(var_f.css(var_a, "marginTop")) || 0;
      var_c.left -= parseFloat(var_f.css(var_a, "marginLeft")) || 0;
      var_d.top += parseFloat(var_f.css(var_b[0], "borderTopWidth")) || 0;
      var_d.left += parseFloat(var_f.css(var_b[0], "borderLeftWidth")) || 0;
      let result;

      result = {
        top: var_c.top - var_d.top,
        left: var_c.left - var_d.left,
      };

      return result;
    },
    offsetParent: function () {
      let result;

      result = this.map(function () {
        let var_a;

        if (this.offsetParent) {
          var_a = this.offsetParent;
        } else {
          var_a = var_c.body;
        }

        while (var_a && !cz.test(var_a.nodeName) && var_f.css(var_a, "position") === "static") {
          var_a = var_a.offsetParent;
        }
        return var_a;
      });

      return result;
    },
  });

  var_f.each(["Left", "Top"], function (var_a, var_c) {
    let var_d;
    var_d = "scroll" + var_c;
    var_f.fn[var_d] = function (var_c) {
      let var_e, var_g;
      if (var_c === var_b) {
        var_e = this[0];
        if (!var_e) {
          return null;
        }
        var_g = cA(var_e);
        let result;

        if (var_g) {
          if ("pageXOffset" in var_g) {
            result = var_g[var_a ? "pageYOffset" : "pageXOffset"];
          } else {
            if (var_f.support.boxModel && var_g.document.documentElement[var_d]) {
              if (var_f.support.boxModel) {
                result = var_g.document.documentElement[var_d];
              } else {
                result = var_f.support.boxModel;
              }
            } else {
              result = var_g.document.body[var_d];
            }
          }
        } else {
          result = var_e[var_d];
        }

        return result;
      }
      let result;

      result = this.each(function () {
        var_g = cA(this);

        if (var_g) {
          var_g.scrollTo(var_a ? var_f(var_g).scrollLeft() : var_c, var_a ? var_c : var_f(var_g).scrollTop());
        } else {
          this[var_d] = var_c;
        }
      });

      return result;
    };
  });

  var_f.each(["Height", "Width"], function (var_a, var_c) {
    let var_d;
    var_d = var_c.toLowerCase();

    var_f.fn["inner" + var_c] = function () {
      let var_a;
      var_a = this[0];
      let result;

      if (var_a) {
        if (var_a.style) {
          result = parseFloat(var_f.css(var_a, var_d, "padding"));
        } else {
          result = this[var_d]();
        }
      } else {
        result = null;
      }

      return result;
    };

    var_f.fn["outer" + var_c] = function (var_a) {
      let var_b;
      var_b = this[0];
      let result;

      if (var_b) {
        if (var_b.style) {
          result = parseFloat(var_f.css(var_b, var_d, var_a ? "margin" : "border"));
        } else {
          result = this[var_d]();
        }
      } else {
        result = null;
      }

      return result;
    };

    var_f.fn[var_d] = function (var_a) {
      let var_e;
      var_e = this[0];
      if (!var_e) {
        let result;

        if (var_a == null) {
          result = null;
        } else {
          result = this;
        }

        return result;
      }
      if (var_f.isFunction(var_a)) {
        let result;

        result = this.each(function (var_b) {
          let var_c;
          var_c = var_f(this);
          var_c[var_d](var_a.call(this, var_b, var_c[var_d]()));
        });

        return result;
      }
      if (var_f.isWindow(var_e)) {
        let var_g, var_h;
        var_g = var_e.document.documentElement["client" + var_c];
        var_h = var_e.document.body;
        let result;

        if ((var_e.document.compatMode === "CSS1Compat" && var_g) ||
        (var_h && var_h["client" + var_c])) {
          if (var_e.document.compatMode === "CSS1Compat" && var_g) {
            if (var_e.document.compatMode === "CSS1Compat") {
              result = var_g;
            } else {
              result = var_e.document.compatMode === "CSS1Compat";
            }
          } else {
            if (var_h) {
              result = var_h["client" + var_c];
            } else {
              result = var_h;
            }
          }
        } else {
          result = var_g;
        }

        return result;
      }
      if (var_e.nodeType === 9) {
        let result;

        result = Math.max(
          var_e.documentElement["client" + var_c],
          var_e.body["scroll" + var_c],
          var_e.documentElement["scroll" + var_c],
          var_e.body["offset" + var_c],
          var_e.documentElement["offset" + var_c]
        );

        return result;
      }
      if (var_a === var_b) {
        let var_i, var_j;
        var_i = var_f.css(var_e, var_d);
        var_j = parseFloat(var_i);
        let result;

        if (var_f.isNumeric(var_j)) {
          result = var_j;
        } else {
          result = var_i;
        }

        return result;
      }
      let result;
      result = this.css(var_d, typeof var_a == "string" ? var_a : var_a + "px");
      return result;
    };
  });

  var_a.jQuery = var_a.$ = var_f;
})(window);
