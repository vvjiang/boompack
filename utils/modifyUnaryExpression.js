import { types, visit } from "recast";

const { literal } = types.builders;

/**
 * 将所有!0转换为true，!1转换为false
 * @param {抽象语法树} ast
 */
const modifyUnaryExpression = (ast) => {
  visit(ast, {
    visitUnaryExpression: function (path) {
      if (path.node.argument.type === "Literal") {
        if (path.node.argument.value === 0) {
          return literal(true);
        }
        if (path.node.argument.value === 1) {
          return literal(false);
        }
      }
      this.traverse(path);
    },
  });
};

export default modifyUnaryExpression;
