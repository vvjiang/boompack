import addBlock from "./addBlock.js";
import modifyReturn from "./modifyReturn.js";
import modifyUnaryExpression from "./modifyUnaryExpression.js";
// 修改声明中的表达式
import replaceVarName from "./modifyVariableDeclaration/replaceVarName.js";
import modifyDeclarationInit from "./modifyVariableDeclaration/modifyDeclarationInit.js";

// 修改表达式
import modifyExpressionStatement from "./modifyExpressionStatement/index.js";

/**
 * 修改抽象语法树
 */
const modifyAst = (ast) => {
  modifyUnaryExpression(ast);
  replaceVarName(ast);
  addBlock(ast);
  modifyReturn(ast);
  modifyDeclarationInit(ast);
  modifyExpressionStatement(ast);
};

export default modifyAst;
