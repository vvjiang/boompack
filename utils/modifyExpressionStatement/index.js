import { visit } from "recast";
import modifyAssignmentExpression from "./modifyAssignmentExpression.js";
import modifyConditionalExpression from "./modifyConditionalExpression.js";
import modifyLogicalExpression from "./modifyLogicalExpression.js";
import modifySequenceExpression from "./modifySequenceExpression.js";

/**
 * 分解赋值表达式
 */
const modifyExpressionStatement = (ast) => {
  visit(ast, {
    visitExpressionStatement: function (path) {
      // 获取所有表达式陈述语句，如果该表达式为赋值表达式
      if (
        path.node.expression.type == "AssignmentExpression" &&
        path.node.expression.operator === "="
      ) {
        modifyAssignmentExpression.call(this, path);
      } else if (path.node.expression.type == "ConditionalExpression") {
        modifyConditionalExpression.call(this, path);
      } else if (path.node.expression.type == "LogicalExpression") {
        modifyLogicalExpression.call(this, path);
      } else if (path.node.expression.type == "SequenceExpression") {
        modifySequenceExpression.call(this, path);
      }
      this.traverse(path);
    },
  });
};

export default modifyExpressionStatement;
