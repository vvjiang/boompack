import { types } from "recast";

const { expressionStatement } = types.builders;

/**
 * 获取所有表达式陈述语句，如果该表达式为序列表达式，那么分解为多个语句
 */
const modifySequenceExpression = function (path) {
  const expressionStatements = path.node.expression.expressions.map(
    (childExp) => {
      return expressionStatement(childExp);
    }
  );
  const parentPath = path.insertBefore(...expressionStatements);
  parentPath.each((childPath) => {
    if (expressionStatements.includes(childPath.node)) {
      this.visit(childPath);
    }
  });
  path.prune();
};

export default modifySequenceExpression;
