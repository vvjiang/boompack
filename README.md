# boompack

## 介绍

反压缩 js 代码，使用 ast 修改压缩的 js，增强可读性，用于分析代码。

## 使用方法

- 克隆仓库到本地后
- yarn install 安装依赖包
- 将需要转换地压缩代码，复制粘贴到**test/from/index.js**这个文件中
- 终端运行脚本 yarn start
- 最终会在**test/to/**这个文件夹下生成 index.js，也就是我们最后修改后的文件。

## 更多详情

想了解更多可以看看我的这篇博客

[反压缩 js ，我的万花筒写轮眼开了，CV 能力大幅提升](https://www.cnblogs.com/vvjiang/p/15976301.html)
